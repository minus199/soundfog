package com.minus.soundfog.modules.player;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "avatar_url",
    "id",
    "kind",
    "last_modified",
    "permalink",
    "permalink_url",
    "uri",
    "username"
})

public class UserDescriptor {

    @JsonProperty("avatar_url")
    private String avatar_url;
    @JsonProperty("id")
    private Integer id;
    @JsonProperty("kind")
    private String kind;
    @JsonProperty("last_modified")
    private String last_modified;
    @JsonProperty("permalink")
    private String permalink;
    @JsonProperty("permalink_url")
    private String permalink_url;
    @JsonProperty("uri")
    private String uri;
    @JsonProperty("username")
    private String username;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return The avatar_url
     */
    @JsonProperty("avatar_url")
    public String getAvatarUrl() {
        return avatar_url;
    }

    /**
     *
     * @param avatarUrl The avatar_url
     */
    @JsonProperty("avatar_url")
    public void setAvatarUrl(String avatarUrl) {
        this.avatar_url = avatarUrl;
    }

    /**
     *
     * @return The id
     */
    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id The id
     */
    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return The kind
     */
    @JsonProperty("kind")
    public String getKind() {
        return kind;
    }

    /**
     *
     * @param kind The kind
     */
    @JsonProperty("kind")
    public void setKind(String kind) {
        this.kind = kind;
    }

    /**
     *
     * @return The last_modified
     */
    @JsonProperty("last_modified")
    public String getLastModified() {
        return last_modified;
    }

    /**
     *
     * @param lastModified The last_modified
     */
    @JsonProperty("last_modified")
    public void setLastModified(String lastModified) {
        this.last_modified = lastModified;
    }

    /**
     *
     * @return The permalink
     */
    @JsonProperty("permalink")
    public String getPermalink() {
        return permalink;
    }

    /**
     *
     * @param permalink The permalink
     */
    @JsonProperty("permalink")
    public void setPermalink(String permalink) {
        this.permalink = permalink;
    }

    /**
     *
     * @return The permalink_url
     */
    @JsonProperty("permalink_url")
    public String getPermalinkUrl() {
        return permalink_url;
    }

    /**
     *
     * @param permalinkUrl The permalink_url
     */
    @JsonProperty("permalink_url")
    public void setPermalinkUrl(String permalinkUrl) {
        this.permalink_url = permalinkUrl;
    }

    /**
     *
     * @return The uri
     */
    @JsonProperty("uri")
    public String getUri() {
        return uri;
    }

    /**
     *
     * @param uri The uri
     */
    @JsonProperty("uri")
    public void setUri(String uri) {
        this.uri = uri;
    }

    /**
     *
     * @return The username
     */
    @JsonProperty("username")
    public String getUsername() {
        return username;
    }

    /**
     *
     * @param username The username
     */
    @JsonProperty("username")
    public void setUsername(String username) {
        this.username = username;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
