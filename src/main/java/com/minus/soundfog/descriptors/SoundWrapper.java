package com.minus.soundfog.descriptors;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.annotation.Generated;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@Generated("org.jsonschema2pojo")
public class SoundWrapper {

    /**
     *
     * (Required)
     *
     */
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    /**
     *
     * (Required)
     *
     */
    @SerializedName("origin")
    @Expose
    private SoundDescriptor likeDescriptor;
    /**
     *
     * (Required)
     *
     */
    @SerializedName("tags")
    @Expose
    private String tags;
    /**
     *
     * (Required)
     *
     */
    @SerializedName("type")
    @Expose
    private String type;

    /**
     * No args constructor for use in serialization
     *
     */
    public SoundWrapper() {
    }

    /**
     *
     * @param tags
     * @param createdAt
     * @param origin
     * @param type
     */
    public SoundWrapper(String createdAt, SoundDescriptor origin, String tags, String type) {
        this.createdAt = createdAt;
        this.likeDescriptor = origin;
        this.tags = tags;
        this.type = type;
    }

    /**
     *
     * (Required)
     *
     * @return The createdAt
     */
    public Date getCreatedAt() {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
        try {
            return formatter.parse(createdAt);
        } catch (ParseException e) {
            return null;
        }
    }

    /**
     *
     * (Required)
     *
     * @param createdAt The created_at
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     *
     * (Required)
     *
     * @return The origin
     */
    public SoundDescriptor getLikeDescriptor() {
        return likeDescriptor;
    }

    /**
     *
     * (Required)
     *
     * @param origin The origin
     */
    public void setLikeDescriptor(SoundDescriptor origin) {
        this.likeDescriptor = origin;
    }

    /**
     *
     * (Required)
     *
     * @return The tags
     */
    public String getTags() {
        return tags;
    }

    /**
     *
     * (Required)
     *
     * @param tags The tags
     */
    public void setTags(String tags) {
        this.tags = tags;
    }

    /**
     *
     * (Required)
     *
     * @return The type
     */
    public String getType() {
        return type;
    }

    /**
     *
     * (Required)
     *
     * @param type The type
     */
    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(createdAt).append(likeDescriptor).append(tags).append(type).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof SoundWrapper) == false) {
            return false;
        }
        SoundWrapper rhs = ((SoundWrapper) other);
        return new EqualsBuilder().append(createdAt, rhs.createdAt).append(likeDescriptor, rhs.likeDescriptor).append(tags, rhs.tags).append(type, rhs.type).isEquals();
    }

}
