package com.minus.soundfog.descriptors;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.minus.soundfog.exceptions.ImmutablePropertyException;
import com.minus.soundfog.modules.db.models.ModelSound;
import com.minus.soundfog.modules.playlist.LoaderType;
import java.net.MalformedURLException;
import java.net.URL;
import javax.annotation.Generated;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@Generated("org.jsonschema2pojo")
public class SoundDescriptor {

    @SerializedName("artwork_url")
    @Expose
    private String artworkUrl;
    @SerializedName("attachments_uri")
    @Expose
    private String attachmentsUri;
    @SerializedName("bpm")
    @Expose
    private Object bpm;
    @SerializedName("comment_count")
    @Expose
    private Integer commentCount;
    @SerializedName("commentable")
    @Expose
    private Boolean commentable;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("download_count")
    @Expose
    private Integer downloadCount;
    @SerializedName("chronologicall_order")
    @Expose
    private Integer chronologicallOrder;
    @SerializedName("downloadable")
    @Expose
    private Boolean downloadable;
    @SerializedName("duration")
    @Expose
    private Integer duration;
    @SerializedName("embeddable_by")
    @Expose
    private String embeddableBy;
    @SerializedName("favoritings_count")
    @Expose
    private Integer favoritingsCount;
    @SerializedName("genre")
    @Expose
    private String genre;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("isrc")
    @Expose
    private Object isrc;
    @SerializedName("key_signature")
    @Expose
    private Object keySignature;
    @SerializedName("kind")
    @Expose
    private String kind;
    @SerializedName("label_id")
    @Expose
    private Object labelId;
    @SerializedName("label_name")
    @Expose
    private Object labelName;
    @SerializedName("last_modified")
    @Expose
    private String lastModified;
    @SerializedName("license")
    @Expose
    private String license;
    @SerializedName("likes_count")
    @Expose
    private Integer likesCount;
    @SerializedName("original_content_size")
    @Expose
    private Integer originalContentSize;

    @SerializedName("sound_type")
    @Expose
    private String soundType;

    @SerializedName("original_format")
    @Expose
    private String originalFormat;
    @SerializedName("permalink")
    @Expose
    private String permalink;
    @SerializedName("permalink_url")
    @Expose
    private String permalinkUrl;
    @SerializedName("playback_count")
    @Expose
    private Integer playbackCount;
    @SerializedName("purchase_title")
    @Expose
    private String purchaseTitle;
    @SerializedName("purchase_url")
    @Expose
    private String purchaseUrl;
    @SerializedName("release")
    @Expose
    private Object release;
    @SerializedName("release_day")
    @Expose
    private Object releaseDay;
    @SerializedName("release_month")
    @Expose
    private Object releaseMonth;
    @SerializedName("release_year")
    @Expose
    private Object releaseYear;
    @SerializedName("reposts_count")
    @Expose
    private Integer repostsCount;
    @SerializedName("sharing")
    @Expose
    private String sharing;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("stream_url")
    @Expose
    private String streamUrl;
    @SerializedName("streamable")
    @Expose
    private Boolean streamable;
    @SerializedName("tag_list")
    @Expose
    private String tagList;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("track_type")
    @Expose
    private Object trackType;
    @SerializedName("uri")
    @Expose
    private String uri;
    @SerializedName("user")
    @Expose
    private User user;
    @SerializedName("user_favorite")
    @Expose
    private Object userFavorite;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("user_playback_count")
    @Expose
    private Object userPlaybackCount;
    @SerializedName("user_uri")
    @Expose
    private String userUri;
    @SerializedName("video_url")
    @Expose
    private String videoUrl;
    @SerializedName("waveform_url")
    @Expose
    private String waveformUrl;
    private ModelSound modelSound;

    /**
     * No args constructor for use in serialization
     *
     */
    public SoundDescriptor() {
    }

    /**
     *
     * @return The artworkUrl
     */
    public URL getArtworkUrl() throws MalformedURLException {
        try {
            return new URL(artworkUrl);
        } catch (MalformedURLException malformedURLException) {
            return null;
        }
    }

    /**
     *
     * @param artworkUrl The artwork_url
     */
    public void setArtworkUrl(String artworkUrl) {
        this.artworkUrl = artworkUrl;
    }

    /**
     *
     * @return The attachmentsUri
     */
    public URL getAttachmentsUri() {
        try {
            return new URL(attachmentsUri);
        } catch (MalformedURLException ex) {
            return null;
        }
    }

    /**
     *
     * @param attachmentsUri The attachments_uri
     */
    public void setAttachmentsUri(String attachmentsUri) {
        this.attachmentsUri = attachmentsUri;
    }

    /**
     *
     * @return The bpm
     */
    public Object getBpm() {
        return bpm;
    }

    /**
     *
     * @param bpm The bpm
     */
    public void setBpm(Object bpm) {
        this.bpm = bpm;
    }

    /**
     *
     * @return The commentCount
     */
    public Integer getCommentCount() {
        return commentCount;
    }

    /**
     *
     * @param commentCount The comment_count
     */
    public void setCommentCount(Integer commentCount) {
        this.commentCount = commentCount;
    }

    /**
     *
     * @return The commentable
     */
    public Boolean getCommentable() {
        return commentable;
    }

    /**
     *
     * @param commentable The commentable
     */
    public void setCommentable(Boolean commentable) {
        this.commentable = commentable;
    }

    /**
     *
     * @return The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     *
     * @param createdAt The created_at
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     *
     * @return The description
     */
    public String getDescription() {
        return description;
    }

    /**
     *
     * @param description The description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     *
     * @return The downloadCount
     */
    public Integer getDownloadCount() {
        return downloadCount;
    }

    /**
     *
     * @param downloadCount The download_count
     */
    public void setDownloadCount(Integer downloadCount) {
        this.downloadCount = downloadCount;
    }

    /**
     *
     * @return The downloadable
     */
    public Boolean getDownloadable() {
        return downloadable;
    }

    /**
     *
     * @param downloadable The downloadable
     */
    public void setDownloadable(Boolean downloadable) {
        this.downloadable = downloadable;
    }

    /**
     *
     * @return The duration
     */
    public Integer getDuration() {
        return duration;
    }

    /**
     *
     * @param duration The duration
     */
    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    /**
     *
     * @return The embeddableBy
     */
    public String getEmbeddableBy() {
        return embeddableBy;
    }

    /**
     *
     * @param embeddableBy The embeddable_by
     */
    public void setEmbeddableBy(String embeddableBy) {
        this.embeddableBy = embeddableBy;
    }

    /**
     *
     * @return The favoritingsCount
     */
    public Integer getFavoritingsCount() {
        return favoritingsCount;
    }

    /**
     *
     * @param favoritingsCount The favoritings_count
     */
    public void setFavoritingsCount(Integer favoritingsCount) {
        this.favoritingsCount = favoritingsCount;
    }

    /**
     *
     * @return The genre
     */
    public String getGenre() {
        return genre;
    }

    /**
     *
     * @param genre The genre
     */
    public void setGenre(String genre) {
        this.genre = genre;
    }

    /**
     *
     * @return The id
     */
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return The isrc
     */
    public Object getIsrc() {
        return isrc;
    }

    /**
     *
     * @param isrc The isrc
     */
    public void setIsrc(Object isrc) {
        this.isrc = isrc;
    }

    /**
     *
     * @return The keySignature
     */
    public Object getKeySignature() {
        return keySignature;
    }

    /**
     *
     * @param keySignature The key_signature
     */
    public void setKeySignature(Object keySignature) {
        this.keySignature = keySignature;
    }

    /**
     *
     * @return The kind
     */
    public String getKind() {
        return kind;
    }

    /**
     *
     * @param kind The kind
     */
    public void setKind(String kind) {
        this.kind = kind;
    }

    /**
     *
     * @return The labelId
     */
    public Object getLabelId() {
        return labelId;
    }

    /**
     *
     * @param labelId The label_id
     */
    public void setLabelId(Object labelId) {
        this.labelId = labelId;
    }

    /**
     *
     * @return The labelName
     */
    public String getLabelName() {
        return (String) labelName;
    }

    /**
     *
     * @param labelName The label_name
     */
    public void setLabelName(Object labelName) {
        this.labelName = labelName;
    }

    /**
     *
     * @return The lastModified
     */
    public String getLastModified() {
        return lastModified;
    }

    /**
     *
     * @param lastModified The last_modified
     */
    public void setLastModified(String lastModified) {
        this.lastModified = lastModified;
    }

    /**
     *
     * @return The license
     */
    public String getLicense() {
        return license;
    }

    /**
     *
     * @param license The license
     */
    public void setLicense(String license) {
        this.license = license;
    }

    /**
     *
     * @return The likesCount
     */
    public Integer getLikesCount() {
        return likesCount;
    }

    /**
     *
     * @param likesCount The likes_count
     */
    public void setLikesCount(Integer likesCount) {
        this.likesCount = likesCount;
    }

    /**
     *
     * @return The originalContentSize
     */
    public Integer getOriginalContentSize() {
        return originalContentSize;
    }

    /**
     *
     * @param originalContentSize The original_content_size
     */
    public void setOriginalContentSize(Integer originalContentSize) {
        this.originalContentSize = originalContentSize;
    }

    /**
     *
     * @return The originalFormat
     */
    public String getOriginalFormat() {
        return originalFormat;
    }

    /**
     *
     * @param originalFormat The original_format
     */
    public void setOriginalFormat(String originalFormat) {
        this.originalFormat = originalFormat;
    }

    /**
     *
     * @return The permalink
     */
    public URL getPermalink() {
        try {
            return new URL(permalink);
        } catch (MalformedURLException ex) {
            return null;
        }
    }

    /**
     *
     * @param permalink The permalink
     */
    public void setPermalink(String permalink) {
        this.permalink = permalink;
    }

    /**
     *
     * @return The permalinkUrl
     */
    public URL getPermalinkUrl() {
        try {
            return new URL(permalinkUrl);
        } catch (MalformedURLException ex) {
            return null;
        }
    }

    /**
     *
     * @param permalinkUrl The permalink_url
     */
    public void setPermalinkUrl(String permalinkUrl) {
        this.permalinkUrl = permalinkUrl;
    }

    /**
     *
     * @return The playbackCount
     */
    public Integer getPlaybackCount() {
        return playbackCount;
    }

    /**
     *
     * @param playbackCount The playback_count
     */
    public void setPlaybackCount(Integer playbackCount) {
        this.playbackCount = playbackCount;
    }

    /**
     *
     * @return The purchaseTitle
     */
    public Object getPurchaseTitle() {
        return purchaseTitle;
    }

    /**
     *
     * @param purchaseTitle The purchase_title
     */
    public void setPurchaseTitle(String purchaseTitle) {
        this.purchaseTitle = purchaseTitle;
    }

    /**
     *
     * @return The purchaseUrl
     */
    public URL getPurchaseUrl() {
        try {
            return new URL(purchaseUrl);
        } catch (MalformedURLException ex) {
            return null;
        }
    }

    /**
     *
     * @param purchaseUrl The purchase_url
     */
    public void setPurchaseUrl(String purchaseUrl) {
        this.purchaseUrl = purchaseUrl;
    }

    /**
     *
     * @return The release
     */
    public Object getRelease() {
        return release;
    }

    /**
     *
     * @param release The release
     */
    public void setRelease(Object release) {
        this.release = release;
    }

    /**
     *
     * @return The releaseDay
     */
    public Double getReleaseDay() {
        return (Double) releaseDay;
    }

    /**
     *
     * @param releaseDay The release_day
     */
    public void setReleaseDay(Object releaseDay) {
        this.releaseDay = releaseDay;
    }

    /**
     *
     * @return The releaseMonth
     */
    public Double getReleaseMonth() {
        return (Double) releaseMonth;
    }

    /**
     *
     * @param releaseMonth The release_month
     */
    public void setReleaseMonth(Object releaseMonth) {
        this.releaseMonth = releaseMonth;
    }

    /**
     *
     * @return The releaseYear
     */
    public Double getReleaseYear() {
        return (Double) releaseYear;
    }

    /**
     *
     * @param releaseYear The release_year
     */
    public void setReleaseYear(Object releaseYear) {
        this.releaseYear = releaseYear;
    }

    /**
     *
     * @return The repostsCount
     */
    public Integer getRepostsCount() {
        return repostsCount;
    }

    /**
     *
     * @param repostsCount The reposts_count
     */
    public void setRepostsCount(Integer repostsCount) {
        this.repostsCount = repostsCount;
    }

    /**
     *
     * @return The sharing
     */
    public String getSharing() {
        return sharing;
    }

    /**
     *
     * @param sharing The sharing
     */
    public void setSharing(String sharing) {
        this.sharing = sharing;
    }

    /**
     *
     * @return The state
     */
    public String getState() {
        return state;
    }

    /**
     *
     * @param state The state
     */
    public void setState(String state) {
        this.state = state;
    }

    /**
     *
     * @return The streamUrl
     */
    public URL getStreamUrl() {
        try {
            return new URL(streamUrl);
        } catch (MalformedURLException ex) {
            return null;
        }
    }

    /**
     *
     * @param streamUrl The stream_url
     */
    public void setStreamUrl(String streamUrl) {
        this.streamUrl = streamUrl;
    }

    /**
     *
     * @return The streamable
     */
    public Boolean getStreamable() {
        return streamable;
    }

    /**
     *
     * @param streamable The streamable
     */
    public void setStreamable(Boolean streamable) {
        this.streamable = streamable;
    }

    /**
     *
     * @return The tagList
     */
    public String getTagList() {
        return tagList;
    }

    /**
     *
     * @param tagList The tag_list
     */
    public void setTagList(String tagList) {
        this.tagList = tagList;
    }

    /**
     *
     * @return The title
     */
    public String getTitle() {
        return title;
    }

    /**
     *
     * @param title The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     *
     * @return The trackType
     */
    public Object getTrackType() {
        return trackType;
    }

    /**
     *
     * @param trackType The track_type
     */
    public void setTrackType(Object trackType) {
        this.trackType = trackType;
    }

    /**
     *
     * @return The uri
     */
    public String getUri() {
        return uri;
    }

    /**
     *
     * @param uri The uri
     */
    public void setUri(String uri) {
        this.uri = uri;
    }

    /**
     *
     * @return The user
     */
    public User getUser() {
        return user;
    }

    /**
     *
     * @param user The user
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     *
     * @return The userFavorite
     */
    public Object getUserFavorite() {
        return userFavorite;
    }

    /**
     *
     * @param userFavorite The user_favorite
     */
    public void setUserFavorite(Object userFavorite) {
        this.userFavorite = userFavorite;
    }

    /**
     *
     * @return The userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     *
     * @param userId The user_id
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     *
     * @return The userPlaybackCount
     */
    public Object getUserPlaybackCount() {
        return userPlaybackCount;
    }

    /**
     *
     * @param userPlaybackCount The user_playback_count
     */
    public void setUserPlaybackCount(Object userPlaybackCount) {
        this.userPlaybackCount = userPlaybackCount;
    }

    /**
     *
     * @return The userUri
     */
    public String getUserUri() {
        return userUri;
    }

    public LoaderType getSoundType() {
        return LoaderType.valueOf(soundType);
    }

    public void setSoundType(String soundType) {
        this.soundType = soundType;
    }

    /**
     *
     * @param userUri The user_uri
     */
    public void setUserUri(String userUri) {
        this.userUri = userUri;
    }

    /**
     *
     * @return The videoUrl
     */
    public URL getVideoUrl() {
        try {
            return new URL(videoUrl);
        } catch (MalformedURLException ex) {
            return null;
        }
    }

    /**
     *
     * @param videoUrl The video_url
     */
    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    /**
     *
     * @return The waveformUrl
     * @throws java.net.MalformedURLException
     */
    public URL getWaveformUrl() {
        try {
            if (waveformUrl.startsWith("https://")) {
                waveformUrl = waveformUrl.replace("https://", "http://");
            }
            return new URL(waveformUrl);
        } catch (MalformedURLException malformedURLException) {
            return null;
        }
    }

    /**
     *
     * @param waveformUrl The waveform_url
     */
    public void setWaveformUrl(String waveformUrl) {
        this.waveformUrl = waveformUrl;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(artworkUrl).append(attachmentsUri).append(bpm).append(commentCount).append(commentable).append(createdAt).append(description).append(downloadCount).append(downloadable).append(duration).append(embeddableBy).append(favoritingsCount).append(genre).append(id).append(isrc).append(keySignature).append(kind).append(labelId).append(labelName).append(lastModified).append(license).append(likesCount).append(originalContentSize).append(originalFormat).append(permalink).append(permalinkUrl).append(playbackCount).append(purchaseTitle).append(purchaseUrl).append(release).append(releaseDay).append(releaseMonth).append(releaseYear).append(repostsCount).append(sharing).append(state).append(streamUrl).append(streamable).append(tagList).append(title).append(trackType).append(uri).append(user).append(userFavorite).append(userId).append(userPlaybackCount).append(userUri).append(videoUrl).append(waveformUrl).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof SoundDescriptor) == false) {
            return false;
        }
        SoundDescriptor rhs = ((SoundDescriptor) other);
        return new EqualsBuilder().append(artworkUrl, rhs.artworkUrl).append(attachmentsUri, rhs.attachmentsUri).append(bpm, rhs.bpm).append(commentCount, rhs.commentCount).append(commentable, rhs.commentable).append(createdAt, rhs.createdAt).append(description, rhs.description).append(downloadCount, rhs.downloadCount).append(downloadable, rhs.downloadable).append(duration, rhs.duration).append(embeddableBy, rhs.embeddableBy).append(favoritingsCount, rhs.favoritingsCount).append(genre, rhs.genre).append(id, rhs.id).append(isrc, rhs.isrc).append(keySignature, rhs.keySignature).append(kind, rhs.kind).append(labelId, rhs.labelId).append(labelName, rhs.labelName).append(lastModified, rhs.lastModified).append(license, rhs.license).append(likesCount, rhs.likesCount).append(originalContentSize, rhs.originalContentSize).append(originalFormat, rhs.originalFormat).append(permalink, rhs.permalink).append(permalinkUrl, rhs.permalinkUrl).append(playbackCount, rhs.playbackCount).append(purchaseTitle, rhs.purchaseTitle).append(purchaseUrl, rhs.purchaseUrl).append(release, rhs.release).append(releaseDay, rhs.releaseDay).append(releaseMonth, rhs.releaseMonth).append(releaseYear, rhs.releaseYear).append(repostsCount, rhs.repostsCount).append(sharing, rhs.sharing).append(state, rhs.state).append(streamUrl, rhs.streamUrl).append(streamable, rhs.streamable).append(tagList, rhs.tagList).append(title, rhs.title).append(trackType, rhs.trackType).append(uri, rhs.uri).append(user, rhs.user).append(userFavorite, rhs.userFavorite).append(userId, rhs.userId).append(userPlaybackCount, rhs.userPlaybackCount).append(userUri, rhs.userUri).append(videoUrl, rhs.videoUrl).append(waveformUrl, rhs.waveformUrl).isEquals();
    }

    public void setModelSound(ModelSound modelSound) throws ImmutablePropertyException {
        if (this.modelSound != null) {
            throw new ImmutablePropertyException();
        }

        this.modelSound = modelSound;
    }

    public ModelSound getModelSound() {
        return modelSound;
    }

    public Integer getChronologicallOrder() {
        return chronologicallOrder;
    }

    public void setChronologicallOrder(Integer chronologicallOrder) {
        this.chronologicallOrder = chronologicallOrder;
    }

}
