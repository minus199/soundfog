package com.minus.soundfog.exceptions;

/**
 * @author minus
 */
public class UnableToPlaySoundException extends Exception{

    public UnableToPlaySoundException(Throwable cause) {
        super(cause);
    }
    
}
