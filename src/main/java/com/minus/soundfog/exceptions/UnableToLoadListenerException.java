package com.minus.soundfog.exceptions;

import com.minus.soundfog.modules.eventsdispatcher.EventType;
import com.minus.soundfog.modules.eventsdispatcher.Listener;
import org.apache.logging.log4j.Logger;

/**
 * @author MiNuS420 <minus199@gmail.com>
 */
public class UnableToLoadListenerException extends RuntimeException {

    private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger(UnableToLoadListenerException.class.getName());

    public UnableToLoadListenerException(EventType eventType, Class<? extends Listener> listenerKlazz) {
        logger.error(getMessage());
    }

}
