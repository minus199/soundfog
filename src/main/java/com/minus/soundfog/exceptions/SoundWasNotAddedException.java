package com.minus.soundfog.exceptions;

import com.minus.soundfog.modules.sounds.SoundService;

/**
 * @author minus
 */
public class SoundWasNotAddedException extends Exception {

    public SoundWasNotAddedException() {
        
    }

    public SoundWasNotAddedException(SoundService soundService) {
        super("Sound " + soundService.getID() + " " + soundService.getTitle() + " was not added.");
    }

}
