package com.minus.soundfog.exceptions;

/**
 * @author minus
 */
public class UnsupportedLoaderException extends RuntimeException {

    public UnsupportedLoaderException(String message, Throwable cause) {
        super(message, cause);
    }
    
    

}
