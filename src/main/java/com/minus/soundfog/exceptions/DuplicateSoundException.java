package com.minus.soundfog.exceptions;

import com.minus.soundfog.modules.sounds.SoundService;

/**
 * @author minus
 */
public class DuplicateSoundException extends Exception {

    public DuplicateSoundException() {
    }

    public DuplicateSoundException(SoundService soundService) {
        super("Sound " + soundService.getID() + " " + soundService.getTitle() + " seems to be duplicate.");
    }

}
