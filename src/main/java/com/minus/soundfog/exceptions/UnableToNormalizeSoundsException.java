package com.minus.soundfog.exceptions;

/**
 * @author minus
 */
public class UnableToNormalizeSoundsException extends RuntimeException{

    public UnableToNormalizeSoundsException(Throwable cause) {
        super(cause);
    }
    
}
