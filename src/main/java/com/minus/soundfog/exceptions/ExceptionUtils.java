package com.minus.soundfog.exceptions;

import java.util.ArrayList;
import java.util.List;

/**
 * @author minus
 */
public class ExceptionUtils {

    public static String getExceptionMessageChainAsString(Throwable throwable) {
        return String.join(", ", ExceptionUtils.getExceptionMessageChain(throwable));
    }
    
    public static List<String> getExceptionMessageChain(Throwable throwable) {
        List<String> result = new ArrayList<>();
        while (throwable != null) {
            result.add(throwable.getMessage());
            throwable = throwable.getCause();
        }
        return result; 
    }
}
