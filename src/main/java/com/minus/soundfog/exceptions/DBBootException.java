package com.minus.soundfog.exceptions;

import org.apache.logging.log4j.Logger;

/**
 * @author minus
 */
public class DBBootException extends RuntimeException {
    private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger(DBBootException.class.getName());
    private final Exception previousException;

    public DBBootException(Exception previousException) {
        super("DB Failed to boot with the following error: " + ExceptionUtils.getExceptionMessageChainAsString(previousException));

        logger.error(getMessage(), previousException);
        this.previousException = previousException;
    }

    public Exception getPreviousException() {
        return previousException;
    }
}
