package com.minus.soundfog.exceptions;

/**
 * @author minus
 */
public class ImmutablePropertyException extends RuntimeException{
    public ImmutablePropertyException() {
        super("Immutable!");
    }
}
