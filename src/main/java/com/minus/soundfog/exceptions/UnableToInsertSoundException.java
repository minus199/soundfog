package com.minus.soundfog.exceptions;

import com.minus.soundfog.modules.sounds.SoundService;

/**
 * @author minus
 */
public class UnableToInsertSoundException extends Exception {
    private Exception previousEx;
    private SoundService sound;

    public UnableToInsertSoundException() {
    }

    public UnableToInsertSoundException(java.lang.Exception previousException, SoundService sound) {
        this.previousEx = previousException;
        this.sound = sound;
    }

    public Exception getPreviousEx() {
        return previousEx;
    }

    public SoundService getSound() {
        return sound;
    }

    @Override
    public String getMessage() {
        return super.getMessage() + " || " + previousEx.getMessage(); //To change body of generated methods, choose Tools | Templates.
    }
}
