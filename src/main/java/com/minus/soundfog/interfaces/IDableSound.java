package com.minus.soundfog.interfaces;

/**
 * @author minus
 */
public interface IDableSound {
    public Integer getID();
}
