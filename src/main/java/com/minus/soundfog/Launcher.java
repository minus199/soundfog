package com.minus.soundfog;

import com.minus.soundfog.exceptions.ExceptionUtils;
import com.minus.soundfog.modules.eventsdispatcher.Dispatcher;
import com.minus.soundfog.modules.ui.SoundFogPreloader;
import com.sun.javafx.application.LauncherImpl;
import java.math.BigInteger;
import java.security.SecureRandom;

/**
 * @author minus
 */
public class Launcher {

    private static final org.apache.logging.log4j.Logger logger = org.apache.logging.log4j.LogManager.getLogger(Launcher.class.getName());

    private static SecureRandom random = new SecureRandom();

    public static String nextSessionId() {
        return new BigInteger(130, random).toString(32 + random.nextInt());
    }

    public static void main(String[] args) {
        initLogger();
        try {
            LauncherImpl.launchApplication(MainApp.class, SoundFogPreloader.class, args);
        } catch (Exception e) {
            logger.fatal(ExceptionUtils.getExceptionMessageChainAsString(e), e);
            System.exit(0);
        }
    }

    private static void initLogger() throws SecurityException {
        /*SLF4JBridgeHandler.removeHandlersForRootLogger();
         SLF4JBridgeHandler.install();
         Logger.getGlobal().setLevel(Level.FINEST);*/

        Thread.setDefaultUncaughtExceptionHandler((Thread t, Throwable ex) -> {
            org.apache.logging.log4j.LogManager.getLogger(Launcher.class.getName())
                    .error("Crashed thread " + t.getName() + ". Exception was: " + ex.toString(), ex);
        });

        logger.info("Logger initiated.");
    }
}
