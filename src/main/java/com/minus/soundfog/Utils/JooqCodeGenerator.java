package com.minus.soundfog.Utils;

import com.minus.soundfog.Launcher;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jooq.util.GenerationTool;
import org.jooq.util.jaxb.Configuration;
import org.jooq.util.jaxb.Database;
import org.jooq.util.jaxb.Generate;
import org.jooq.util.jaxb.Generator;
import org.jooq.util.jaxb.Jdbc;
import org.jooq.util.jaxb.Property;
import org.jooq.util.jaxb.Target;

/**
 * @author minus
 */
public class JooqCodeGenerator {
    public static void generate() {
        Configuration configuration = new Configuration()
                .withJdbc(new Jdbc()
                        .withDriver("org.h2.Driver")
                        .withUrl("jdbc:h2:~/soundfog;AUTOCOMMIT=OFF;MVCC=true")
                        .withUser("app")
                        .withPassword("app")
                )
                .withGenerator(new Generator()
                    .withDatabase(new Database()
                        //.withName("org.jooq.util.h2.H2Database")
                        .withName("org.jooq.util.jpa.JPADatabase")
                        .withProperties(new Property().withKey("packages").withValue("com.minus.soundfog.modules.db.models"))
                    ).withTarget(new Target()
                        .withPackageName("com.minus.soundfog.modules.db.mappings")
                        .withDirectory("/tmp/target/generated-sources/jooq")
                    ).withGenerate(new Generate()
                            .withDaos(Boolean.TRUE)
                            .withFluentSetters(Boolean.TRUE)
                            .withImmutablePojos(Boolean.TRUE)
                            .withInstanceFields(Boolean.TRUE)
                            .withJpaAnnotations(Boolean.TRUE)
                            .withPojos(Boolean.TRUE)
                            .withRelations(Boolean.TRUE)
                            .withValidationAnnotations(Boolean.TRUE)
                            .withGeneratedAnnotation(Boolean.FALSE)
                    )
                );

        try {
            GenerationTool.generate(configuration);
        } catch (Exception ex) {
            Logger.getLogger(Launcher.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
