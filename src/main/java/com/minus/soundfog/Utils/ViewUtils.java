package com.minus.soundfog.Utils;

import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.awt.image.FilteredImageSource;
import java.awt.image.ImageFilter;
import java.awt.image.ImageProducer;
import java.awt.image.RGBImageFilter;
import java.util.ResourceBundle;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import org.apache.logging.log4j.Logger;

/**
 * @author minus
 */
public class ViewUtils {
    private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger(ViewUtils.class.getName());
    
    public static Node loadFXML(String fxml, ResourceBundle resourceBundle) {
        FXMLLoader loader = new FXMLLoader(ViewUtils.class.getResource("/fxml/" + fxml + ".fxml"), resourceBundle);
        try {
            return loader.load();
        } catch (Exception ex) {
            logger.error(ex.getCause().getMessage());
        }

        return null;
    }

    public static Node loadFXML(String fxmlMainViewfxml) {
        return loadFXML(fxmlMainViewfxml, null);
    }

    public static BufferedImage makeColorTransparent(java.awt.Image im) {
        ImageFilter filter = new RGBImageFilter() {
            int markerRGB = 16777215 | -16777216;

            @Override
            public final int filterRGB(int x, int y, int rgb) {
                if ((rgb | -16777216) == markerRGB) {
                    return 16777215 & rgb;
                } else {
                    return rgb;
                }
            }
        };
        ImageProducer ip = new FilteredImageSource(im.getSource(), filter);
        return (BufferedImage) Toolkit.getDefaultToolkit().createImage(ip);
    }
}
