package Utils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * @author minus
 */
final public class MethodUtils {

    private Class klazz;
    private Method[] currentObjectMethods;
    private ArrayList<Method> matchedMethods;

    private MethodUtils(Class klazz) {
        this.klazz = klazz;
        currentObjectMethods = this.klazz.getMethods();
    }

    public static MethodUtils onObject(Object object) {
        return new MethodUtils(object.getClass());
    }

    public static MethodUtils onClass(Class klazz) {
        return new MethodUtils(klazz);
    }

    private ArrayList<Method> findMethodsByPrefix(MethodTypesEnum methodType) {
        ArrayList<Method> output = new ArrayList();

        for (Method method : currentObjectMethods) {
            if (method.getName().startsWith(methodType.toString().toLowerCase())) {
                if (method.getName().equals("getClass")) {
                    continue;
                }

                output.add(method);
            }
        }

        return output;
    }

    public MethodUtils allSetters() {
        matchedMethods = findMethodsByPrefix(MethodTypesEnum.SET);
        return this;
    }

    public MethodUtils allGetters() {
        matchedMethods = findMethodsByPrefix(MethodTypesEnum.GET);
        return this;
    }

    public ArrayList<Method> get() {
        return matchedMethods;
    }

    public HashMap invoke(Object... args) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        HashMap output = new HashMap();

        for (Method matchedMethod : matchedMethods) {
            try {
                output.put(matchedMethod.getName(), matchedMethod.invoke(klazz, args).toString());
            } catch (NullPointerException exception) {
                output.put(matchedMethod.getName(), "");
            }
        }

        return output;
    }

    public static boolean isGetter(Method method) {
        if (!method.getName().startsWith("get")) {
            return false;
        }

        if (method.getParameterTypes().length != 0) {
            return false;
        }

        return !void.class.equals(method.getReturnType());
    }

    public static boolean isSetter(Method method) {
        if (!method.getName().startsWith("set")) {
            return false;
        }
        if (method.getParameterTypes().length != 1) {
            return false;
        }
        return true;
    }
}
