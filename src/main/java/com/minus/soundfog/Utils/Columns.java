package com.minus.soundfog.Utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Columns {

    List<List<String>> lines = new ArrayList<>();

    int numColumns = -1;
    private int maxColSize = 0;
    final private String delimiter;

    private String output;

    public Columns() {
        this.delimiter = "\t";
    }

    public Columns(String delimiter) {
        this.delimiter = delimiter == null ? " " : delimiter;
    }

    public Columns addLine(Object... line) {
        List<String> collect = Arrays.asList(line).stream().map(o -> o.toString()).collect(Collectors.toList());
        lines.add(collect);

        final Comparator<String> comp = (s1, s2) -> Integer.compare(s1.length(), s2.length());
        maxColSize = Math.max(maxColSize, collect.stream().max(comp).get().length());

        return this;
    }

    private String prepareRow(List<String> line) {
        return line.stream()
                .map((word) -> this.padWord(word))
                .collect(Collectors.joining(""));
    }

    private String padWord(String word) {
        int currentMax = (int) (maxColSize * 1.10);
        while (currentMax % 2 != 0) {
            currentMax += 1;
        }

        StringBuilder paddedWord = new StringBuilder();
        paddedWord.append(word);
        final String pad = new String(new char[currentMax - word.length()]).replace("\0", " ");
        
        paddedWord.append(pad);
        paddedWord.append(delimiter);
        //paddedWord.append(maxColSize - pad.length());

        return paddedWord.toString();
    }

    public String toString() {
        String collect = lines.stream().map(line -> prepareRow(line)).collect(Collectors.joining("\n"));
        return "\n" + collect + "\n";
    }

}
