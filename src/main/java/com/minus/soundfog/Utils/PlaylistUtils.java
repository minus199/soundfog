package com.minus.soundfog.Utils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.apache.logging.log4j.Logger;

/**
 * @author MiNuS420 <minus199@gmail.com>
 */
public class PlaylistUtils {

    private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger(PlaylistUtils.class.getName());

    final public static List<String> diff(IntStream biggerStream, IntStream smallerStream) {
        Set<Integer> biggerSet = collectToSet(biggerStream);
        Set<Integer> smallerSet = collectToSet(smallerStream);

        if (biggerSet.equals(smallerSet)) {
            return new ArrayList();
        }

        HashSet<Integer> toExclude = new HashSet<>(biggerSet);
        toExclude.removeAll(smallerSet);

        return toExclude.stream()
                .map(Object::toString)
                .collect(Collectors.toList());
    }

    private static Set<Integer> collectToSet(IntStream intStream) {
        return intStream.boxed()
                .sorted()
                .collect(Collectors.toSet());
    }
}
