package Utils;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Optional;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextArea;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;

/**
 * @author minus
 */
public class MsgGen {
    public static void alertException(String title, String header, Exception ex) {
        javafx.scene.control.Alert alert = new javafx.scene.control.Alert(javafx.scene.control.Alert.AlertType.ERROR);
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(ex.getMessage());

        // Backtrace to string
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        ex.printStackTrace(pw);
        String exceptionText = sw.toString();

        //Label label = new Label("The exception stacktrace was:");
        TextArea textArea = new TextArea(exceptionText);
        textArea.setEditable(false);
        textArea.setWrapText(true);

        textArea.setMaxWidth(Double.MAX_VALUE);
        textArea.setMaxHeight(Double.MAX_VALUE);
        GridPane.setVgrow(textArea, Priority.ALWAYS);
        GridPane.setHgrow(textArea, Priority.ALWAYS);

        GridPane expContent = new GridPane();
        expContent.setMaxWidth(Double.MAX_VALUE);
        //expContent.add(label, 0, 0);
        expContent.add(textArea, 0, 1);

        alert.getDialogPane().setContent(expContent);
        alert.setWidth(700);
        alert.setHeight(600);

        alert.showAndWait();
    }

    public static void alertException(Exception ex) {
        alertException("This can be helpful", "Something bad happend -- " + ex.getMessage(), ex);
    }

    public static final Optional<ButtonType> alert(String title, String headerText, String content, Alert.AlertType alertType) {
        Alert alert = new Alert(alertType);
        alert.setTitle(title);
        alert.setHeaderText(headerText);
        alert.setContentText(content);

        return alert.showAndWait();
    }

    public static void alertInfo(String title, String headerText, String content) {
        alert(title, headerText, content, Alert.AlertType.INFORMATION);
    }

    public static void alertError(String title, String headerText, String content) {
        alert(title, headerText, content, Alert.AlertType.ERROR);
    }

    public static void alertWarning(String title, String headerText, String content) {
        alert(title, headerText, content, Alert.AlertType.WARNING);
    }

    public static void alertComifrmation(String title, String headerText, String content) {
        alert(title, headerText, content, Alert.AlertType.CONFIRMATION);
    }
}
