package com.minus.soundfog.modules.eventsdispatcher;

import java.util.HashMap;
import java.util.List;
import java.util.Set;

/**
 * @author minus
 */
interface IListenersRegistrar {
    public IListenersRegistrar register();
    public HashMap<EventType, List<Class<? extends Listener>>> getListeners();
    public Set<EventType> getRegisteredEvents();
}
