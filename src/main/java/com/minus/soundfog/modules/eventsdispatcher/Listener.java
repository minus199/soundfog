package com.minus.soundfog.modules.eventsdispatcher;

/**
 *
 * @author minus
 */
public interface Listener {
    abstract public void execute(Event event);
}
