package com.minus.soundfog.modules.eventsdispatcher;

import com.minus.soundfog.modules.ui.MainViewController;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

/**
 * @author MiNuS420 <minus199@gmail.com>
 */
class PlaylistListenersRegistrar implements IListenersRegistrar {

    private final HashMap<EventType, List<Class<? extends Listener>>> listeners = new HashMap();

    @Override
    public IListenersRegistrar register() {
        //registerListener(EventType.PLAYLIST_LOADED_EVENT, MainViewController.class);
        return this;
    }

    private void registerListener(EventType eventType, Class<? extends Listener> listenerKlazz) {
        if (listeners.get(eventType) == null) {
            listeners.put(eventType, new ArrayList());
        }

        listeners.get(eventType).add(listenerKlazz);
    }

    @Override
    public Set<EventType> getRegisteredEvents() {
        return listeners.keySet();
    }

    @Override
    public HashMap<EventType, List<Class<? extends Listener>>> getListeners() {
        return listeners;
    }

}
