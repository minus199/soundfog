package com.minus.soundfog.modules.eventsdispatcher;

import com.minus.soundfog.modules.dispatching.listeners.FetchedBatchNormalizedListener;
import com.minus.soundfog.modules.dispatching.listeners.SoundClickedListener;
import com.minus.soundfog.modules.dispatching.listeners.SoundCreatedListener;
import com.minus.soundfog.modules.dispatching.listeners.SoundOnPlayClickedListener;
import com.minus.soundfog.modules.dispatching.listeners.SoundProgressListener;
import com.minus.soundfog.modules.dispatching.listeners.SoundsContainersCacheReadyListener;
import com.minus.soundfog.modules.dispatching.listeners.UpdateViewOnPlaybackActionListener;
import com.minus.soundfog.modules.dispatching.listeners.playback.NextListener;
import com.minus.soundfog.modules.dispatching.listeners.playback.PauseListener;
import com.minus.soundfog.modules.dispatching.listeners.playback.PlayListener;
import com.minus.soundfog.modules.dispatching.listeners.playback.PlayPauseListener;
import com.minus.soundfog.modules.dispatching.listeners.playback.PreviousListener;
import com.minus.soundfog.modules.dispatching.listeners.playback.SeekBackwardsListener;
import com.minus.soundfog.modules.dispatching.listeners.playback.SeekForwardListener;
import com.minus.soundfog.modules.dispatching.listeners.playback.StopListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import org.apache.logging.log4j.Logger;

/**
 * @author MiNuS420 <minus199@gmail.com>
 */
class MainListenersRegistrar implements IListenersRegistrar {

    private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger(MainListenersRegistrar.class.getName());
    private final HashMap<EventType, List<Class<? extends Listener>>> listeners = new HashMap();

    /**
     *
     * @return
     */
    @Override
    public IListenersRegistrar register() {
        registerListener(EventType.SOUND_PROGRESS_EVENT, SoundProgressListener.class);
        registerListener(EventType.SOUND_ON_PLAY_CLICKED_EVENT, SoundOnPlayClickedListener.class);
        registerListener(EventType.SOUND_CREATED_EVENT, SoundCreatedListener.class);
        
        registerListener(EventType.SOUND_CLICKED_EVENT, SoundClickedListener.class);

        /* Playback Related */
        registerListener(EventType.PREVIOUS_EVENT, PreviousListener.class);
        registerListener(EventType.NEXT_EVENT, NextListener.class);
        registerListener(EventType.STOP_EVENT, StopListener.class);
        registerListener(EventType.PAUSE_EVENT, PauseListener.class);
        registerListener(EventType.PLAY_EVENT, PlayListener.class);
        registerListener(EventType.PLAY_PAUSE_EVENT, PlayPauseListener.class);
        registerListener(EventType.SEEK_BACKWARDS_EVENT, SeekBackwardsListener.class);
        registerListener(EventType.SEEK_FORWARD_EVENT, SeekForwardListener.class);

        registerListener(EventType.HANDLE_VIEW_ON_PLAYBACK_ACTION_EVENT, UpdateViewOnPlaybackActionListener.class);

        registerListener(EventType.SOUNDS_CONTAINERS_CACHE_READY, SoundsContainersCacheReadyListener.class);
        registerListener(EventType.FETCHED_PAGE_BATCH_NORMALIZED_EVENT, FetchedBatchNormalizedListener.class);
        
        return this;
    }

    private void registerListener(EventType eventType, Class<? extends Listener> listenerKlazz) {
        if (listeners.get(eventType) == null) {
            listeners.put(eventType, new ArrayList());
        }

        listeners.get(eventType).add(listenerKlazz);
    }

    @Override
    public Set<EventType> getRegisteredEvents() {
        return listeners.keySet();
    }

    public HashMap<EventType, List<Class<? extends Listener>>> getListeners() {
        return listeners;
    }

}
