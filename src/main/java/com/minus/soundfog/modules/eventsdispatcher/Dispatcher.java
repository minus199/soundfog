package com.minus.soundfog.modules.eventsdispatcher;

import com.minus.soundfog.modules.dispatching.DispatcherRunnable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import org.reflections.Reflections;

/**
 * @author minus
 */
public class Dispatcher {

    private static final org.apache.logging.log4j.Logger logger = org.apache.logging.log4j.LogManager.getLogger(Dispatcher.class.getName());

    final private HashMap<Class<? extends IListenersRegistrar>, IListenersRegistrar> dispatchers = new HashMap();
    private static Boolean isInitiated = false;

    final private HashMap<EventType, ArrayList<Class<? extends Listener>>> eventListeners = new HashMap<>();
    //final private EventsRegistrar eventsRegistrar;

    private Dispatcher() {

    }

    private static class InstanceContainer {

        private static final Dispatcher instance = new Dispatcher();
    }

    public static Dispatcher manage() {
        Dispatcher instance = InstanceContainer.instance;

        if (!isInitiated) {
            Set<Class<? extends IListenersRegistrar>> eventsRegistrar = new Reflections(Dispatcher.class).getSubTypesOf(IListenersRegistrar.class);

            eventsRegistrar.forEach((Class<? extends IListenersRegistrar> registrar) -> {
                try {
                    instance.dispatchers.put(registrar, registrar.newInstance().register());
                } catch (InstantiationException | IllegalAccessException ex) {
                    logger.error(ex);
                }
            });

            isInitiated = true;
        }

        return instance;
    }

    public Dispatcher dispatch(Event event) {
        dispatchers.forEach((Class<? extends IListenersRegistrar> eventsRegistrarKlazz, IListenersRegistrar eventsRegistrar) -> {

            if (eventsRegistrar.getRegisteredEvents().contains(event.getType())) {
                List<Class<? extends Listener>> listeners = eventsRegistrar.getListeners().get(event.getType());

                Thread dispatchingThread = new Thread(new DispatcherRunnable(event, (ArrayList<Class<? extends Listener>>) listeners));
                dispatchingThread.start();
            }
        });

        return this;
    }
}
