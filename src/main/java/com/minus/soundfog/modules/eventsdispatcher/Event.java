package com.minus.soundfog.modules.eventsdispatcher;

/**
 * @author minus
 */
public interface Event {
    public EventType getType();
}
