package com.minus.soundfog.modules.ui;

import com.minus.soundfog.MainApp;
import com.minus.soundfog.exceptions.WaveformNotFoundException;
import com.minus.soundfog.modules.player.Player;
import com.minus.soundfog.modules.playlist.ActivePlaylist;
import com.minus.soundfog.modules.sounds.SoundService;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.effect.BlendMode;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;

/**
 * FXML Controller class
 *
 * @author minus
 */
public class CurrentlyPlayingViewController extends StackPane implements Initializable {

    final static String HIGHLIGHTED_CLASS = "highlighted-sound";

    @FXML
    private Label artist;
    @FXML
    private Label title;
    @FXML
    private Label progress;
    @FXML
    private ImageView waveform;
    @FXML
    private ProgressIndicator playProgressBar;
    private double lastLocation;

    private Boolean isCurrentlyPlayingProgressViewHolded = false;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        getStyleClass().add("currently-playing");
        waveform.blendModeProperty().setValue(BlendMode.MULTIPLY);
        playProgressBar.getStyleClass().add(".progress-bar");
        setHeight(80.00);
    }

    final public void toggleHighlight(SoundService soundService) {
        toggleHighlight(ActivePlaylist.getInstance().get(soundService));
    }

    final public void toggleHighlight(HBox element) {
        if (element.getStyleClass().contains(HIGHLIGHTED_CLASS)) {
            element.getStyleClass().remove(HIGHLIGHTED_CLASS);
        } else {
            element.getStyleClass().add(HIGHLIGHTED_CLASS);
        }
    }

    public void onPlayEvent(SoundService soundService) {
        playProgressBar.prefWidthProperty().bind(MainApp.getInstance().getScene().widthProperty());
        playProgressBar.prefHeightProperty().bind(heightProperty());

        /* Update currently playing view */
        Platform.runLater(() -> {
            /* Try to remove highlighting from previous playing sound container */
            SoundService previouslyPlaying = Player.getInstance().getPreviouslyPlaying();
            if (previouslyPlaying != null) {
                toggleHighlight(previouslyPlaying);
            }

            /* Add hightlighting to currently playing */
            toggleHighlight(soundService);

            playProgressBar.setProgress(0);
            artist.setText(soundService.getArtist());
            title.setText(soundService.getTitle());
            try {
                updateWaveform(soundService);
            } catch (WaveformNotFoundException ex) {
                Logger.getLogger(CurrentlyPlayingViewController.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
    }

    public void onPlaying(String currentProgress, SoundService soundService) {
        Platform.runLater(() -> {
            progress.setText(currentProgress);
            if (!isCurrentlyPlayingProgressViewHolded) {
                playProgressBar.setProgress(Player.getInstance().getProgressPrecentage());
            }
        });
    }

    private void updateWaveform(SoundService soundService) throws WaveformNotFoundException {
        Stop[] stops = new Stop[]{new Stop(0.3, Color.PALEVIOLETRED), new Stop(0.6, Color.DEEPPINK), new Stop(0.8, Color.DARKGREEN)};
        LinearGradient waveformGradient = new LinearGradient(0, 0, 1, 0, true, CycleMethod.NO_CYCLE, stops);

        SnapshotParameters parameters = new SnapshotParameters();
        parameters.setFill(waveformGradient);

        WritableImage snapshot = soundService.getWaveform().getImageView().snapshot(parameters, null);
        waveform.setImage(snapshot);
        waveform.setBlendMode(BlendMode.OVERLAY);
        waveform.fitWidthProperty().bind(MainApp.getInstance().getScene().widthProperty());
        waveform.fitHeightProperty().bind(heightProperty());

        /*try {
         BufferedImage img = ImageIO.read(waveform.get);
         BufferedImage makeColorTransparent = makeColorTransparent(SwingFXUtils.fromFXImage(waveform.getImage(), new BufferedImage);
        
        
         SwingFXUtils.toFXImage(makeColorTransparent, snapshot);
         } catch (IOException ex) {
         Logger.getLogger(CurrentlyPlayingViewController.class.getName()).log(Level.SEVERE, null, ex);
         }*/
    }

    @FXML
    public void onMouseMoveAction(MouseEvent arg0) {
        if (Player.getInstance().currentlyPlaying() != null) {
            isCurrentlyPlayingProgressViewHolded = true;

            if (arg0.getEventType() == MouseEvent.MOUSE_MOVED) {
                lastLocation = arg0.getSceneX();
                playProgressBar.progressProperty().set(arg0.getSceneX() / playProgressBar.getWidth());
            }
        }
    }

    @FXML
    public void onMouseReleased(MouseEvent event) {
        if (isCurrentlyPlayingProgressViewHolded) {
            double progressPrecentage = lastLocation / playProgressBar.getWidth();
            double duration = Player.getInstance().getDuration().toSeconds();

            Player.getInstance().seekTo(duration * progressPrecentage);

            System.out.println("seek to" + duration * progressPrecentage);
        }
    }

    @FXML
    public void onMouseExit(MouseEvent arg0) {
        isCurrentlyPlayingProgressViewHolded = false;
    }
}
