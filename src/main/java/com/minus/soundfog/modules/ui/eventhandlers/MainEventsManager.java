package com.minus.soundfog.modules.ui.eventhandlers;

import com.minus.soundfog.MainApp;
import com.minus.soundfog.annotations.UIEventsManager;
import com.minus.soundfog.modules.KeyboardHandler;
import com.minus.soundfog.modules.playlist.ActivePlaylist;
import com.minus.soundfog.modules.playlist.ActivitiesPlaylist;
import com.minus.soundfog.modules.playlist.LikesPlaylist;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import org.apache.logging.log4j.Logger;
import org.jnativehook.GlobalScreen;
import org.jnativehook.NativeHookException;

/**
 * @author MiNuS420 <minus199@gmail.com>
 */
@UIEventsManager
public class MainEventsManager extends IUserActionEventManager {

    private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger(MainEventsManager.class.getName());

    final private MainApp mainApp;

    final private KeyCombination jumpToCurrentlyPlayingPageCombo = new KeyCodeCombination(KeyCode.P, KeyCodeCombination.CONTROL_DOWN, KeyCodeCombination.SHIFT_DOWN);

    final private KeyCombination loadFetchStreamCombo = new KeyCodeCombination(KeyCode.DIGIT1, KeyCodeCombination.CONTROL_DOWN);
    final private KeyCombination loadFetchLikesCombo = new KeyCodeCombination(KeyCode.DIGIT2, KeyCodeCombination.CONTROL_DOWN);

    final private KeyCombination loadStreamCombo = new KeyCodeCombination(KeyCode.DIGIT1, KeyCodeCombination.CONTROL_DOWN, KeyCodeCombination.SHIFT_DOWN);
    final private KeyCombination loadLikesCombo = new KeyCodeCombination(KeyCode.DIGIT2, KeyCodeCombination.CONTROL_DOWN, KeyCodeCombination.SHIFT_DOWN);

    final private KeyCombination previousPage = new KeyCodeCombination(KeyCode.LEFT, KeyCodeCombination.CONTROL_DOWN, KeyCodeCombination.SHIFT_DOWN);
    final private KeyCombination nextPage = new KeyCodeCombination(KeyCode.RIGHT, KeyCodeCombination.CONTROL_DOWN, KeyCodeCombination.SHIFT_DOWN);

    public MainEventsManager(MainApp mainApp) {
        this.mainApp = mainApp;
    }

    @Override
    public void handle() {
        KeyboardHandler.register();

        Stage stage = mainApp.getStage();
        stage.setOnCloseRequest(event -> {
            try {
                GlobalScreen.unregisterNativeHook();
            } catch (NativeHookException ex) {
                logger.error("unable to GlobalScreen.unregisterNativeHook() during shutdown.", ex);
            }

            System.runFinalization();
            logger.info("Terminated");
            System.exit(0);
        });

        stage.maximizedProperty()
                .addListener((ObservableValue<? extends Boolean> ov, Boolean t, Boolean t1) -> {
                    if (!t1) {
                        Platform.runLater(stage::centerOnScreen);
                    }
                });

        stage.iconifiedProperty()
                .addListener((ObservableValue<? extends Boolean> ov, Boolean t, Boolean t1) -> {
                    logger.debug("minimized:" + t1);
                });

        mainApp.getScene().setOnKeyPressed((KeyEvent event) -> {

            if (loadStreamCombo.match(event)) {
                ActivitiesPlaylist.getInstance().refresh();
                event.consume();
            }
            if (loadLikesCombo.match(event)) {
                LikesPlaylist.getInstance().refresh();
                event.consume();
            }

            if (loadFetchStreamCombo.match(event)) {
                logger.info("Loading stream");
                ActivitiesPlaylist.getInstance().load();

                event.consume();
            }

            if (loadFetchLikesCombo.match(event)) {
                logger.info("Loading likes");
                LikesPlaylist.getInstance().load();

                event.consume();
            }

            if (previousPage.match(event)) {
                ActivitiesPlaylist.getInstance().getPaginator().fetchPrevious();
                event.consume();
            }

            if (nextPage.match(event)) {
                ActivitiesPlaylist.getInstance().getPaginator().fetchNext();
                event.consume();
            }
            
            if (jumpToCurrentlyPlayingPageCombo.match(event)){
                ActivePlaylist.getInstance().getPaginator().JumpTo(Integer.parseInt((String) System.getProperties().get("playingPage")));
            }
        });

        logger.info("Loaded events handling for main menu");
    }
}
