package com.minus.soundfog.modules.ui;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.concurrent.Task;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.layout.HBox;

/**
 * @author minus
 */
public class SpinnerView extends HBox implements Initializable{
 
    final int N_SECS = 10;
    private Task task;
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        new Thread(createTask().createLayout().task).start();
    }

    private SpinnerView createTask() {
        task = new Task<Void>() {
            @Override public Void call() {
                for (int i=0; i < N_SECS; i++) {
                    if (isCancelled()) {
                        break;
                    }
                    // uncomment updateProgress call if you want to show progress
                    // rather than let progress remain indeterminate.
                    // updateProgress(i, N_SECS);
                    updateMessage((N_SECS - i) + "");
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        return null;
                    }
                }

                updateMessage(0 + "");
                updateProgress(N_SECS, N_SECS);

                return null;
            }
        };
        
        return this;
    }

    private SpinnerView createLayout() {
        getChildren().setAll(
            createProgressIndicator(task),
            createCounter(task)
        );

        setAlignment(Pos.CENTER_RIGHT);
        setPadding(new Insets(10));

        return this;
    }

    private ProgressIndicator createProgressIndicator(Task task) {
        ProgressIndicator progress = new ProgressIndicator();

        progress.progressProperty().bind(task.progressProperty());

        return progress;
    }

    private Label createCounter(Task task) {
        Label counter = new Label();

        counter.setMinWidth(20);
        counter.setAlignment(Pos.CENTER_RIGHT);
        counter.textProperty().bind(task.messageProperty());
        counter.setStyle("-fx-border-color: forestgreen;");

        return counter;
    }
}
