package com.minus.soundfog.modules.ui.rscBundels;

import com.minus.soundfog.modules.sounds.SoundService;
import java.util.Enumeration;
import java.util.ListResourceBundle;
import java.util.ResourceBundle;

/**
 * @author minus
 */
public class SoundDisplayRscBundle extends ListResourceBundle{
    final private SoundService sound;

    public SoundDisplayRscBundle(SoundService sound) {
        this.sound = sound;
    }

    public SoundService getSound() {
        return sound;
    }

    @Override
    protected Object[][] getContents() {
        return new Object[][]{};
    }

}
