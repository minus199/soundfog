package com.minus.soundfog.modules.ui;

import com.minus.soundfog.modules.concurrency.SearchbarKeypressTimewatch;
import com.minus.soundfog.modules.dispatching.events.PlaylistLoadedEvent;
import com.minus.soundfog.modules.eventsdispatcher.Event;
import com.minus.soundfog.modules.eventsdispatcher.Listener;
import com.minus.soundfog.modules.playlist.ActivePlaylist;
import com.minus.soundfog.modules.playlist.Playlist;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.property.DoubleProperty;
import javafx.collections.ListChangeListener;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author minus
 */
public class MainViewController extends BorderPane implements Initializable, Listener {

    private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger(MainViewController.class.getName());

    final public static String clientID = "1812d1d9a12736f77e5b2d3339f6997f";
    final public static String clientSecret = "626232736f879d7598bcc69b3d958455";

    @FXML
    private Label playlistItemCount;

    @FXML
    private ScrollPane playlistContainer;

    @FXML
    private PlaybackViewController playbackViewController;

    @FXML
    private FlowPane playlistView;

    @FXML
    private CurrentlyPlayingViewController currentlyPlayingViewController;

    @FXML
    private TextField searchBar;

    @FXML
    private ProgressBar waitingKeystrokesProgressBar;

    private SearchbarKeypressTimewatch searchbarKeypressTimewatch;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        adjustView();
        initEventHandlers();
        waitingKeystrokesProgressBar.getStyleClass().add("time-progress-bar");
    }

    private void initEventHandlers() {
        searchBar.setOnKeyReleased((KeyEvent event) -> {
            if (!event.isControlDown() && event.getCode().isLetterKey()) {
                if (searchBar.getText().length() == 1 || (searchbarKeypressTimewatch != null && !searchbarKeypressTimewatch.isAlive())) {
                    searchbarKeypressTimewatch = new SearchbarKeypressTimewatch();
                    searchbarKeypressTimewatch.start();
                   
                }

                searchbarKeypressTimewatch.resetTimer();
            }

            if (event.getCode() == KeyCode.ESCAPE || getSearchTerm().length() == 0) {
                playlistView.getChildren().clear();
                if (ActivePlaylist.getInstance() != null) {
                    ActivePlaylist.toggleLastList().getPaginator().fetchNext();
                }

                searchBar.setText("");
            }
        });

        playlistView.getChildren().addListener((ListChangeListener.Change<? extends Node> c) -> {
            ListChangeListener.Change<? extends Node> fp = c;
            playlistItemCount.setText(Integer.toString(fp.getList().size()) + "/" + ActivePlaylist.getInstance().size()
                    + "[" + ActivePlaylist.getInstance().getPaginator().getCurrent() + "/" + ActivePlaylist.getInstance().getPaginator().getLastPage() + "]");
        });

        playlistContainer.setOnScroll((ScrollEvent event) -> {
            if (ActivePlaylist.getInstance() == null || ActivePlaylist.getInstance().isEmpty()) {
                return;
            }

            if (event.getDeltaY() > 0) {
                ActivePlaylist.getInstance().getPaginator().fetchPrevious();
            } else {
                ActivePlaylist.getInstance().getPaginator().fetchNext();
            }

            event.consume();
        });
    }

    private void adjustView() {
        playlistView.setVgap(5);
        playlistView.setHgap(10);
        playlistView.setPadding(new Insets(10, 0, 10, 50));
        playlistView.setBackground(new Background(new BackgroundFill(new Color(1, 1, 1, 1), CornerRadii.EMPTY, Insets.EMPTY)));
    }

    public DoubleProperty getVolumeValueProperty() {
        return playbackViewController.getVolumeValueProperty();
    }

    public void putSoundContainers(List<HBox> soundContainers) {
        playlistView.getChildren().clear();
        if (soundContainers == null) {
            return;
        }

        appendSoundContainers(soundContainers);
    }

    public void appendSoundContainers(List<HBox> soundContainers) {
        int from = playlistView.getChildren().size();
        playlistView.getChildren().addAll(soundContainers);
        playlistView.getChildren().addAll(ActivePlaylist.getInstance().getElements());
        int to = playlistView.getChildren().size();

        logger.info("Appended " + from + " - " + to);
    }

    public CurrentlyPlayingViewController getCurrentlyPlayingViewController() {
        return currentlyPlayingViewController;
    }

    public String getSearchTerm() {
        return searchBar.getText();
    }

    @Override
    public void execute(Event event) {
        PlaylistLoadedEvent e = (PlaylistLoadedEvent) event;

        playlistView.getChildren().clear();
        playlistView.getChildren().addAll(ActivePlaylist.getInstance().getElements());
    }

    final public void updateKeyStrokesDelayProgress(double progressPrecentage) {
        if (waitingKeystrokesProgressBar.getProgress() == 0){
            waitingKeystrokesProgressBar.setVisible(true);
        }
        
        this.waitingKeystrokesProgressBar.setProgress(progressPrecentage);
        if (waitingKeystrokesProgressBar.getProgress() >= 0.95){
            waitingKeystrokesProgressBar.setVisible(false);
        }
    }
}
