package com.minus.soundfog.modules.ui;

import com.minus.soundfog.MainApp;
import com.minus.soundfog.exceptions.ArtworkNotFoundException;
import com.minus.soundfog.modules.ui.rscBundels.SoundDisplayRscBundle;
import flowlistview.FlowListView;
import java.awt.ScrollPane;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;

/**
 * @author minus
 */
public class SoundsViewController extends ScrollPane implements Initializable {

    @FXML
    private FlowListView<String> flowListController;
    @FXML
    private TextField nameField;

    public void initialize(URL url, ResourceBundle r) {
        flowListController.selectedItemProperty().addListener(new ChangeListener<String>() {
            

            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        });
    }

    public void onAdd() {
        flowListController.getItems().add(nameField.getText());
    }

    public void onRemove() {
        flowListController.getItems().remove(nameField.getText());
    }
}
