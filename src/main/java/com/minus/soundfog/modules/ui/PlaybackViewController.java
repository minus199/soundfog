package com.minus.soundfog.modules.ui;

import com.minus.soundfog.MainApp;
import com.minus.soundfog.modules.dispatching.events.playback.NextEvent;
import com.minus.soundfog.modules.dispatching.events.playback.PauseEvent;
import com.minus.soundfog.modules.dispatching.events.playback.PlayPauseEvent;
import com.minus.soundfog.modules.dispatching.events.playback.PreviousEvent;
import com.minus.soundfog.modules.dispatching.events.playback.StopEvent;
import com.minus.soundfog.modules.eventsdispatcher.Dispatcher;
import com.minus.soundfog.modules.player.Player;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import javafx.beans.binding.Bindings;
import javafx.beans.property.DoubleProperty;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.effect.ColorAdjust;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.FlowPane;
import javafx.scene.paint.Color;

/**
 * FXML Controller class
 *
 * @author minus
 */
public class PlaybackViewController extends FlowPane implements Initializable {

    @FXML
    private Button play;
    @FXML
    private Button stop;
    @FXML
    private Button pause;
    @FXML
    private Button nextSound;
    @FXML
    private Button previousSound;

    @FXML
    private Button seekForward;
    @FXML
    private Button seekBackward;
    
    @FXML
    private Slider volume; 

    @Override
    public void initialize(URL location, ResourceBundle resources) {
   
        HashMap<String, Button> buttons = new HashMap<>();
        buttons.put("play", play);
        buttons.put("stop", stop);
        buttons.put("pause", pause);
        buttons.put("next", nextSound);
        buttons.put("previous", previousSound);
        buttons.put("seekBackwards", seekBackward);
        buttons.put("seekForward", seekForward);

        for (Map.Entry<String, Button> entrySet : buttons.entrySet()) {
            prepareButton(entrySet.getKey(), entrySet.getValue());
        }
    }
    
    public DoubleProperty getVolumeValueProperty(){
        return volume.valueProperty();
    }

    private void prepareButton(String icon, Button button) {
        button.setGraphic(new ImageView(new Image("/icons/" + icon + ".png")));
        button.getStyleClass().add("playback-button");
        /*BackgroundFill fillBG = new BackgroundFill(Color.WHITE, new CornerRadii(1), new Insets(0.0, 0.0, 0.0, 0.0));
         button.setBackground(new Background(fillBG));*/

        button.setGraphic(fade(button));
        button.setOnMouseEntered((MouseEvent event) -> {
            button.setGraphic(darken(button));
        });

        button.setOnMouseExited((MouseEvent event) -> {
            button.setGraphic(fade(button));
        });

        button.armedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (newValue) {
                button.setGraphic(glow(button));
            } else {
                button.setGraphic(fade(button));
            }
        });
    }

    private ImageView glow(Button button) {
        ImageView graphic = (ImageView) button.getGraphic();

        DropShadow borderGlow = new DropShadow();
        borderGlow.setOffsetY(0f);
        borderGlow.setOffsetX(0f);
        borderGlow.setColor(Color.DARKORANGE);
        borderGlow.setWidth(30);
        borderGlow.setHeight(30);

        graphic.setEffect(borderGlow);
        return graphic;
    }

    private ImageView fade(Button button) {
        return changeBrightness(button, 0.5);
    }

    private ImageView darken(Button button) {
        return changeBrightness(button, 0.0);
    }

    private ImageView changeBrightness(Button button, Double brightness) {
        ImageView graphic = (ImageView) button.getGraphic();

        ColorAdjust colorAdjust = new ColorAdjust();
        colorAdjust.setBrightness(brightness);
        
        graphic.setEffect(colorAdjust);
        return graphic;
    }

    public void toggle(Button toggle) {
        final Image unselected = new Image(
                "http://.png"
        );
        final Image selected = new Image(
                "http://.png"
        );
        final ImageView toggleImage = new ImageView();
        toggle.setGraphic(toggleImage);
        toggleImage.imageProperty().bind(Bindings
                .when(toggle.pressedProperty())
                .then(selected)
                .otherwise(unselected)
        );
    }

    @FXML
    private void nextAction(ActionEvent event) {
        Dispatcher.manage().dispatch(new NextEvent());
    }

    @FXML
    private void previousAction(ActionEvent event) {
        Dispatcher.manage().dispatch(new PreviousEvent());
    }

    @FXML
    private void playPauseAction(ActionEvent event) throws IOException {
        Dispatcher.manage().dispatch(new PlayPauseEvent());
    }

    @FXML
    private void seekForwardAction(ActionEvent event) throws IOException {
        Player.getInstance().seekForward();

    }

    @FXML
    private void seekBackwardsAction(ActionEvent event) throws IOException {
        Player.getInstance().seekBackwards();
    }

    @FXML
    private void stopAction(ActionEvent event) throws IOException {
        Dispatcher.manage().dispatch(new StopEvent());
    }

    @FXML
    private void pauseAction(ActionEvent event) throws IOException {
        Dispatcher.manage().dispatch(new PauseEvent());
    }
}
