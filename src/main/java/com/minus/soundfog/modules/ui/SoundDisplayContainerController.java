package com.minus.soundfog.modules.ui;

import com.minus.soundfog.modules.dispatching.events.SoundClickedEvent;
import com.minus.soundfog.modules.eventsdispatcher.Dispatcher;
import com.minus.soundfog.modules.ui.rscBundels.SoundDisplayRscBundle;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;

/**
 * @author minus
 */
public class SoundDisplayContainerController extends HBox implements Initializable {

    @FXML
    private ImageView artwork;

    @FXML
    private Label title;

//    @FXML
//    private Label artist;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        assert title != null : "title not injected";
        //assert artist != null : "artist not injected";
        assert artwork != null : "artwork not injected";

        //getStyleClass().add("sound-item");
        SoundDisplayRscBundle rb = (SoundDisplayRscBundle) resources;
        //artist.setText(rb.getSound().getArtist());
        title.setText(rb.getSound().getTitle());

        artwork.setImage(rb.getSound().getArtwork().getJavaFxImage());
        artwork.setFitWidth(50);
        artwork.setPreserveRatio(true);
    }
     
    @FXML
    public void onClickAction(MouseEvent event){
        Dispatcher.manage().dispatch(new SoundClickedEvent((HBox) event.getSource()));
    }
    
    @FXML
    public void onMouseOverAction(MouseEvent event){
        ((HBox)event.getSource()).getStyleClass().add("pointed-sound");
    }
    
    @FXML
    public void onMouseOut(MouseEvent event){
        ((HBox)event.getSource()).getStyleClass().remove("pointed-sound");
    }
}