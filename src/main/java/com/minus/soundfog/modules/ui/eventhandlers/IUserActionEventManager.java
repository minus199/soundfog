package com.minus.soundfog.modules.ui.eventhandlers;

/**
 *
 * @author MiNuS420 <minus199@gmail.com>
 */
abstract public class IUserActionEventManager {
    abstract public void handle();
}
