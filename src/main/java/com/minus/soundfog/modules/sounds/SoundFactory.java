package com.minus.soundfog.modules.sounds;

import com.minus.soundfog.modules.db.dto.SoundDto;
import com.minus.soundfog.modules.db.models.ModelSound;

/**
 * @author minus
 */
public class SoundFactory {
    private static final class InstanceContainer{
        private static final SoundFactory instance = new SoundFactory();
    }
    
    public static SoundFactory getInstance(){
        return InstanceContainer.instance;
    }
    
    final public SoundService getByID(Integer id){
        return getByModel((ModelSound) SoundDto.getInstance().getByID(id));
    }
    
    final public SoundService getByModel(ModelSound modelSound){
        return new SoundService(modelSound);
    }
}
