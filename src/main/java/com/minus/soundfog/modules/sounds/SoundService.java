package com.minus.soundfog.modules.sounds;

import com.minus.soundfog.exceptions.ArtworkNotFoundException;
import com.minus.soundfog.exceptions.WaveformNotFoundException;
import com.minus.soundfog.modules.db.models.ModelSound;
import com.minus.soundfog.modules.picturegallery.ImageCreator;
import com.minus.soundfog.modules.picturegallery.Picture;
import com.minus.soundfog.modules.ui.MainViewController;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Objects;

/**
 * @author minus
 */
public class SoundService implements Comparable<SoundService> {

    final private ModelSound modelSound;

    public SoundService(ModelSound modelSound) {
        this.modelSound = modelSound;
    }

    public Integer getID() {
        return modelSound.getId();
    }

    public Integer getExternalID() {
        return modelSound.getSoundcloudID();
    }

    public String getArtist() {
        return modelSound.getArtist().getNicename();
    }

    public String getTitle() {
        return modelSound.getTitle();
    }

    public String[] getTags() {
        return modelSound.getTags().split(" ");
    }

    public String getGenere() {
        return modelSound.getGenere();
    }

    public Picture getArtwork() {
        try {
            return new Picture(modelSound.getSoundArtwork().getModelImage());
        } catch (IOException | NullPointerException ex) {
            try {
                return ImageCreator.makeIcon("default-artwork.png");
            } catch (ArtworkNotFoundException ex1) {
                return null;
            }
        }
    }

    public Picture getWaveform() throws WaveformNotFoundException {
        try {
            return new Picture(modelSound.getSoundWaveform().getModelImage());
        } catch (IOException ex) {
            throw new WaveformNotFoundException();
        }
    }

    public URL getStreamURL() throws MalformedURLException {
        return new URL(modelSound.getUrls().getStreamURL().toString() + "?client_id=" + MainViewController.clientID);
    }

    public Integer getDuration() {
        return modelSound.getStatistics().getDuration();
    }

    public Integer getNaturalOrder() {
        return modelSound.getChronoligcallOrder();
    }
    
    

    @Override
    public int compareTo(SoundService o) {
        return getNaturalOrder() - o.getNaturalOrder();
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 47 * hash + Objects.hashCode(this.modelSound);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SoundService other = (SoundService) obj;
        if (!Objects.equals(this.modelSound, other.modelSound)) {
            return false;
        }
        return true;
    }
}
