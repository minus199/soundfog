package com.minus.soundfog.modules.comparators;

import com.minus.soundfog.interfaces.IDableSound;
import java.util.Comparator;

/**
 * @author minus
 */
public class DisplayComparator implements Comparator<IDableSound> {

    @Override
    public int compare(IDableSound o1, IDableSound o2) {
        return o1.getID() < o2.getID() ? -1 : o1.getID() > o2.getID() ? 1 : doSecodaryOrderSort(o1, o2);
    }

    public int doSecodaryOrderSort(IDableSound o1, IDableSound o2) {
        return o1.getID() < o2.getID() ? -1 : o1.getID() > o2.getID() ? 1 : 0;
    }
}
