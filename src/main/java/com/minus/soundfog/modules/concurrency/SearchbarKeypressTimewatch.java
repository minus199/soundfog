package com.minus.soundfog.modules.concurrency;

import com.minus.soundfog.MainApp;
import com.minus.soundfog.modules.playlist.SearchResultPlaylist;

/**
 * @author MiNuS420 <minus199@gmail.com>
 */
public class SearchbarKeypressTimewatch extends Thread {

    final public static Long WAITING_PERIOD = 1100L; // in millis
    private long timer;

    public SearchbarKeypressTimewatch() {
        resetTimer();
    }

    @Override
    public void run() {
        long delta = System.currentTimeMillis() - timer;
        while (delta <= WAITING_PERIOD) {
            delta = System.currentTimeMillis() - timer;
            double remainingProgress = (double) getRemainingTime() / WAITING_PERIOD;
            MainApp.getInstance().getMainViewController().updateKeyStrokesDelayProgress(remainingProgress);
        }

        SearchResultPlaylist.getInstance().load();
        this.interrupt();
    }

    final public SearchbarKeypressTimewatch resetTimer() {
        this.timer = System.currentTimeMillis();
        return this;
    }

    final public Long getRemainingTime() {
        return System.currentTimeMillis() - timer;
    }
}
