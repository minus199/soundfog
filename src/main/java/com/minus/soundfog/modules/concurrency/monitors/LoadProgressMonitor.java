package com.minus.soundfog.modules.concurrency.monitors;

import com.minus.soundfog.MainApp;
import com.minus.soundfog.Utils.Columns;
import com.minus.soundfog.modules.playlist.ActivePlaylist;

/**
 * @author MiNuS420 <minus199@gmail.com>
 */
public class LoadProgressMonitor implements Runnable {

    private static final org.apache.logging.log4j.Logger logger = org.apache.logging.log4j.LogManager.getLogger(LoadProgressMonitor.class.getName());

    @Override
    public void run() {
        int playlistSize = ActivePlaylist.getInstance().size();
        int processedCount = Integer.parseInt(System.getProperties().getProperty("fetchedCount"));
        while (playlistSize <= processedCount) {
            Columns columns = new Columns("|")
                    .addLine(
                            "active playlist size: " + ActivePlaylist.getInstance().size(),
                            "fetched: " + System.getProperties().getProperty("fetchedCount"),
                            "flushed: " + System.getProperties().getProperty("flushedCount"),
                            "flushed-summed: " + System.getProperties().getProperty("flushedCountSummed"),
                            "raw-processed: " + System.getProperty("rawProcessed")
                    );

            logger.info(columns.toString());

            try {
                Thread.sleep(2000);
            } catch (InterruptedException ex) {

            }
        }

        logger.info("Finished.");
    }

}
