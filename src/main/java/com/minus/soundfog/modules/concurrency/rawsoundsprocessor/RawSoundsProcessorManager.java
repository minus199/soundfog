package com.minus.soundfog.modules.concurrency.rawsoundsprocessor;

import com.google.common.util.concurrent.ThreadFactoryBuilder;
import com.google.gson.JsonArray;
import com.minus.soundfog.modules.concurrency.monitors.RawSoundsProcessorPoolsMonitor;
import com.minus.soundfog.modules.db.models.BaseModel;
import com.minus.soundfog.modules.db.models.ModelFetchedPages;
import com.minus.soundfog.modules.playlist.ActivePlaylist;
import com.minus.soundfog.modules.playlist.LoaderType;
import java.util.List;
import java.util.TreeMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author MiNuS420 <minus199@gmail.com>
 */
final public class RawSoundsProcessorManager {

    private int batchLimit = 0;
    final private TreeMap<Integer, RawSoundsProcessor> processedBatchs = new TreeMap();
    private ThreadPoolExecutor processorPool;
    private final SynchronousQueue<Runnable> queue = new SynchronousQueue<>();

    private RawSoundsProcessorManager() {

    }

    final private static class InstanceContainer {

        final private static RawSoundsProcessorManager instance = new RawSoundsProcessorManager();
    }

    public static final RawSoundsProcessorManager getInstance() {
        return InstanceContainer.instance;
    }

    {
        restartPool();
    }

    public RawSoundsProcessorManager processBatch(JsonArray dataBatch, LoaderType loaderType, ModelFetchedPages modelFetchedPages) {
        return processBatch(new RawSoundsProcessor(dataBatch, loaderType, modelFetchedPages), dataBatch.size());
    }

    public RawSoundsProcessorManager processBatch(List<? extends BaseModel> modelSounds, LoaderType loaderType) {
        return processBatch(new RawSoundsProcessor(modelSounds, loaderType), modelSounds.size());
    }

    private RawSoundsProcessorManager processBatch(RawSoundsProcessor processor, int size) {
        int currentCount = Integer.parseInt(System.getProperties().getProperty("fetchedCount", Integer.toString(0)));
        System.getProperties().setProperty("fetchedCount", Integer.toString(currentCount + size));

        processedBatchs.put(processedBatchs.size(), processor);
        processorPool.submit(processor);

        return this;
    }

    public RawSoundsProcessorManager restartPool() {
        processorPool = new ThreadPoolExecutor(
                5, 5, 0L, TimeUnit.MILLISECONDS,
                queue,
                new ThreadFactoryBuilder().setNameFormat("normalize-pool-%d").build(),
                new ThreadPoolExecutor.CallerRunsPolicy()
        );

        processorPool.prestartCoreThread();

        ExecutorService executorService = Executors.newSingleThreadExecutor();
        executorService.submit(new RawSoundsProcessorPoolsMonitor(processorPool));
        executorService.shutdown();

        return this;
    }

    public void terminate() {
        new Thread(() -> {
            processorPool.shutdown();

            try {
                processorPool.awaitTermination(0L, TimeUnit.DAYS);
            } catch (InterruptedException ex) {
                Logger.getLogger(RawSoundsProcessorManager.class.getName()).log(Level.SEVERE, null, ex);
            }
        }).start();
    }

    public void setBatchLimit(int batchLimit) {
        if (!processorPool.isTerminated() && batchLimit > 0) {
            throw new RuntimeException("Unable to change batch limit since pool is busy");
        }

        this.batchLimit = batchLimit;
    }
}
