package com.minus.soundfog.modules.concurrency;

import com.minus.soundfog.modules.dispatching.events.playback.PlayPauseEvent;
import com.minus.soundfog.modules.eventsdispatcher.Dispatcher;
import com.minus.soundfog.modules.playlist.ActivePlaylist;
import com.minus.soundfog.modules.playlist.Playlist;
import com.minus.soundfog.modules.sounds.SoundFactory;
import com.minus.soundfog.modules.sounds.SoundService;
import java.lang.reflect.InvocationTargetException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.layout.HBox;
import javafx.scene.media.MediaPlayer;

/**
 * @author minus
 */
public class DefaultSoundClickActionRunnable implements Runnable {

    static MediaPlayer mediaPlayer;
    private final SoundService soundService;

    public DefaultSoundClickActionRunnable(HBox clickedElement) {
        Class<? extends Playlist> playListKlazz = (Class<? extends Playlist>) clickedElement.getProperties().get("parentPlaylist");

        try {
            Playlist playListInstance = (Playlist) ClassLoader.getSystemClassLoader().loadClass(playListKlazz.getName()).getMethod("getInstance").invoke(null);
            SoundService soundByElement = playListInstance.getSoundByElement(clickedElement);
        } catch (ClassNotFoundException | NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            Logger.getLogger(DefaultSoundClickActionRunnable.class.getName()).log(Level.SEVERE, null, ex);
        }

        Integer currentPage = ActivePlaylist.getInstance().getPaginator().getCurrent();
        System.getProperties().setProperty("playingPage", currentPage.toString());
        
        this.soundService = SoundFactory.getInstance().getByID((Integer) clickedElement.getProperties().get("soundID"));
    }

    @Override
    public void run() {
        /* Default action when clicking a sound in the list -- this is why there's an additional event triggering */
        Dispatcher.manage().dispatch(new PlayPauseEvent(soundService));
    }
}
