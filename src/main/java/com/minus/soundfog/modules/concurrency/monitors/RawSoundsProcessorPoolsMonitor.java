package com.minus.soundfog.modules.concurrency.monitors;

import com.minus.soundfog.Utils.Columns;
import java.util.concurrent.ThreadPoolExecutor;
import org.apache.logging.log4j.Logger;

/**
 * @author MiNuS420 <minus199@gmail.com>
 */
public class RawSoundsProcessorPoolsMonitor implements Runnable {

    private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger(RawSoundsProcessorPoolsMonitor.class.getName());

    private final ThreadPoolExecutor rawSoundsBatchProcessorsPool;

    public RawSoundsProcessorPoolsMonitor(ThreadPoolExecutor rawSoundsBatchProcessorsPool) {
        this.rawSoundsBatchProcessorsPool = rawSoundsBatchProcessorsPool;
    }

    @Override
    public void run() {
        while (!rawSoundsBatchProcessorsPool.isTerminated()) {
            String toString = new Columns("|")
                    .addLine("ActiveCount ", rawSoundsBatchProcessorsPool.getActiveCount())
                    .addLine("CompletedTaskCount ", rawSoundsBatchProcessorsPool.getCompletedTaskCount())
                    .addLine("TaskCount ", rawSoundsBatchProcessorsPool.getTaskCount())
                    .addLine("isShutdown ", rawSoundsBatchProcessorsPool.isShutdown() ? "true" : "false")
                    .addLine("isTerminated ", rawSoundsBatchProcessorsPool.isTerminated() ? "true" : "false")
                    .addLine("isTerminating ", rawSoundsBatchProcessorsPool.isTerminating() ? "true" : "false")
                    .toString();

            logger.info(toString);

            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {

            }
        }

    }
}
