package com.minus.soundfog.modules.concurrency.rawsoundsprocessor;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.minus.soundfog.descriptors.SoundDescriptor;
import com.minus.soundfog.exceptions.UnableToCommitGroupException;
import com.minus.soundfog.exceptions.UnableToPersistGroupException;
import com.minus.soundfog.modules.db.grouppersisters.SoundModelsGroupPersister;
import com.minus.soundfog.modules.db.models.ModelSound;
import com.minus.soundfog.modules.playlist.LoaderType;
import com.minus.soundfog.modules.sounds.SoundFactory;
import com.minus.soundfog.modules.sounds.SoundService;

/**
 * @author minus
 */
class RawSoundProcessor {

    private JsonElement rawSoundData;
    private SoundDescriptor soundDescriptor = null;
    private ModelSound modelSound = null;
    private SoundService soundService = null;

    public RawSoundProcessor(JsonElement rawSoundData) {
        this.rawSoundData = rawSoundData;
        toSoundDescriptor();
    }

    public RawSoundProcessor(SoundDescriptor soundDescriptor) {
        this.rawSoundData = null;
        this.soundDescriptor = soundDescriptor;
    }

    public RawSoundProcessor(ModelSound modelSound) {
        this.rawSoundData = null;
        this.modelSound = modelSound;
    }

    final public RawSoundProcessor toSoundDescriptor() {
        if (rawSoundData == null) {
            throw new RuntimeException("Must set rawData first.");
        }

        if (soundDescriptor == null) {
            String type = rawSoundData.getAsJsonObject().get("sound_type").getAsString();
            
            JsonElement get = rawSoundData.getAsJsonObject().get("origin");
            rawSoundData = get != null ? get : rawSoundData;
            soundDescriptor = new Gson().fromJson(rawSoundData, SoundDescriptor.class);
            soundDescriptor.setSoundType(type);
        }
        
        return this;
    }
    
    final public RawSoundProcessor toModel(Boolean isPersistanceNeeded) throws UnableToPersistGroupException, UnableToCommitGroupException {
        if (soundDescriptor == null) {
            throw new RuntimeException("Must set like descriptor first.");
        }

        if (modelSound == null) {
            modelSound = (ModelSound) new SoundModelsGroupPersister(soundDescriptor).persistGroup().commit().getModels().get(ModelSound.class);
        }

        return this;
    }

    final public RawSoundProcessor toSoundService() {
        if (modelSound == null) {
            throw new RuntimeException("Must set modelSound first.");
        }

        if (soundService == null) {
            soundService = SoundFactory.getInstance().getByModel(modelSound);
        }
        
        return this;
    }

    public SoundService getSoundService() {
        if (soundService == null) {
            toSoundService();
        }
        
        return soundService;
    }
}
