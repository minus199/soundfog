package com.minus.soundfog.modules.concurrency.rawsoundsprocessor;

import com.google.common.util.concurrent.ThreadFactoryBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.minus.soundfog.Utils.PlaylistUtils;
import com.minus.soundfog.exceptions.UnableToCommitGroupException;
import com.minus.soundfog.exceptions.UnableToPersistGroupException;
import com.minus.soundfog.modules.db.models.BaseModel;
import com.minus.soundfog.modules.db.models.ModelFetchedPages;
import com.minus.soundfog.modules.db.models.ModelSound;
import com.minus.soundfog.modules.dispatching.events.FetchedBatchNormalizedEvent;
import com.minus.soundfog.modules.eventsdispatcher.Dispatcher;
import com.minus.soundfog.modules.playlist.LoaderType;
import com.minus.soundfog.modules.sounds.SoundService;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * @author minus
 */
class RawSoundsProcessor implements Runnable {

    private static final org.apache.logging.log4j.Logger logger = org.apache.logging.log4j.LogManager.getLogger(RawSoundsProcessor.class.getName());

    final private JsonArray dataBatch;
    private final ModelFetchedPages modelFetchedPages;
    private final List<ModelSound> modelSounds;
    private final List<SoundService> soundsCache = Collections.synchronizedList(new ArrayList());
    final private LoaderType loaderType;

    /**
     *
     * @param dataBatch
     * @param modelFetchedPages -- A none null value is equal to saying
     * everything related to this batch should be persisted
     * @param loaderType
     */
    public RawSoundsProcessor(JsonArray dataBatch, LoaderType loaderType, ModelFetchedPages modelFetchedPages) {
        this.dataBatch = dataBatch;
        this.modelFetchedPages = modelFetchedPages;
        this.modelSounds = null;
        this.loaderType = loaderType;
    }

    /**
     * No persistence is needed when using this constructor
     *
     * @param modelSounds
     * @param loaderType
     */
    public RawSoundsProcessor(List<? extends BaseModel> modelSounds, LoaderType loaderType) {
        this.modelSounds = (List<ModelSound>) modelSounds;
        this.modelFetchedPages = null;
        this.dataBatch = null;
        this.loaderType = loaderType;
    }

    @Override
    public void run() {
        ThreadPoolExecutor rawSoundsProcessingPool = new ThreadPoolExecutor(
                0,
                Integer.MAX_VALUE,
                0L, TimeUnit.SECONDS,
                new SynchronousQueue<>(),
                new ThreadFactoryBuilder().setNameFormat("rawSoundsProcessing-pool-%d").build());

        if (dataBatch != null) {
            logger.info("Found raw data from cloud  -- Processing batch of " + dataBatch.size() + " sounds");
            for (JsonElement rawSoundData : dataBatch) {
                ((JsonObject) rawSoundData).addProperty("sound_type", loaderType.toString()); // Add the origin/type of sound -- likes, activities, etc
                
                rawSoundsProcessingPool.submit(new RawSoundFromCloud(rawSoundData));
            }
        }

        if (modelSounds != null) {
            logger.info("Found persisted data -- Processing batch of " + modelSounds.size() + " sounds");
            for (ModelSound modelSound : modelSounds) {
                rawSoundsProcessingPool.submit(new RawSoundFromDB(modelSound));
            }
        }

        terminate(rawSoundsProcessingPool);
    }

    private void terminate(ExecutorService rawSoundsProcessingPool) {
        rawSoundsProcessingPool.shutdown();

        Runnable awaitRawSoundsProcessingPoolTerminationThread = () -> {
            try {
                rawSoundsProcessingPool.awaitTermination(10, TimeUnit.MINUTES);
            } catch (InterruptedException ex) {
                Logger.getLogger(RawSoundsProcessor.class.getName()).log(Level.SEVERE, null, ex);
            }

            //validateBatch();

            Dispatcher.manage().dispatch(new FetchedBatchNormalizedEvent(soundsCache, loaderType));
        };

        new Thread(awaitRawSoundsProcessingPoolTerminationThread, "awaitRawSoundsProcessingPoolTerminationThread").start();
    }

    private boolean validateBatch() {
        List<String> diff = PlaylistUtils.diff(
                modelSounds.stream().mapToInt((ModelSound model) -> model.getSoundcloudID()),
                soundsCache.stream().mapToInt((SoundService m) -> m != null ? m.getExternalID() : 0)
        );

        if (diff.isEmpty()) {
            logger.info("Normalization to sound services completed(caching) with total of " + soundsCache.size() + " cached sounds");
            return true;
        }

        String ids = diff.stream()
                .map(Object::toString)
                .collect(Collectors.joining(", "));

        logger.error("Some sounds were not processed correctly: " + ids);
        return false;
    }

    private boolean persistPage() {
        if (modelFetchedPages != null) {
            //modelFetchedPages.save();
            return true;
        }

        return false;
    }

    private class RawSoundFromCloud implements Runnable {
        private final JsonElement rawSoundData;

        private RawSoundFromCloud(JsonElement rawSoundData) {
            this.rawSoundData = rawSoundData;
        }

        @Override
        public void run() {
            SoundService soundService;
            try {
                soundService = new RawSoundProcessor(rawSoundData).toModel(modelFetchedPages != null).getSoundService();

                if (soundService == null) {
                    throw new RuntimeException("Unable to find sound -- model is null ");
                }

                soundsCache.add(soundService);
                persistPage();
            } catch (UnableToPersistGroupException | UnableToCommitGroupException ex) {
                Logger.getLogger(RawSoundsProcessor.class.getName()).log(Level.SEVERE, null, ex);
            } catch (Exception ex) {
                Logger.getLogger(RawSoundsProcessor.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private class RawSoundFromDB implements Runnable {

        private final ModelSound modelSound;

        public RawSoundFromDB(ModelSound modelSound) {
            this.modelSound = modelSound;
        }

        @Override
        public void run() {
            SoundService soundService;
            try {
                soundService = new RawSoundProcessor(modelSound).getSoundService();
                if (soundService == null) {
                    logger.error("Could not load sound for " + modelSound.getTitle());
                    return;
                }

                soundsCache.add(soundService);
            } catch (Exception ex) {
                logger.error(ex);
            }
        }
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 59 * hash + Objects.hashCode(this.dataBatch);
        hash = 59 * hash + Objects.hashCode(this.loaderType);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RawSoundsProcessor other = (RawSoundsProcessor) obj;
        if (!Objects.equals(this.dataBatch, other.dataBatch)) {
            return false;
        }
        if (this.loaderType != other.loaderType) {
            return false;
        }
        return true;
    }

}
