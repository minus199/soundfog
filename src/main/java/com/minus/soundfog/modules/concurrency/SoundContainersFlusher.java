package com.minus.soundfog.modules.concurrency;

import com.minus.soundfog.Utils.ViewUtils;
import com.minus.soundfog.modules.dispatching.events.SoundsContainersCacheReadyEvent;
import com.minus.soundfog.modules.eventsdispatcher.Dispatcher;
import com.minus.soundfog.modules.playlist.CachedContainer;
import com.minus.soundfog.modules.sounds.SoundService;
import com.minus.soundfog.modules.ui.rscBundels.SoundDisplayRscBundle;
import java.util.List;
import javafx.scene.layout.HBox;
import org.apache.logging.log4j.Logger;

/**
 * @author minus
 */
public class SoundContainersFlusher {

    private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger(SoundContainersFlusher.class.getName());

    final private List<SoundService> soundServices;
    private final CachedContainer cachedContainer;

    /**
     *
     * @param soundServices
     * @param cachedContainer
     */
    public SoundContainersFlusher(List<SoundService> soundServices, CachedContainer cachedContainer) {
        this.soundServices = soundServices;
        this.cachedContainer = cachedContainer;
    }

    public void flush() {
        /* Create view containers for all sound services */
        soundServices.stream().forEach((soundService) -> {
            cacheContainer(soundService);

            int currentCount = Integer.parseInt(System.getProperties().getProperty("flushedCount", Integer.toString(0)));
            System.getProperties().setProperty("flushedCount", Integer.toString(++currentCount));
        });

        int currentCount = Integer.parseInt(System.getProperties().getProperty("flushedCountSummed", Integer.toString(0)));
        System.getProperties().setProperty("flushedCountSummed", Integer.toString(currentCount + soundServices.size()));

        Dispatcher.manage().dispatch(new SoundsContainersCacheReadyEvent(cachedContainer));
        cachedContainer.pushToPlaylist();
    }

    private void cacheContainer(SoundService soundService) {
        if (soundService == null) {
            logger.error("Soundservice was null???");
            return;
        }

        HBox metaContainer = (HBox) ViewUtils.loadFXML("SoundDisplayContainer", new SoundDisplayRscBundle(soundService));
        if (metaContainer == null) {
            throw new RuntimeException("Unable to create container " + soundService.getTitle());
        }

        metaContainer.setCache(true);
        metaContainer.getProperties().put("soundID", soundService.getID());
        metaContainer.getProperties().put("parentPlaylist", cachedContainer.getPlayListKlazz());
        //metaContainer.getProperties().put("index", SoundBatchesCounter.getInstance().getTotalSoundsProcessed() + 1);
        cachedContainer.put(soundService, metaContainer);
    }

}
