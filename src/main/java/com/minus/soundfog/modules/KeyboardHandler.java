package com.minus.soundfog.modules;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.jnativehook.GlobalScreen;
import org.jnativehook.NativeHookException;
import org.jnativehook.keyboard.NativeKeyEvent;
import org.jnativehook.keyboard.NativeKeyListener;

/**
 * @author minus
 */
public class KeyboardHandler implements NativeKeyListener {
    private static final org.apache.logging.log4j.Logger logger = org.apache.logging.log4j.LogManager.getLogger(KeyboardHandler.class.getName());
    private short hotKeyFlag = 0x00;
    private static final short MASK_CTRL = 1 << 0;
    private static final short MASK_LEFT = 1 << 1;

    public static void register() {
        /* Block junk logging from this package */
        Logger logr = Logger.getLogger(GlobalScreen.class.getPackage().getName());
        logr.setLevel(Level.WARNING);
        
        try {
            GlobalScreen.registerNativeHook();
        } catch (NativeHookException ex) {
            logger.error("There was a problem registering the native hook.", ex);
            System.exit(ex.getCode());
        }
        
        GlobalScreen.addNativeKeyListener(new KeyboardHandler());
    }

    private KeyboardHandler() {

    }

    /**
     *
     * @param e
     */
    public void nativeKeyPressed(NativeKeyEvent e) {
        int ind = 0;
        if (NativeKeyEvent.getKeyText(e.getKeyCode()) != null) {
            if (NativeKeyEvent.getKeyText(e.getKeyCode()).equals("P")) {
                logger.debug("Pressed P globally");
            }
        }
        if (e.getKeyCode() == NativeKeyEvent.CTRL_L_MASK) {
            hotKeyFlag &= MASK_CTRL;
            if (hotKeyFlag == (MASK_CTRL & MASK_LEFT)) {
                logger.debug("Pressed MASK_CTRL & MASK_LEFT globally");
            }
        } else if (e.getKeyCode() == NativeKeyEvent.VC_RIGHT) {
            hotKeyFlag &= MASK_LEFT;

            // Check the mask and do work.
            if (hotKeyFlag == (MASK_CTRL & MASK_LEFT)) {
                logger.debug("Pressed MASK_CTRL & MASK_LEFT globally");
            }
        }

        /*switch (e.getKeyCode()) {
         //                case S:
         //                    Player.getInstance().stop();
         //                    ke.consume();
         //                    break;
         case NativeKeyEvent.VC_RIGHT:
         Player.getInstance().seekForward();
         break;
         case NativeKeyEvent.VC_LEFT:
         Player.getInstance().seekBackwards();
         break;
         case NativeKeyEvent.VC_SPACE:
         try {
         Player.getInstance().pausePlay();
         } catch (UnableToPlaySoundException ex) {
         Logger.getLogger(KeyboardHandler.class.getName()).log(Level.SEVERE, null, ex);
         }
        
         break;
         }*/
    }

    @Override
    public void nativeKeyReleased(NativeKeyEvent e) {
        if (e.getKeyCode() == NativeKeyEvent.CTRL_L_MASK) {
            hotKeyFlag ^= MASK_CTRL;
        } else if (e.getKeyCode() == NativeKeyEvent.VC_RIGHT) {
            hotKeyFlag ^= MASK_LEFT;
        }
    }

    @Override
    public void nativeKeyTyped(NativeKeyEvent e) {
        //System.out.println("Key Typed: " + e.getKeyText(e.getKeyCode()));
    }
}
