package com.minus.soundfog.modules.dispatching.events.playback;

import com.minus.soundfog.modules.eventsdispatcher.EventType;
import com.minus.soundfog.modules.sounds.SoundService;

/**
 * @author minus
 */
public class PlayEvent extends AbstractPlaybackEvent{
    final private SoundService soundService;

    public PlayEvent(SoundService soundService) {
        this.soundService = soundService;
    }

    public PlayEvent() {
        this.soundService = null;
    }
    
    @Override
    public EventType getType() {
        return EventType.PLAY_EVENT;
    }

    public SoundService getSoundService() {
        return soundService;
    }
}
