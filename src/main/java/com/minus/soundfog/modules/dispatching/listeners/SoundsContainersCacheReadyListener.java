package com.minus.soundfog.modules.dispatching.listeners;

import com.minus.soundfog.modules.dispatching.events.SoundsContainersCacheReadyEvent;
import com.minus.soundfog.modules.eventsdispatcher.Event;
import com.minus.soundfog.modules.eventsdispatcher.Listener;

/**
 * @author minus
 */
public class SoundsContainersCacheReadyListener implements Listener {
    @Override
    public void execute(Event event) {
        SoundsContainersCacheReadyEvent e = (SoundsContainersCacheReadyEvent) event;
    }
}
