package com.minus.soundfog.modules.dispatching.events.playback;

import com.minus.soundfog.annotations.EventViewProperties;
import com.minus.soundfog.modules.eventsdispatcher.EventType;

/**
 * @author minus
 */
@EventViewProperties(keepAfterEvent = true)
public class SeekBackwardsEvent extends AbstractPlaybackEvent {
    @Override
    public EventType getType() {
        return EventType.SEEK_BACKWARDS_EVENT;
    }
}
