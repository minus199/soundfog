package com.minus.soundfog.modules.dispatching.events;

import com.minus.soundfog.modules.eventsdispatcher.Event;
import com.minus.soundfog.modules.eventsdispatcher.EventType;
import com.minus.soundfog.descriptors.SoundDescriptor;
import com.minus.soundfog.modules.db.models.ModelSound;

/**
 * @author minus
 */
public class SoundCreatedEvent implements Event{
    private final SoundDescriptor likeDescriptor;
    private final ModelSound modelSound;

    public SoundCreatedEvent(SoundDescriptor likeDescriptor, ModelSound modelSound) {
        this.likeDescriptor = likeDescriptor;
        this.modelSound = modelSound;
    }

    @Override
    public EventType getType() {
        return EventType.SOUND_CREATED_EVENT;
    }

    public SoundDescriptor getLikeDescriptor() {
        return likeDescriptor;
    }

    public ModelSound getModelSound() {
        return modelSound;
    }
}
