package com.minus.soundfog.modules.dispatching.events;

import com.minus.soundfog.modules.eventsdispatcher.Event;
import com.minus.soundfog.modules.eventsdispatcher.EventType;
import com.minus.soundfog.modules.playlist.CachedContainer;

/**
 * @author minus
 */
public class SoundsContainersCacheReadyEvent implements Event {

    private final CachedContainer containersCache;

    public SoundsContainersCacheReadyEvent(CachedContainer containersCache) {
        this.containersCache = containersCache;
    }

    @Override
    public EventType getType() {
        return EventType.SOUNDS_CONTAINERS_CACHE_READY;
    }

    public CachedContainer getContainersCache() {
        return containersCache;
    }

}
