package com.minus.soundfog.modules.dispatching.listeners;

import com.minus.soundfog.modules.dispatching.events.SoundOnPlayClickedEvent;
import com.minus.soundfog.modules.eventsdispatcher.Event;
import com.minus.soundfog.modules.eventsdispatcher.Listener;

/**
 * @author minus
 */
public class SoundOnPlayClickedListener implements Listener {

    @Override
    public void execute(Event event) {
        SoundOnPlayClickedEvent e = (SoundOnPlayClickedEvent) event;
        
    }
}
