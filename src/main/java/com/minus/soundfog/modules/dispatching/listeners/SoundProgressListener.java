package com.minus.soundfog.modules.dispatching.listeners;

import com.minus.soundfog.MainApp;
import com.minus.soundfog.modules.dispatching.events.SoundProgressEvent;
import com.minus.soundfog.modules.eventsdispatcher.Event;
import com.minus.soundfog.modules.eventsdispatcher.EventType;
import com.minus.soundfog.modules.eventsdispatcher.Listener;
import java.util.Date;
import javafx.scene.Node;

/**
 * @author minus
 */
public class SoundProgressListener implements Listener {

    @Override
    public void execute(Event event) {
        if (event.getType() == EventType.SOUND_PROGRESS_EVENT) {
            SoundProgressEvent e = (SoundProgressEvent) event;

            MainApp.getInstance().getMainViewController().getCurrentlyPlayingViewController().onPlaying(
                    convertTimestampToReadbable((long) e.getDuration().toMillis()) + " / " + convertTimestampToReadbable(e.getSound().getDuration().longValue()), e.getSound());
        }
    }

    private String convertTimestampToReadbable(long timeInMillis) {
        Date date = new Date((long) timeInMillis);
        long second = (long) ((timeInMillis / 1000) % 60);
        long minute = (long) ((timeInMillis / (1000 * 60)) % 60);
        long hour = (long) ((timeInMillis / (1000 * 60 * 60)) % 24);

        return String.format("%02d:%02d:%02d", hour, minute, second);
    }
}
