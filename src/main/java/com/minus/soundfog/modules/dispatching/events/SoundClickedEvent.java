package com.minus.soundfog.modules.dispatching.events;

import com.minus.soundfog.modules.eventsdispatcher.Event;
import com.minus.soundfog.modules.eventsdispatcher.EventType;
import javafx.scene.layout.HBox;

/**
 * @author minus
 */
public class SoundClickedEvent implements Event{
    final private HBox clickSourceElement;

    public SoundClickedEvent(HBox clickSourceElement) {
        this.clickSourceElement = clickSourceElement;
    }

    /**
     * The element in list view which was clicked
     * @return 
     */
    public HBox getClickSourceElement() {
        return clickSourceElement;
    }

    @Override
    public EventType getType() {
        return EventType.SOUND_CLICKED_EVENT;
    }
}
