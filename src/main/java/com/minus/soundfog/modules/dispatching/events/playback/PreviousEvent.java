package com.minus.soundfog.modules.dispatching.events.playback;

import com.minus.soundfog.modules.eventsdispatcher.Event;
import com.minus.soundfog.modules.eventsdispatcher.EventType;

/**
 * @author minus
 */
public class PreviousEvent extends AbstractPlaybackEvent{

    @Override
    public EventType getType() {
        return EventType.PREVIOUS_EVENT;
    }

}
