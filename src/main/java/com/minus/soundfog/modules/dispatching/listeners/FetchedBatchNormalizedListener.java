package com.minus.soundfog.modules.dispatching.listeners;

import com.minus.soundfog.modules.concurrency.SoundContainersFlusher;
import com.minus.soundfog.modules.dispatching.events.FetchedBatchNormalizedEvent;
import com.minus.soundfog.modules.eventsdispatcher.Event;
import com.minus.soundfog.modules.eventsdispatcher.Listener;
import com.minus.soundfog.modules.playlist.ActivitiesCachedContainer;
import com.minus.soundfog.modules.playlist.CachedContainer;
import com.minus.soundfog.modules.playlist.LikesCachedContainer;
import com.minus.soundfog.modules.playlist.SearchResultCachedContainer;

/**
 * @author minus
 */
public class FetchedBatchNormalizedListener implements Listener {

    @Override
    public void execute(Event event) {
        FetchedBatchNormalizedEvent e = (FetchedBatchNormalizedEvent) event;
        CachedContainer cachedContainer;
        
        switch (e.getLoaderType()){
            case ACTIVITIES:
                cachedContainer = new ActivitiesCachedContainer();
                break;
            case LIKES:
                cachedContainer = new LikesCachedContainer();
                break;
            case SEARCH_RESULT:
                cachedContainer = new SearchResultCachedContainer();
                break;
            default:
                if (e.getLoaderType() == null)
                    throw new RuntimeException("Loader type must be specified.");
                
                throw new RuntimeException(e.getLoaderType().name() + " is not a supported loader.");
        }
        
        /*Thread interpolateSoundToDisplayContainerThread = new Thread(new SoundContainersFlusher(e.getSounds(), cachedContainer), "interpolateSoundToDisplayContainer");
        interpolateSoundToDisplayContainerThread.start();*/
        
        new SoundContainersFlusher(e.getSounds(), cachedContainer).flush();
    }
}
