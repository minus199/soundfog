package com.minus.soundfog.modules.dispatching.events;

import com.minus.soundfog.modules.eventsdispatcher.Event;
import com.minus.soundfog.modules.eventsdispatcher.EventType;
import com.minus.soundfog.modules.sounds.SoundService;
import javafx.util.Duration;

/**
 * @author minus
 */
public class SoundProgressEvent implements Event{
    private final Duration microsecondPosition;
    private final SoundService soundService;

    public SoundProgressEvent(Duration duration, SoundService soundService) {
        this.microsecondPosition = duration;
        this.soundService = soundService;
    }

    @Override
    public EventType getType() {
        return EventType.SOUND_PROGRESS_EVENT;
    }

    public Duration getDuration() {
        return microsecondPosition;
    }

    public SoundService getSound() {
        return soundService;
    }
}
