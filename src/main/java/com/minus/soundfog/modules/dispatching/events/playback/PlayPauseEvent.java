package com.minus.soundfog.modules.dispatching.events.playback;

import com.minus.soundfog.annotations.EventViewProperties;
import com.minus.soundfog.modules.eventsdispatcher.EventType;
import com.minus.soundfog.modules.sounds.SoundService;

/**
 * @author minus
 */
@EventViewProperties(keepAfterEvent = true)
public class PlayPauseEvent extends AbstractPlaybackEvent {
    final private SoundService soundService;

    public PlayPauseEvent() {
        soundService = null;
    }

    public PlayPauseEvent(SoundService soundService) {
        this.soundService = soundService;
    }

    public SoundService getSoundService() {
        return soundService;
    }
    
    @Override
    public EventType getType() {
        return EventType.PLAY_PAUSE_EVENT;
    }
}
