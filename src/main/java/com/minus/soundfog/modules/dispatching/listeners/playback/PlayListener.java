package com.minus.soundfog.modules.dispatching.listeners.playback;

import com.minus.soundfog.exceptions.UnableToPlaySoundException;
import com.minus.soundfog.modules.dispatching.events.playback.PlayEvent;
import com.minus.soundfog.modules.eventsdispatcher.Event;
import com.minus.soundfog.modules.player.Player;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author minus
 */
public class PlayListener extends AbstractPlaybackListener {

    @Override
    protected void _execute(Event event) {
        try {
            PlayEvent playEvent = (PlayEvent) event;
            if (playEvent.getSoundService() != null) {
                Player.getInstance().play(playEvent.getSoundService());
                return;
            }

            Player.getInstance().play();
        } catch (UnableToPlaySoundException ex) {
            Logger.getLogger(PlayPauseListener.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
