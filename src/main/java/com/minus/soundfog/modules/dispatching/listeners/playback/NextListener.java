package com.minus.soundfog.modules.dispatching.listeners.playback;

import com.minus.soundfog.exceptions.UnableToPlaySoundException;
import com.minus.soundfog.modules.eventsdispatcher.Event;
import com.minus.soundfog.modules.player.Player;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author minus
 */
public class NextListener extends AbstractPlaybackListener {

    @Override
    protected void _execute(Event event) {
        try {
            Player.getInstance().next();
        } catch (UnableToPlaySoundException ex) {
            Logger.getLogger(NextListener.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
