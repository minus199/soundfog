package com.minus.soundfog.modules.dispatching.events;

import com.minus.soundfog.modules.eventsdispatcher.Event;
import com.minus.soundfog.modules.eventsdispatcher.EventType;
import com.minus.soundfog.modules.playlist.LoaderType;
import com.minus.soundfog.modules.sounds.SoundService;
import java.util.List;

/**
 * @author minus
 */
public class FetchedBatchNormalizedEvent implements Event {

    private final List<SoundService> sounds;
    private final LoaderType loaderType;

    /**
     *
     * @param soundsCache
     * @param loaderType
     */
    public FetchedBatchNormalizedEvent(List<SoundService> soundsCache, LoaderType loaderType) {
        this.sounds = soundsCache;
        this.loaderType = loaderType;
    }

    public List<SoundService> getSounds() {
        return sounds;
    }

    public LoaderType getLoaderType() {
        return loaderType;
    }
    
    @Override
    public EventType getType() {
        return EventType.FETCHED_PAGE_BATCH_NORMALIZED_EVENT;
    }
}
