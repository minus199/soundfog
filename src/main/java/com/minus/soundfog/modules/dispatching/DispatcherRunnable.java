package com.minus.soundfog.modules.dispatching;

import com.minus.soundfog.modules.eventsdispatcher.Event;
import com.minus.soundfog.modules.eventsdispatcher.Listener;
import java.lang.reflect.Method;
import java.util.ArrayList;
import org.apache.logging.log4j.Logger;

/**
 * @author minus
 */
public class DispatcherRunnable implements Runnable {

    private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger(DispatcherRunnable.class.getName());

    private final Event event;
    private final ArrayList<Class<? extends Listener>> listeners;

    public DispatcherRunnable(Event event, ArrayList<Class<? extends Listener>> listeners) {
        this.event = event;
        this.listeners = listeners;
    }

    @Override
    public void run() {
        for (Class<? extends Listener> listenerKlazz : listeners) {
            try {
                logger.trace("Dispatching " + event.getType() + " with " + listenerKlazz.getSimpleName());
                Listener instance = listenerKlazz.newInstance();
                instance.execute(event);
            } catch (InstantiationException | IllegalAccessException ex) {
                logger.error(ex);
            } catch (ClassCastException ex) {
                logger.error(getClass().getName() + "::line 29 -> " + ex.getMessage(), ex);
            } catch (Exception ex){
                logger.error(ex);
            }
        }
    }
}
