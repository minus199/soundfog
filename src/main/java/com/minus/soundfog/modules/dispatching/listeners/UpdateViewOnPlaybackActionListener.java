package com.minus.soundfog.modules.dispatching.listeners;

import com.minus.soundfog.MainApp;
import com.minus.soundfog.modules.dispatching.events.playback.PlayEvent;
import com.minus.soundfog.modules.dispatching.events.playback.UpdateViewOnPlaybackActionEvent;
import com.minus.soundfog.modules.eventsdispatcher.Event;
import com.minus.soundfog.modules.eventsdispatcher.Listener;
import com.minus.soundfog.modules.player.Player;
import com.minus.soundfog.modules.sounds.SoundService;
import com.minus.soundfog.modules.ui.CurrentlyPlayingViewController;


/**
 * @author minus
 */
public class UpdateViewOnPlaybackActionListener implements Listener {

    private Event event;
    final private CurrentlyPlayingViewController currentlyPlayingViewController;

    public UpdateViewOnPlaybackActionListener() {
        currentlyPlayingViewController = MainApp.getInstance().getMainViewController().getCurrentlyPlayingViewController();
    }

    @Override
    public void execute(Event event) {
        UpdateViewOnPlaybackActionEvent e = (UpdateViewOnPlaybackActionEvent) event;
        this.event = e.getLastTriggeredEvent();

        switch (e.getLastTriggeredEvent().getType()) {
            case NEXT_EVENT:
                handleNext();
                break;
            case PAUSE_EVENT:
                handlePause();
                break;
            case PLAY_EVENT:
                handlePlay();
                break;
            case PREVIOUS_EVENT:
                handlePrevious();
                break;
            case SEEK_BACKWARDS_EVENT:
                handleSeekBackwards();
                break;
            case SEEK_FORWARD_EVENT:
                handleSeekForward();
                break;
            case STOP_EVENT:
                handleStop();
                break;
        }
    }
    
    private void handlePlay() {
        currentlyPlayingViewController.onPlayEvent(((PlayEvent) event).getSoundService());
    }

    private void handlePause() {
    }

    private void handleStop() {
        SoundService previouslyPlaying = Player.getInstance().getPreviouslyPlaying();
        if (previouslyPlaying != null) {
            currentlyPlayingViewController.toggleHighlight(previouslyPlaying);
        }
    }

    private void handleNext() {
        //playerView.onPlayEvent(new PlayEvent(((NextEvent) event).getSoundService()));
        //currentlyPlayingViewController.onPlayEvent(((PlayEvent) event).getSoundService());
        throw new RuntimeException("WTF");
    }

    private void handlePrevious() {
        //currentlyPlayingViewController.onPlayEvent(((PlayEvent) event).getSoundService());
        throw new RuntimeException("WTF");
    }

    private void handleSeekForward() {
    }

    private void handleSeekBackwards() {
    }
}
