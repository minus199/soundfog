package com.minus.soundfog.modules.dispatching.events.playback;

import com.minus.soundfog.modules.eventsdispatcher.Event;
import com.minus.soundfog.modules.eventsdispatcher.EventType;

/**
 * @author minus
 */
public class UpdateViewOnPlaybackActionEvent implements Event{
    final private Event lastTriggeredEvent;

    public UpdateViewOnPlaybackActionEvent(Event previousEvent) {
        this.lastTriggeredEvent = previousEvent;
    }
    
    @Override
    public EventType getType() {
        return EventType.HANDLE_VIEW_ON_PLAYBACK_ACTION_EVENT;
    }

    public Event getLastTriggeredEvent() {
        return lastTriggeredEvent;
    }
}
