package com.minus.soundfog.modules.dispatching.listeners;

import com.minus.soundfog.modules.concurrency.DefaultSoundClickActionRunnable;
import com.minus.soundfog.modules.dispatching.events.SoundClickedEvent;
import com.minus.soundfog.modules.eventsdispatcher.Event;
import com.minus.soundfog.modules.eventsdispatcher.Listener;

/**
 * @author minus
 */
public class SoundClickedListener implements Listener {

    @Override
    public void execute(Event event) {
        SoundClickedEvent e = (SoundClickedEvent) event;
        Thread thread = new Thread(new DefaultSoundClickActionRunnable(e.getClickSourceElement()));
        thread.start();
    }
}
