package com.minus.soundfog.modules.dispatching.listeners.playback;

import com.minus.soundfog.modules.dispatching.events.playback.PauseEvent;
import com.minus.soundfog.modules.dispatching.events.playback.PlayEvent;
import com.minus.soundfog.modules.dispatching.events.playback.PlayPauseEvent;
import com.minus.soundfog.modules.eventsdispatcher.Dispatcher;
import com.minus.soundfog.modules.eventsdispatcher.Event;
import com.minus.soundfog.modules.player.Player;

/**
 * @author minus
 */
public class PlayPauseListener extends AbstractPlaybackListener {
    @Override
    protected void _execute(Event event) {
        PlayPauseEvent playPauseEvent = (PlayPauseEvent) event;

        if (Player.getInstance().isPlaying() && Player.getInstance().currentlyPlaying().equals(playPauseEvent.getSoundService())) {
            Dispatcher.manage().dispatch(new PauseEvent(playPauseEvent.getSoundService()));
        } else {
            Dispatcher.manage().dispatch(new PlayEvent(playPauseEvent.getSoundService()));
        }
    }
}
