package com.minus.soundfog.modules.dispatching.events.playback;

import com.minus.soundfog.annotations.EventViewProperties;
import com.minus.soundfog.modules.eventsdispatcher.EventType;
import com.minus.soundfog.modules.sounds.SoundService;

/**
 * @author minus
 */
@EventViewProperties(keepAfterEvent = true)
public class PauseEvent extends AbstractPlaybackEvent {

    private final SoundService soundService;

    public PauseEvent() {
        this.soundService = null;
    }
    
    public PauseEvent(SoundService soundService) {
        this.soundService = soundService;
    }

    @Override
    public EventType getType() {
        return EventType.PAUSE_EVENT;
    }

    public SoundService getSoundService() {
        return soundService;
    }
}
