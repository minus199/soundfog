package com.minus.soundfog.modules.dispatching.listeners.playback;

import com.minus.soundfog.modules.dispatching.events.playback.UpdateViewOnPlaybackActionEvent;
import com.minus.soundfog.modules.eventsdispatcher.Dispatcher;
import com.minus.soundfog.modules.eventsdispatcher.Event;
import com.minus.soundfog.modules.eventsdispatcher.Listener;

/**
 * @author minus
 */
public abstract class AbstractPlaybackListener implements Listener{
    abstract protected void _execute(Event event);

    @Override
    public void execute(Event event) {
        _execute(event);
        Dispatcher.manage().dispatch(new UpdateViewOnPlaybackActionEvent(event));
    }
}
