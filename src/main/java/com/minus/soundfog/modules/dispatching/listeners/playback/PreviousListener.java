package com.minus.soundfog.modules.dispatching.listeners.playback;

import com.minus.soundfog.exceptions.UnableToPlaySoundException;
import com.minus.soundfog.modules.eventsdispatcher.Event;
import com.minus.soundfog.modules.player.Player;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author minus
 */
public class PreviousListener extends AbstractPlaybackListener {

    @Override
    protected void _execute(Event event) {
         try {
            Player.getInstance().previous();
        } catch (UnableToPlaySoundException ex) {
            Logger.getLogger(PreviousListener.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
