package com.minus.soundfog.modules.dispatching.events;

import com.minus.soundfog.modules.eventsdispatcher.EventType;
import javafx.scene.layout.HBox;
/**
 * @author minus
 */
public class SoundOnPlayClickedEvent extends SoundClickedEvent{

    public SoundOnPlayClickedEvent(HBox clickSourceElement) {
        super(clickSourceElement);
    }

    @Override
    public EventType getType() {
        return EventType.SOUND_ON_PLAY_CLICKED_EVENT;
    }
}
