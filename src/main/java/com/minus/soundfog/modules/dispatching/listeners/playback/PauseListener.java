package com.minus.soundfog.modules.dispatching.listeners.playback;

import com.minus.soundfog.modules.eventsdispatcher.Event;
import com.minus.soundfog.modules.player.Player;

/**
 * @author minus
 */
public class PauseListener extends AbstractPlaybackListener {

    @Override
    protected void _execute(Event event) {
        Player.getInstance().pause();
    }
}
