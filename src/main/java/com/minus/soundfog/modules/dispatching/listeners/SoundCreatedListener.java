package com.minus.soundfog.modules.dispatching.listeners;

import com.minus.soundfog.modules.dispatching.events.SoundCreatedEvent;
import com.minus.soundfog.modules.eventsdispatcher.Event;
import com.minus.soundfog.modules.eventsdispatcher.Listener;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author minus
 */
public class SoundCreatedListener implements Listener {
    private static final org.apache.logging.log4j.Logger logger = org.apache.logging.log4j.LogManager.getLogger(SoundCreatedListener.class.getName());
    @Override
    public void execute(Event event) {
        SoundCreatedEvent e = (SoundCreatedEvent) event;
        assert e.getModelSound().getId() > 0 : "Failed to save " + e.getModelSound().getTitle();
        logger.debug(e.getLikeDescriptor().getTitle() + " was created.");
    }
}
