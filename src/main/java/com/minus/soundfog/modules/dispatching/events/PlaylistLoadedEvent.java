package com.minus.soundfog.modules.dispatching.events;

import com.minus.soundfog.modules.eventsdispatcher.Event;
import com.minus.soundfog.modules.eventsdispatcher.EventType;
import com.minus.soundfog.modules.playlist.Playlist;

/**
 * @author MiNuS420 <minus199@gmail.com>
 */
public class PlaylistLoadedEvent implements Event {

    private final Playlist activeList;

    /**
     *
     * @param activeList
     */
    public PlaylistLoadedEvent(Playlist activeList) {
        this.activeList = activeList;

    }

    @Override
    public EventType getType() {
        return EventType.PLAYLIST_LOADED_EVENT;
    }

    public Playlist getActiveList() {
        return activeList;
    }

}
