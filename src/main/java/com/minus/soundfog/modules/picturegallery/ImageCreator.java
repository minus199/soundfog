package com.minus.soundfog.modules.picturegallery;

import com.minus.soundfog.exceptions.ArtworkNotFoundException;
import com.minus.soundfog.modules.db.DB;
import com.minus.soundfog.modules.db.models.ModelImage;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

/**
 * @author minus
 */
public class ImageCreator {
    final private static String CONFIG_ICON_PREFIX = "icon.";
    
    private ImageCreator() throws SQLException, IOException {
    }
    
    public static Picture makeArtwork(URL url) throws IOException{
        ModelImage _persistImage = createModelImg(url, ImgDescendantClass.ARTWORK);
        if (_persistImage != null){
            Picture picture = new Picture(_persistImage);
            return picture;
        }
        
        return null;
    }
    
    public static Picture makeWaveform(URL url) throws IOException{
        ModelImage _persistImage = createModelImg(url, ImgDescendantClass.WAVEFORM);
        if (_persistImage != null){
            Picture picture = new Picture(_persistImage);
            return picture;
        }
        
        return null;
    }
    
    public static Picture makeAvatar(URL url) throws IOException {
        ModelImage _persistImage = createModelImg(url, ImgDescendantClass.ARTIST_AVATAR);
        if (_persistImage != null){
            Picture picture = new Picture(_persistImage);
            return picture;
        }
        
        return null;
    }
    
    public static MenuIcon makeIcon(String icon) throws ArtworkNotFoundException {
        String iconPath = "/icons/" + icon;
        
        try {
            return new MenuIcon(createModelImg(ImageCreator.class.getResource(iconPath), ImgDescendantClass.ICON));
        } catch (IOException ex) {
            throw new ArtworkNotFoundException();
        }
    }

    private static ModelImage createModelImg(URL pictureURL, ImgDescendantClass descendantClass) throws IOException {
//        BufferedImage img = ImageIO.read(pictureURL);
        byte[] bytesArray = getBytesArray(pictureURL);
        
        ModelImage modelImage = new ModelImage(generateHash(bytesArray), bytesArray, descendantClass);
        modelImage.save();
        return (ModelImage)DB.getInstance().save(modelImage, true);
    }

    private static String generateHash(byte[] bytes) {
        return Integer.toString(Math.abs(Arrays.hashCode(bytes)));
    }

    private static byte[] getBytesArray(URL url) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        InputStream is = url.openStream();

        byte[] byteChunk = new byte[4096];
        int n;

        while ((n = is.read(byteChunk)) > 0) {
            baos.write(byteChunk, 0, n);
        }

        if (is != null) {
            is.close();
        }

        return baos.toByteArray();
    }
}
