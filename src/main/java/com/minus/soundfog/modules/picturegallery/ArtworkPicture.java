package com.minus.soundfog.modules.picturegallery;

import com.minus.soundfog.modules.db.models.ModelImage;
import java.io.IOException;

/**
 * @author minus
 */
public class ArtworkPicture extends Picture{

    public ArtworkPicture(ModelImage modelImage) throws IOException {
        super(modelImage);
        
    }
}
