package com.minus.soundfog.modules.picturegallery;

import com.minus.soundfog.modules.db.models.ModelImage;
import java.io.IOException;

/**
 * @author minus
 */
public class MenuIcon extends Picture{
    public MenuIcon(ModelImage modelImage) throws IOException {
        super(modelImage);
        getImageView().setFitWidth(20);
    }
}
