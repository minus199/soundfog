package com.minus.soundfog.modules.picturegallery;

/**
 *
 * @author minus
 */
public enum ImgDescendantClass {
    ARTWORK, WAVEFORM, ARTIST_AVATAR, ICON;
}
