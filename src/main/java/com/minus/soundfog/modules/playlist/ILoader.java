package com.minus.soundfog.modules.playlist;

/**
 * @author MiNuS420 <minus199@gmail.com>
 */
public interface ILoader {

    int getFetchLoadSizeLimit();

    LoaderType getType();

    boolean load();
    
}
