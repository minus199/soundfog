package com.minus.soundfog.modules.playlist;

import com.minus.soundfog.MainApp;
import com.minus.soundfog.exceptions.UnableToNormalizeSoundsException;
import com.minus.soundfog.modules.concurrency.rawsoundsprocessor.RawSoundsProcessorManager;
import com.minus.soundfog.modules.db.DB;
import com.minus.soundfog.modules.db.models.ModelSound;
import java.util.List;

/**
 * @author minus
 */
class SearchResultLoader extends AbstractLoader {

    private SearchResultLoader() {
    }

    
    final private static class InstanceContainer {
        final private static SearchResultLoader instance = new SearchResultLoader();
    }
    
    public static final SearchResultLoader getInstance() {
        return InstanceContainer.instance;
    }

    @Override
    protected void normalizeToSoundService() throws UnableToNormalizeSoundsException {
        String searchTerm = MainApp.getInstance().getMainViewController().getSearchTerm();
        super.fetch(DB.getInstance().fulltext(searchTerm));
        RawSoundsProcessorManager.getInstance().terminate();
    }

    @Override
    Playlist getPlayList() {
        return SearchResultPlaylist.getInstance();
    }

    @Override
    protected Boolean isPersistanceNeed() {
        return false;
    }

    @Override
    protected Boolean isPaginated() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getFetchLoadSizeLimit() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public LoaderType getType() {
        return LoaderType.SEARCH_RESULT;
    }

}
