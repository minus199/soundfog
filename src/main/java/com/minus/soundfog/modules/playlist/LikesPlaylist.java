package com.minus.soundfog.modules.playlist;

/**
 * @author minus
 */
public class LikesPlaylist extends SoundCloudPlaylist {
    private LikesPlaylist() {
        super(LikesLoader.class);
    }

    private static class InstanceContainer {
        private static final LikesPlaylist instance = new LikesPlaylist();
    }

    public static Playlist getInstance() {
        return InstanceContainer.instance;
    }
}
