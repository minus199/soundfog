package com.minus.soundfog.modules.playlist;

/**
 * @author minus
 */
public class ActivitiesPlaylist extends SoundCloudPlaylist {

    private ActivitiesPlaylist() {
        super(ActivityLoader.class);
    }

    private static class InstanceContainer {
        private static final ActivitiesPlaylist instance = new ActivitiesPlaylist();
    }

    public static Playlist getInstance() {
        return InstanceContainer.instance;
    }
}
