package com.minus.soundfog.modules.playlist;

import com.minus.soundfog.exceptions.UnableToNormalizeSoundsException;
import java.io.IOException;

/**
 * @author minus
 */
class ActivityLoader extends AbstractSoundcloudLoader {

    final private int PAGE_SIZE = 50;

    private ActivityLoader() {
    }
    
    final private static class InstanceContainer {

        final private static ActivityLoader instance = new ActivityLoader();
    }
    
    public static final ActivityLoader getInstance() {
        return InstanceContainer.instance;
    }
    
    @Override
    protected String getSoundcloudPath() {
        return "/me/activities";
    }

    @Override
    protected void normalizeToSoundService() throws UnableToNormalizeSoundsException {
        sync();
    }

    @Override
    Playlist getPlayList() {
        return ActivitiesPlaylist.getInstance();
    }

    @Override
    final protected Boolean isPersistanceNeed() {
        return false;
    }

    @Override
    protected Boolean isPaginated() {
        return false;
    }

    @Override
    public int getFetchLoadSizeLimit() {
        return PAGE_SIZE;
    }

    @Override
    public LoaderType getType() {
        return LoaderType.ACTIVITIES;
    }
}
