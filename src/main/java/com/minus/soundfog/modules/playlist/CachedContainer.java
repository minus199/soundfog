package com.minus.soundfog.modules.playlist;

import com.minus.soundfog.modules.sounds.SoundService;
import java.util.HashMap;
import javafx.scene.layout.HBox;

/**
 *
 * @author minus
 */
abstract public class CachedContainer extends HashMap<SoundService, HBox> implements ISoundcloundPlaylistable {

    @Override
    abstract public Playlist getPlaylist();

    final public Class<? extends Playlist> getPlayListKlazz() {
        return getPlaylist().getClass();
    }

    public void pushToPlaylist() {
        ActivePlaylist.getInstance().putAll(this);
    }
}
