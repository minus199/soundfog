package com.minus.soundfog.modules.playlist;

/**
 * @author minus
 */
public class SearchResultCachedContainer extends CachedContainer {

    @Override
    public SearchResultPlaylist getPlaylist() {
        return SearchResultPlaylist.getInstance();
    }

}
