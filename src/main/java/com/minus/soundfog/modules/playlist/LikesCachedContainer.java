package com.minus.soundfog.modules.playlist;

/**
 * @author minus
 */
public class LikesCachedContainer extends CachedContainer{

    @Override
    public SoundCloudPlaylist getPlaylist() {
        return (SoundCloudPlaylist) LikesPlaylist.getInstance();
    }

}
