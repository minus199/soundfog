package com.minus.soundfog.modules.playlist;

import com.minus.soundfog.exceptions.UnableToNormalizeSoundsException;
import com.minus.soundfog.modules.concurrency.rawsoundsprocessor.RawSoundsProcessorManager;
import com.minus.soundfog.modules.db.models.BaseModel;
import com.minus.soundfog.modules.db.models.ModelSound;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author minus
 */
abstract class AbstractLoader implements ILoader {

    private static final org.apache.logging.log4j.Logger logger = org.apache.logging.log4j.LogManager.getLogger(AbstractLoader.class.getName());

    private final ExecutorService newSingleThreadExecutor;

    abstract Playlist getPlayList();

    abstract protected Boolean isPersistanceNeed();

    abstract protected Boolean isPaginated();

    protected AbstractLoader() {
        newSingleThreadExecutor = Executors.newSingleThreadExecutor();
    }

    @Override
    final public boolean load() {
        newSingleThreadExecutor.submit(() -> {
            RawSoundsProcessorManager.getInstance().restartPool();
            normalizeToSoundService();
        });
        return true;
    }

    abstract protected void normalizeToSoundService() throws UnableToNormalizeSoundsException;

    protected void fetch(List<? extends BaseModel> models) {
        int totalItemsCount = models.size();
        int partitionsCount = Math.max((int) (models.size() / 5), 1);
        int itemPerPartition = Math.min((int) totalItemsCount / partitionsCount, models.size());

        for (int i = 0; i < partitionsCount; i++) {
            try {
                int indexBottom = i * itemPerPartition;
                int indexTop = (i + 1) * itemPerPartition;
                RawSoundsProcessorManager.getInstance()
                        .processBatch(models.subList(indexBottom, indexTop), getType());

                logger.trace(indexBottom + " to " + indexTop + " sent to processing...");
            } catch (Exception e) {
                logger.error("Error while processing batch from DB: " + e.getMessage(), e);
            }
        }

        int itemPerLastPartition = (int) totalItemsCount % partitionsCount;
        if (itemPerLastPartition == 0) {
            return;
        }

        int bottomLast = (partitionsCount * itemPerPartition);
        int topLast = bottomLast + itemPerLastPartition;
        RawSoundsProcessorManager.getInstance()
                .processBatch(models.subList(bottomLast, topLast), getType())
                .terminate();
        
        logger.trace(bottomLast + " to " + topLast + " sent to processing... -- RawSoundsProcessorManager terminated.");
    }
}
