package com.minus.soundfog.modules.playlist;

import com.minus.soundfog.modules.dispatching.events.PlaylistLoadedEvent;
import com.minus.soundfog.modules.eventsdispatcher.Dispatcher;
import java.util.Objects;
import org.apache.logging.log4j.Logger;

/**
 * @author MiNuS420 <minus199@gmail.com>
 */
final public class ActivePlaylist {

    private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger(ActivePlaylist.class.getName());

    private static Playlist activeList;
    private static Playlist previousList;

    public static final Playlist getInstance() {
        return activeList;
    }

    /**
     * Switches the instance to the previously selected playlist while keeping
     * reference to the current playlist. thus, the current playlist becomes the
     * previous playlist and the previous playlist becomes the active one
     *
     *
     * @return the instance of the active(previously selected) instance
     */
    public static Playlist toggleLastList() {
        if (previousList != null) {
            Playlist activeListCurrent = activeList;
            activeList = previousList;
            previousList = activeListCurrent;
        } else {
            logger.warn("Tried to toggle active playlist while previous list is null -- continuing with current playlist.");
        }

        return getInstance();
    }

    final synchronized static void load(Playlist toBeActiveList) {
        if (toBeActiveList == null) {
            throw new NullPointerException("List must not be null.");
        }

        /* In order to make sure that there wont be a situation of the same active and previous list */
        if (activeList != null && (previousList == null || !activeList.getClass().equals(previousList.getClass()))) {
            previousList = activeList;
        }
        activeList = toBeActiveList;

        Dispatcher.manage().dispatch(new PlaylistLoadedEvent(activeList));
    }
}
