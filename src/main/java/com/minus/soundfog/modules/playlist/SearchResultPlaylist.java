package com.minus.soundfog.modules.playlist;

/**
 * @author minus
 */
public class SearchResultPlaylist extends Playlist{

    private SearchResultPlaylist() {
        super(SearchResultLoader.class);
    }

    private static final class InstanceContainer{
        private final static SearchResultPlaylist instance = new SearchResultPlaylist();
    }
    
    final public static SearchResultPlaylist getInstance(){
        return InstanceContainer.instance;
    }

    @Override
    protected void beforeLoad() {
        clear();
    }
}
