package com.minus.soundfog.modules.playlist;

import com.minus.soundfog.exceptions.UnsupportedLoaderException;
import com.minus.soundfog.modules.sounds.SoundService;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;
import javafx.scene.layout.HBox;

/**
 * @author minus
 */
abstract public class Playlist implements Map<SoundService, HBox> {

    private static final org.apache.logging.log4j.Logger logger = org.apache.logging.log4j.LogManager.getLogger(Playlist.class.getName());

    final private TreeMap<SoundService, HBox> delegatee = new TreeMap();
    final private Class<? extends AbstractLoader> loaderKlazz;
    private SoundService currentSound;
    private HBox currentElement;
    private Paginator paginator;

    private final PropertyChangeSupport mPcs = new PropertyChangeSupport(this);

    protected Playlist(Class<? extends AbstractLoader> loaderKlazz) {
        this.loaderKlazz = loaderKlazz;
    }

    protected void beforeLoad(){
        
    }
    
    final public void load() throws UnsupportedLoaderException {
        try {
            beforeLoad();
            Method getInstance = loaderKlazz.getMethod("getInstance");
            AbstractLoader loader = (AbstractLoader) getInstance.invoke(null);

            if (paginator == null) {
                paginator = new Paginator(this);
                addListener(paginator);
            }

            ActivePlaylist.load(this);

            loader.load();
        } catch (IllegalAccessException | NoSuchMethodException | SecurityException | IllegalArgumentException | InvocationTargetException ex) {
            throw new UnsupportedLoaderException("Loader of type " + loaderKlazz.getName() + " is not implemented correctly: " + ex.getMessage(), ex);
        }
    }

    final public void refresh() {
        load();
    }

    public SoundService getCurrentSound() {
        return currentSound;
    }

    public HBox getCurrentElement() {
        return currentElement;
    }

    public Map.Entry<SoundService, HBox> getPrevious() {
        return delegatee.lowerEntry(currentSound);
    }

    public Map.Entry<SoundService, HBox> getNext() {
        return delegatee.higherEntry(currentSound);
    }

    public void setCurrent(SoundService soundService) {
        this.currentSound = soundService;
        this.currentElement = this.get(soundService);
    }

    public void setCurrent(HBox current) {
        this.currentElement = current;
        currentSound = getSoundByElement(current);
    }

    final public SoundService getSoundByElement(HBox element) {
        Set<SoundService> keysByValue = getKeysByValue(this, element);
        return keysByValue.stream().findFirst().get();
    }

    public void update(SoundService currentlyPlaying) {
        setCurrent(currentlyPlaying);
    }

    public Set<SoundService> getSounds() {
        return keySet();
    }

    public Collection<HBox> getElements() {
        return values();
    }

    private static <T, E> Set<T> getKeysByValue(Map<T, E> map, E value) {
        return map.entrySet()
                .stream()
                .filter(entry -> Objects.equals(entry.getValue(), value))
                .map(Map.Entry::getKey)
                .collect(Collectors.toSet());
    }

    public LoaderType getLoaderType() {
        try {
            AbstractLoader loader = loaderKlazz.newInstance();
            return loader.getType();
        } catch (InstantiationException | IllegalAccessException ex) {
            logger.error(getClass().getName() + ".getLoaderType() error");
            return null;
        }
    }

    public Playlist addListener(PropertyChangeListener listener) {
        mPcs.addPropertyChangeListener(listener);
        size();
        return this;
    }

    public Playlist removeListener(PropertyChangeListener listener) {
        mPcs.removePropertyChangeListener(listener);

        return this;
    }

    @Override
    public int size() {
        return delegatee.size();
    }

    @Override
    public boolean isEmpty() {
        return delegatee.isEmpty();
    }

    @Override
    public boolean containsKey(Object key) {
        return delegatee.containsKey(key);
    }

    @Override
    public boolean containsValue(Object value) {
        return delegatee.containsValue(value);
    }

    @Override
    public HBox get(Object key) {
        return delegatee.get(key);
    }

    @Override
    public HBox put(SoundService key, HBox value) {
        int previousSize = size();
        HBox remove = delegatee.put(key, value);
        mPcs.firePropertyChange("size", previousSize, size());
        logger.info("Putting to playlist: " + previousSize + " / " + size());
        return remove;
    }

    @Override
    public HBox remove(Object key) {
        int previousSize = size();
        HBox remove = delegatee.remove(key);
        mPcs.firePropertyChange("size", previousSize, size());
        logger.info("Removing from playlist: " + previousSize + " / " + size());
        return remove;
    }

    @Override
    public void putAll(Map<? extends SoundService, ? extends HBox> m) {
        int previousSize = size();
        delegatee.putAll(m);
        mPcs.firePropertyChange("size", previousSize, size());
        logger.info("Putting all to playlist: " + previousSize + " / " + size());
    }

    @Override
    public void clear() {
        int previousSize = size();
        delegatee.clear();
        mPcs.firePropertyChange("size", previousSize, size());
        logger.info("Clearing playlist: " + previousSize + " / " + size());
    }

    @Override
    public Set<SoundService> keySet() {
        return delegatee.keySet();
    }

    @Override
    public Collection<HBox> values() {
        return delegatee.values();
    }

    @Override
    public Set<Entry<SoundService, HBox>> entrySet() {
        return delegatee.entrySet();
    }

    public SoundService firstKey() {
        return delegatee.firstKey();
    }

    public Paginator getPaginator() {
        return paginator;
    }
}
