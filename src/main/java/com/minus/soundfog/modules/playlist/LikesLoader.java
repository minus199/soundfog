package com.minus.soundfog.modules.playlist;

import com.minus.soundfog.exceptions.UnableToNormalizeSoundsException;
import com.minus.soundfog.modules.concurrency.rawsoundsprocessor.RawSoundsProcessorManager;
import com.minus.soundfog.modules.db.dto.SoundDto;

/**
 * @author minus
 */
class LikesLoader extends AbstractSoundcloudLoader {

    final private int PAGE_SIZE = 40;
    private static final org.apache.logging.log4j.Logger logger = org.apache.logging.log4j.LogManager.getLogger(LikesLoader.class.getName());

    private LikesLoader() {
    }

    final private static class InstanceContainer {

        final private static LikesLoader instance = new LikesLoader();
    }

    public static final LikesLoader getInstance() {
        return InstanceContainer.instance;
    }

    @Override
    protected String getSoundcloudPath() {
        return "/me/favorites";
    }

    @Override
    protected void normalizeToSoundService() throws UnableToNormalizeSoundsException {
        super.fetch(SoundDto.getInstance().getAll());
        //sync();
        RawSoundsProcessorManager.getInstance().terminate();
    }

    @Override
    Playlist getPlayList() {
        return LikesPlaylist.getInstance();
    }

    @Override
    protected Boolean isPersistanceNeed() {
        return true;
    }

    @Override
    protected Boolean isPaginated() {
        return false;
    }

    @Override
    public int getFetchLoadSizeLimit() {
        return PAGE_SIZE;
    }

    @Override
    public LoaderType getType() {
        return LoaderType.LIKES;
    }
}
