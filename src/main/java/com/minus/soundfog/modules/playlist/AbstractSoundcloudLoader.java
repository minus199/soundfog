package com.minus.soundfog.modules.playlist;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.minus.soundfog.MainApp;
import com.minus.soundfog.modules.GenericComperableKeyValuePair;
import com.minus.soundfog.modules.concurrency.rawsoundsprocessor.RawSoundsProcessorManager;
import com.minus.soundfog.modules.db.dto.FetchedPageDto;
import com.minus.soundfog.modules.db.models.BaseModel;
import com.minus.soundfog.modules.db.models.ModelFetchedPages;
import static com.minus.soundfog.modules.ui.MainViewController.clientID;
import static com.minus.soundfog.modules.ui.MainViewController.clientSecret;
import com.soundcloud.api.ApiWrapper;
import com.soundcloud.api.Http;
import com.soundcloud.api.Request;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.TreeMap;
import java.util.function.Consumer;
import java.util.stream.Stream;

/**
 * @author minus
 */
abstract class AbstractSoundcloudLoader extends AbstractLoader {

    private static final org.apache.logging.log4j.Logger logger = org.apache.logging.log4j.LogManager.getLogger(AbstractSoundcloudLoader.class.getName());

    protected String nextCursor;
    final protected List<BaseModel> fetchedPages = FetchedPageDto.getInstance().getAll();
    final protected HashMap<Integer, BaseModel> tmap = new HashMap<>();

    private ApiWrapper wrapper;

    final private ArrayList cursors;
    private final LinkedList<GenericComperableKeyValuePair> chronologicallyOrderedSoundsMeta = new LinkedList();

    public AbstractSoundcloudLoader() {
        Object cursor = MainApp.getProperties().get("soundcloud.cursors");

        if (cursor == null) {
            MainApp.getProperties().put("soundcloud.cursors", new ArrayList<>());
        }

        cursors = (ArrayList) MainApp.getProperties().get("soundcloud.cursors");
    }

    abstract protected String getSoundcloudPath();

    protected JsonArray sync() {
        JsonArray fetchSoundsResponse = null;

        try {
            fetchSoundsResponse = makeRequest((String) MainApp.getProperties().get("nextCursor"), 1, "1");
        } catch (Exception ex) {
            logger.error("Exception during sounds syncing", ex);
            fetchSoundsResponse = null;
        }

        return fetchSoundsResponse;
    }

    private JsonArray makeRequest(String cursor, Integer pageNumber, String linkedPartitioning) throws IOException {
        return makeRequest(cursor, pageNumber, linkedPartitioning, new JsonArray());
    }

    private JsonArray makeRequest(String cursor, Integer pageNumber, String linkedPartitioning, JsonArray currentSounds) throws IOException {
        logger.info("Processing request -- Current page number: {0}", pageNumber);

        String formatJSON = null;
        formatJSON = Http.formatJSON(Http.getString(getWrapper().get(prepareRequest(pageNumber, cursor, linkedPartitioning))));
        JsonObject responseJsonObject = (JsonObject) new JsonParser().parse(formatJSON);
        JsonElement nextHref = responseJsonObject.get("next_href");
        HashMap<String, Object> nextIterationArgs = parseNextIterationArgs(nextHref);

        JsonArray currentCollection = (JsonArray) responseJsonObject.get("collection");
        keepOrder(currentCollection);

        ModelFetchedPages modelFetchedPages = null;

        RawSoundsProcessorManager.getInstance().processBatch(currentCollection, getType(), modelFetchedPages);

        if (nextIterationArgs.get("cursor") != null) {
            cursors.add(nextIterationArgs.get("cursor").toString());
        }

        if (isPaginated() || nextHref == null || cursors.contains(nextHref)) {
            return currentSounds;
        }

        return makeRequest(
                nextIterationArgs.get("cursor").toString(),
                ++pageNumber,
                (String) nextIterationArgs.get("linked_partitioning"),
                currentSounds
        );
    }

    private void keepOrder(JsonArray currentCollection) {
        for (JsonElement element : currentCollection) {
            JsonObject jo = (JsonObject) element;
            GenericComperableKeyValuePair orderedElement = new GenericComperableKeyValuePair(
                    jo.get("id").getAsLong(),
                    jo.get("title").getAsString()
            );
            synchronized (this) {
                chronologicallyOrderedSoundsMeta.add(orderedElement);
            }

            jo.add("chronologicall_order", new JsonPrimitive(chronologicallyOrderedSoundsMeta.indexOf(orderedElement)));
        }
    }

    private HashMap<String, Object> parseNextIterationArgs(JsonElement nextHref) {
        HashMap<String, Object> nextIterationArgs = new HashMap<>();

        if (nextHref == null) {
            return nextIterationArgs;
        }

        String[] nextSoundURIParams = nextHref.getAsString().split("\\?")[1].split("&");
        for (String nextSoundURIParam : nextSoundURIParams) {
            String[] currentKeyValue = nextSoundURIParam.split("=");
            nextIterationArgs.put(currentKeyValue[0], currentKeyValue[1]);
        }

        return nextIterationArgs;
    }

    private Request prepareRequest(Integer pageNumber, String cursor, String linkedPartitioning) {
        Request request = Request.to(getSoundcloudPath());
        request.add("limit", getFetchLoadSizeLimit());
        request.add("page_number", pageNumber);
        request.add("client_id", clientID);
        //request.add("order", "createdAt");
        if (cursor != null) {
            request.add("cursor", cursor);
        }
        request.add("linked_partitioning", linkedPartitioning);
        return request;
    }

    final protected ApiWrapper getWrapper() throws IOException {
        if (wrapper == null) {
            wrapper = new ApiWrapper(clientID, clientSecret, null, null);
            wrapper.login("minus199@gmail.com", "jhubhjhubh");
        }

        return wrapper;
    }

    public ArrayList getCursors() {
        return cursors;
    }

}
