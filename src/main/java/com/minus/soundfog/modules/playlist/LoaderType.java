package com.minus.soundfog.modules.playlist;

/**
 * @author minus
 */
public enum LoaderType {
    ACTIVITIES,
    LIKES, 
    SEARCH_RESULT;
}
