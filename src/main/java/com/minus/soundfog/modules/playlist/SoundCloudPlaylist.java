package com.minus.soundfog.modules.playlist;

/**
 * @author minus
 */
abstract class SoundCloudPlaylist extends Playlist {

    final public static Integer PAGE_SIZE_LIMIT = 50;

    public SoundCloudPlaylist(Class<? extends AbstractLoader> loaderKlazz) {
        super(loaderKlazz);
    }
}
