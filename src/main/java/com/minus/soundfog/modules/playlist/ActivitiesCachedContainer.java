package com.minus.soundfog.modules.playlist;

/**
 * @author minus
 */
public class ActivitiesCachedContainer extends CachedContainer{

    @Override
    final public SoundCloudPlaylist getPlaylist() {
        return (SoundCloudPlaylist) ActivitiesPlaylist.getInstance();
    }
}
