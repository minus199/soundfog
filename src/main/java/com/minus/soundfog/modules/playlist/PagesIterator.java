package com.minus.soundfog.modules.playlist;

import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.NavigableMap;
import java.util.NoSuchElementException;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentSkipListMap;
import org.apache.logging.log4j.Logger;

/**
 * @author MiNuS420 <minus199@gmail.com>
 */
final class PagesIterator implements Iterator {
    private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger(PagesIterator.class.getName());

    private final NavigableMap<Integer, List> pages;
    private Integer current;
    Comparator<Integer> maxComp = (Integer int1, Integer int2) -> Integer.compare(int1, int2);

    PagesIterator() {
        this.pages = new ConcurrentSkipListMap();
    }

    public Integer current() {
        return current;
    }
    
    public List getCurrentPage(){
        return pages.get(current);
    }

    @Override
    public boolean hasNext() {
        return current < pages.lastKey();
    }

    @Override
    public Integer next() throws NoSuchElementException {
        if (!hasNext()) {
            reset();
        } else {
            current++;
        }
        
        return current;
    }

    public Integer previous() {
        if (!hasPrevious()) {
            end();
        } else {
            current--;
        }

        return current;
    }

    public boolean hasPrevious() {
        return current > 1;
    }

    public List peek() {
        return pages.get(current + 1);
    }

    public List peekPrevious() {
        return pages.get(current - 1);
    }

    @Override
    public void remove() throws NoSuchElementException {
        throw new UnsupportedOperationException("Not allowed to modify."); //To change body of generated methods, choose Tools | Templates.
    }

    public Integer setIndex(Object o) {
        if (pages.get((int) o) == null) {
            throw new IndexOutOfBoundsException();
        }

        return current = (Integer) o;
    }

    public void reset() {
        setIndex(1);
    }

    public Integer end() {
        return setIndex(pages.lastKey());
    }
    
    public void putPages(TreeMap<Integer, List> pages){
        this.pages.putAll(pages);
    }

    void putPage(int i, List<Object> subList) {
        this.pages.put(i, subList);
    }
    
    public int size(){
        return pages.size();
    }
}
