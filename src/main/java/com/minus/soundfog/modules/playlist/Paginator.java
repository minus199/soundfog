package com.minus.soundfog.modules.playlist;

import com.minus.soundfog.MainApp;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import javafx.application.Platform;
import javafx.scene.layout.HBox;
import org.hibernate.HibernateException;

/**
 * @author MiNuS420 <minus199@gmail.com>
 */
public class Paginator implements Iterable<Integer>, PropertyChangeListener {

    private static final org.apache.logging.log4j.Logger logger = org.apache.logging.log4j.LogManager.getLogger(Paginator.class.getName());
    final private PagesIterator iterator;
    final public static int pageSize = 18;
    private Long totalCount = 0L;
    private int lastPageNumber;

    private final Map<Integer, HBox> elementByPage = new HashMap<>();
    private ExecutorService waitForIteratorUpdate;
    private final Playlist playlist;

    Paginator(Playlist playlist) {
        iterator = new PagesIterator();
        this.playlist = playlist;
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        synchronized (this) {
            waitForIteratorUpdate = waitForIteratorUpdate == null ? Executors.newSingleThreadExecutor() : waitForIteratorUpdate;
            long newCount = ((Integer) evt.getNewValue()).longValue();
            if (totalCount == newCount) {
                return;
            }

            waitForIteratorUpdate.submit(() -> {
                totalCount = newCount;
                countFromPlaylist();
            });
        }
    }

    final public Integer getLastPage() {
        return lastPageNumber;
    }

    private boolean countFromPlaylist() throws HibernateException {
        if (ActivePlaylist.getInstance() != null && !ActivePlaylist.getInstance().getElements().isEmpty()) {
            int lastBatch = (int) (totalCount % pageSize);
            lastPageNumber = (int) ((totalCount / pageSize)) + 1;

            logger.info("Current page count: " + iterator.size());

            for (int i = 1; i <= lastPageNumber; i++) {
                int from = pageSize * (i - 1);
                int to = i == lastPageNumber ? from + lastBatch : pageSize * i;
                iterator.putPage(i, Arrays.asList(ActivePlaylist.getInstance().values().toArray()).subList(from, to));

                if (i == 1) {
                    iterator.setIndex(i);

                    if (ActivePlaylist.getInstance().getPaginator().getCurrent() == 1) {
                        ActivePlaylist.getInstance().getPaginator().jumpToFirst();
                    }
                }
            }
            
            return true;
        }

        return false;
    }

    public void fetchPrevious() {
        fetchPage();
        iterator.previous();
    }

    public void fetchNext() {
        fetchPage();
        iterator.next();
    }

    public Integer getCurrent() {
        return iterator.current();
    }

    private void fetchPage() throws HibernateException {
        Platform.runLater(() -> {
            try {
                MainApp.getInstance().getMainViewController().putSoundContainers((List<HBox>) iterator.getCurrentPage());
            } catch (Exception e) {

            }
        });
    }

    @Override
    public Iterator<Integer> iterator() {
        return iterator;
    }

    public void jumpToFirst() {
        iterator.reset();
        fetchPage();
    }

    public void JumpTo(int parseInt) {
        iterator.setIndex(parseInt);
        fetchPage();
    }
}
