package com.minus.soundfog.modules;

/**
 * @author MiNuS420 <minus199@gmail.com>
 * @param <K>
 * @param <V>
 */
public class GenericComperableKeyValuePair<K extends Object & Comparable, V extends Object> {

    private final K key;
    private final V value;

    public GenericComperableKeyValuePair(K key, V value) {
        this.key = key;
        this.value = value;
    }

    public K getKey() {
        return key;
    }

    public V getValue() {
        return value;
    }

}
