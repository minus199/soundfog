package com.minus.soundfog.modules.db;

import com.minus.soundfog.annotations.DtoMeta;
import com.minus.soundfog.exceptions.DBBootException;
import com.minus.soundfog.modules.db.models.BaseModel;
import com.minus.soundfog.modules.db.models.ModelSound;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.Entity;
import javax.persistence.EntityManagerFactory;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.type.UrlType;
import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;
import org.reflections.Reflections;

/**
 * @author minus
 */
final public class DB {
    private static final org.apache.logging.log4j.Logger logger = org.apache.logging.log4j.LogManager.getLogger(DB.class.getName());
    private SessionFactory sessionFactory;

    private EntityManagerFactory entityManagerFactory;
    final private Connection connection;
    private DSLContext dslContext;

    private static class InstanceHolder {

        final private static DB instance = new DB();
    }

    public synchronized static DB getInstance() {
        return InstanceHolder.instance;
    }

    private DB() {
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            sessionFactory.close();
            while (!sessionFactory.isClosed()) {
                logger.trace("Attempting to close connection to DB...");
            }
            
            logger.trace("Connection to DB closed.");
        }));

        Configuration configuration = new Configuration();
        configuration.registerTypeOverride(new UrlType());

        Reflections reflections = new Reflections(getClass());
        final Set<Class<?>> classes = reflections.getTypesAnnotatedWith(Entity.class);
        classes.stream().forEach((next) -> {
            configuration.addAnnotatedClass(next);
        });

        configuration.configure("hibernate.cfg.xml");
        configuration.setProperty("hibernate.connection.url", "jdbc:hsqldb:file:data/SoundsDB");// => )rties();
        ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();
        try {
            sessionFactory = configuration.buildSessionFactory(serviceRegistry);
            connection = sessionFactory.openSession().doReturningWork((Connection cnctn) -> cnctn);
            dslContext = DSL.using(connection, SQLDialect.HSQLDB);
        } catch (Exception exception) {
            throw new DBBootException(exception);
        }
    }

    final public DSLContext getDslContext() {
        return dslContext;
    }

    public List<ModelSound> fulltext(String searchTerm) {
        List<ModelSound> lookUp = null;
        try (Session session = getSession()) {
            try {
                lookUp = new FullTextHelper().lookUp(session, searchTerm);
            } catch (SQLException ex) {
                logger.catching(ex);
            }
        }

        return lookUp;
    }

    private List _query(String queryString, HashMap<Object, Object> params) {
        Query query;
        try (Session session = getSession()) {
            query = session.createQuery(queryString);
            if (params != null) {
                for (Map.Entry<Object, Object> entrySet : params.entrySet()) {
                    String key = entrySet.getKey().toString();
                    query.setParameter(key, entrySet.getValue());
                }
            }

            return query.list();
        }
    }

    public BaseModel queryItem(String fieldName, Object arg) throws ClassNotFoundException {
        StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
        for (StackTraceElement stackTraceElement : stackTraceElements) {
            try {
                DtoMeta annotation = Class.forName(stackTraceElement.getClassName()).getAnnotation(DtoMeta.class);
                if (annotation == null) {
                    continue;
                }

                Class<? extends BaseModel> baseModel = annotation.baseModelClass();

                HashMap param = new HashMap();
                param.put(fieldName, arg);

                String queryString = "from "
                        .concat(baseModel.getName())
                        .concat(" where ")
                        .concat(fieldName + " = :" + arg);

                return (BaseModel) _query(queryString, param).get(0); // TODO: hackie, find better solution
            } catch (ClassNotFoundException classNotFoundException) {
                logger.catching(classNotFoundException);
            }
        }

        throw new ClassNotFoundException("Unable to locate Model for DTO");
    }

    final public List query(String queryString) {
        return _query(queryString, null);
    }

    final public List query(String queryString, HashMap<Object, Object> params) {
        return _query(queryString, params);
    }

    final public Session createSession() throws HibernateException {
        return sessionFactory.openSession();
    }

    final public Session getSession() throws HibernateException {
        return sessionFactory.openSession();
    }

    final public Transaction startTransaction() {
        Transaction transaction = getSession().getTransaction();
        transaction.begin();
        return transaction;
    }

    public BaseModel save(BaseModel model) {
        return save(model, false);
    }

    public BaseModel getByID(Class<? extends BaseModel> modelKlazz, int id) {
        try (Session session = getSession()) {
            return session.get(modelKlazz, id);
        }
    }

    public void save(boolean orUpdate, ArrayList<BaseModel> models) {
        try (Session session = getSession()) {
            Transaction transaction = session.beginTransaction();

            models.forEach(m -> {
                if (orUpdate) {
                    session.save(m);
                } else {
                    session.save(m);
                }
            });

            session.flush();
            transaction.commit();

            models.forEach(m -> session.refresh(m));
        }
    }

    public BaseModel save(BaseModel model, boolean orUpdate) {
        Session session = getSession();

        boolean hasError = false;

        Transaction transaction = session.beginTransaction();
        try {
            if (orUpdate) {
                session.saveOrUpdate(model);
            } else {
                session.save(model);
            }

            session.flush();
            session.refresh(model);

            transaction.commit();
        } catch (Exception ex) {
            transaction.rollback();
            hasError = true;
        } finally {
            session.close();

            return hasError ? null : model;
        }
    }

    public void commitTransaction(Transaction transaction) {
        transaction.commit();
    }
}
