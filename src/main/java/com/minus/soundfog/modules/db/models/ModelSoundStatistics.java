package com.minus.soundfog.modules.db.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * @author minus
 */

@Entity
@Table(name = "sound_statistics")
public class ModelSoundStatistics extends BaseModel{

    @Id
    @GeneratedValue
    @Column(name = "id", insertable = false, updatable = false)
    private int id;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "soundForStatistics", nullable = true, foreignKey = @ForeignKey(name = "statistics"))
    private ModelSound sound;
    
    @Column(nullable = true)
    private Double bpm;
    @Column(nullable = true)
    private Integer comment_count;
    @Column(nullable = true)
    private Integer download_count;
    @Column(nullable = true)
    private Integer duration;
    @Column(nullable = true)
    private Long FileSize;
    @Column(nullable = true)
    private String format;
    @Column(nullable = true)
    private Integer favoritingsCount;
    @Column(nullable = true)
    private Integer playbackCount;

    public ModelSoundStatistics() {
    }

    public ModelSoundStatistics(
            ModelSound modelSound,
            Double bpm, Integer duration, Long FileSize, String format, 
            Integer playbackCount, Integer commentCount, Integer downloadCount, Integer favoritingsCount) {
        
        this.sound = modelSound;
        
        this.bpm = bpm;
        this.comment_count = commentCount;
        this.download_count = downloadCount;
        this.duration = duration;
        this.FileSize = FileSize;
        this.format = format;
        this.favoritingsCount = favoritingsCount;
        this.playbackCount = playbackCount;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ModelSound getSound() {
        return sound;
    }

    public void setSound(ModelSound sound) {
        this.sound = sound;
    }

    public Double getBpm() {
        return bpm;
    }

    public void setBpm(Double bpm) {
        this.bpm = bpm;
    }

    public Integer getComment_count() {
        return comment_count;
    }

    public void setComment_count(Integer comment_count) {
        this.comment_count = comment_count;
    }

    public Integer getDownload_count() {
        return download_count;
    }

    public void setDownload_count(Integer download_count) {
        this.download_count = download_count;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Long getFileSize() {
        return FileSize;
    }

    public void setFileSize(Long FileSize) {
        this.FileSize = FileSize;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public Integer getFavoritingsCount() {
        return favoritingsCount;
    }

    public void setFavoritingsCount(Integer favoritingsCount) {
        this.favoritingsCount = favoritingsCount;
    }

    public Integer getPlaybackCount() {
        return playbackCount;
    }

    public void setPlaybackCount(Integer playbackCount) {
        this.playbackCount = playbackCount;
    }
}
