package com.minus.soundfog.modules.db.dto;

import com.minus.soundfog.annotations.DtoMeta;
import com.minus.soundfog.exceptions.NoSuitableConstructorFoundException;
import com.minus.soundfog.modules.db.DB;
import com.minus.soundfog.modules.db.models.BaseModel;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.sql.Connection;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jooq.DSLContext;
import org.jooq.impl.DAOImpl;
import org.jooq.impl.TableImpl;

/**
 * @author minus
 */
abstract public class BaseDto {
    final protected DAOImpl dao;
    final private DSLContext dslContext;

    protected BaseDto() {
        try {
            dao = getClass().getAnnotation(DtoMeta.class).daoClass().newInstance();
        } catch (InstantiationException | IllegalAccessException ex) {
            throw new RuntimeException("Dao was not set or was not found.");
        }
        
        validateAnnotations();
        dslContext = DB.getInstance().getDslContext();
    }

    abstract public TableImpl getTableLike();

    final public Class<? extends BaseModel> getModelKlazz() {
        try {
            return Class.forName(getClass().getName()).getAnnotation(DtoMeta.class).baseModelClass();
        } catch (ClassNotFoundException ex) {
            return null;
        }
    }

    abstract public DAOImpl getDao();

    final protected DSLContext getContext() {
        return dslContext;
    }

    /**
     * @WorkInProgress @param args if empty, no params constructor will be used
     * and an empty instantiated model will be returned. In which one could
     * invoke relevant setter methods before save
     * @return
     * @throws NoSuitableConstructorFoundException
     */
    private BaseModel _createNewModel(Object... args) throws NoSuitableConstructorFoundException {
        try {
            Method[] methods = getClassOfModel().getMethods();
            Constructor<?>[] constructors = getClassOfModel().getConstructors();
            Constructor<?> mostSuitableConstructor = null;
            for (Constructor<?> constructor : constructors) {
                if (constructor.getParameterCount() == args.length) {
                    mostSuitableConstructor = constructor;
                    break;
                }
            }

            if (mostSuitableConstructor == null || mostSuitableConstructor.getParameters() == null) {
                throw new NoSuitableConstructorFoundException();
            }

            TreeMap<String, Method> sortedMethodNames = new TreeMap();
            for (Method method : methods) {

                if (method.getName().startsWith("set")) {

                    Parameter[] parameters = mostSuitableConstructor.getParameters();

                    Parameter[] parameters1 = method.getParameters();

                    boolean contains = Arrays.asList(mostSuitableConstructor.getParameters()).contains(method.getParameters()[0]);
//                    Object a = mostSuitableConstructor.getParameters()[binarySearch];

                    sortedMethodNames.put(method.getName(), method);
                }
            }

            Arrays.sort(args);

            BaseModel newModel = getClassOfModel().getConstructor().newInstance();
            for (Map.Entry<String, Method> methodItem : sortedMethodNames.entrySet()) {
                String methodName = methodItem.getKey();
                Method method = methodItem.getValue();
                method.invoke(newModel, args);

            }

            return getClassOfModel().getConstructor().newInstance();
        } catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            Logger.getLogger(BaseDto.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null; // throw new not enough params exception or something
    }

    protected BaseModel singleParamQuery(String field, Object arg) {
        HashMap params = new HashMap();
        params.put(field, arg);

        String queryString = "from " + getModelName() + " where " + field + " = :" + field;
        List doQuery = DB.getInstance().query(queryString, params);

        return doQuery.isEmpty() ? null : getClassOfModel().cast(doQuery.get(0));
    }

    final public BaseModel getByID(int id) {
        return (BaseModel) DB.getInstance().getByID(getClassOfModel(), id);
    }

    public List<BaseModel> getAll() {
        return getAll(false);
    }

    public List<BaseModel> getAll(Boolean desc) {
        return DB.getInstance().query("from " + getClassOfModel().getName() + (desc ? "order by " + getClassOfModel().getName() + ".id desc" : ""));
    }

    final public BaseModel getLast() {
        return (BaseModel) DB.getInstance().getSession()
                .createQuery("from " + getModelName() + " ORDER BY id DESC")
                .setMaxResults(1)
                .uniqueResult();
    }

    final public BaseModel getFirst() {
        return (BaseModel) DB.getInstance().getSession()
                .createQuery("from " + getModelName() + " ORDER BY id ASC")
                .setMaxResults(1)
                .uniqueResult();
    }

    final public Class<? extends BaseModel> getClassOfModel() {
        try {
            return Class.forName(getClass().getName()).getAnnotation(DtoMeta.class).baseModelClass();
        } catch (ClassNotFoundException ex) {
            return null;
        }
    }

    final public String getModelName() {
        return getClassOfModel().getName();
    }

    private Boolean validateAnnotations() {
        if (getClassOfModel() == null) {
            // This is to make sure that some query "magic" functionality will be maintained
            Logger.getLogger(getClass().getName()).log(
                    Level.WARNING,
                    "{0} must use {1}. As all other DTOs....You\'re not a special or unique snowfalke", new Object[]{getClass().getName(), DtoMeta.class.getName()}
            );
        }

        return getClassOfModel() == null;
    }
}
