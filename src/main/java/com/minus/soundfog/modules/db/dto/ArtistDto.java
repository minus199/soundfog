package com.minus.soundfog.modules.db.dto;

import com.minus.soundfog.annotations.DtoMeta;
import com.minus.soundfog.modules.db.mappings.daos.ArtistDao;
import com.minus.soundfog.modules.db.models.ModelArtist;
import org.jooq.impl.DAOImpl;
import org.jooq.impl.TableImpl;

/**
 * @author minus
 */
@DtoMeta(baseModelClass = ModelArtist.class, daoClass = ArtistDao.class)
public class ArtistDto extends BaseDto {

    public ArtistDto() {
        ArtistDao artistDao = new ArtistDao();
    }

    @Override
    public TableImpl getTableLike() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public DAOImpl getDao() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private static class InstanceContainer {

        final private static ArtistDto instance = new ArtistDto();
    }

    public static ArtistDto getInstance() {
        return InstanceContainer.instance;
    }

    public ModelArtist getBySoundCloudID(int soundcloudID) {
        return (ModelArtist) singleParamQuery("soundcloudID", soundcloudID);
    }
}
