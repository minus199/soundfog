package com.minus.soundfog.modules.db.models;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * @author minus
 */
@Entity
@Table(name = "sound_extras")
public class ModelSoundExtras extends BaseModel {

    @Id
    @GeneratedValue
    @Column(name = "id", insertable = false, updatable = false)
    private int id;

    @JoinColumn(name = "soundForExtras", nullable = true, foreignKey = @ForeignKey(name = "extras"))
    @OneToOne(fetch = FetchType.LAZY)
    private ModelSound sound;

    @Column(nullable = true)
    private Boolean commentable;
    @Column(nullable = true)
    private Boolean downloadable;
    @Column(nullable = true)
    private String embeddableBy;
    @Column(nullable = true)
    private Boolean streamable;

    @Column(nullable = true)
    private Double labelID;
    @Column(nullable = true)
    private String labelName;
    @Column(nullable = true)
    private String kind;
    @Column(nullable = true)
    private String sharing;
    @Column(nullable = true)
    private String state;
    @Column(nullable = true)
    private String release;
    @Column(name = "release_date", nullable = true)
    private Date releaseDate;
    @Column(nullable = true)
    private Date lastModified;

    public ModelSoundExtras() {
    }

    public ModelSoundExtras(
            ModelSound modelSound,
            Boolean commentable, Boolean downloadable, String embeddableBy, Boolean streamable,
            Double labelID, String labelName,
            String kind, String sharing, String state,
            String release, Double release_day, Double release_month, Double release_year,
            String lastModified) {

        this.sound = modelSound;

        this.commentable = commentable;
        this.downloadable = downloadable;
        this.embeddableBy = embeddableBy;
        this.streamable = streamable;
        this.labelID = labelID;
        this.labelName = labelName;
        this.kind = kind;
        this.sharing = sharing;
        this.state = state;
        this.release = release;

        //this.releaseDate = new GregorianCalendar(release_year.intValue(), release_month.intValue() - 1, release_day.intValue()).getTime();
        this.releaseDate = new Date();
        try {
            this.lastModified = DateFormat.getInstance().parse(lastModified);
        } catch (ParseException ex) {
            //Logger.getLogger(ModelSoundExtras.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Integer getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ModelSound getSound() {
        return sound;
    }

    public void setSound(ModelSound sound) {
        this.sound = sound;
    }

    public Boolean getCommentable() {
        return commentable;
    }

    public void setCommentable(Boolean commentable) {
        this.commentable = commentable;
    }

    public Boolean getDownloadable() {
        return downloadable;
    }

    public void setDownloadable(Boolean downloadable) {
        this.downloadable = downloadable;
    }

    public String getEmbeddableBy() {
        return embeddableBy;
    }

    public void setEmbeddableBy(String embeddableBy) {
        this.embeddableBy = embeddableBy;
    }

    public Boolean getStreamable() {
        return streamable;
    }

    public void setStreamable(Boolean streamable) {
        this.streamable = streamable;
    }

    public Double getLabelID() {
        return labelID;
    }

    public void setLabelID(Double labelID) {
        this.labelID = labelID;
    }

    public String getLabelName() {
        return labelName;
    }

    public void setLabelName(String labelName) {
        this.labelName = labelName;
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public String getSharing() {
        return sharing;
    }

    public void setSharing(String sharing) {
        this.sharing = sharing;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getRelease() {
        return release;
    }

    public void setRelease(String release) {
        this.release = release;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

    public Date getLastModified() {
        return lastModified;
    }

    public void setLastModified(Date lastModified) {
        this.lastModified = lastModified;
    }
}
