package com.minus.soundfog.modules.db.dto;

import com.minus.soundfog.annotations.DtoMeta;
import com.minus.soundfog.exceptions.DuplicateFetchedPageException;
import com.minus.soundfog.modules.db.DB;
import com.minus.soundfog.modules.db.mappings.Tables;
import com.minus.soundfog.modules.db.mappings.daos.FetchedPagesDao;
import com.minus.soundfog.modules.db.models.ModelFetchedPages;
import org.jooq.Record;
import org.jooq.Record1;
import org.jooq.Result;
import org.jooq.impl.DAOImpl;
import org.jooq.impl.TableImpl;

/**
 * @author minus
 */
@DtoMeta(baseModelClass = ModelFetchedPages.class, daoClass = FetchedPagesDao.class)
public class FetchedPageDto extends BaseDto {

    public ModelFetchedPages createNewModel(String cursor, String linkedPartitioning, int pageNumber, int pageSize, String content) throws DuplicateFetchedPageException {
        ModelFetchedPages modelFetchedPages = new ModelFetchedPages(cursor, linkedPartitioning, pageNumber, pageSize, content);

        if (getByHash(modelFetchedPages.getGeneratedHashCode()) == null) {
            //modelFetchedPages.save();
            return modelFetchedPages;
        }

        throw new DuplicateFetchedPageException();
    }

    @Override
    public TableImpl getTableLike() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public FetchedPagesDao getDao() {
        return (FetchedPagesDao) dao;
    }

    private static class InstanceContainer {

        final private static FetchedPageDto instance = new FetchedPageDto();
    }

    public static FetchedPageDto getInstance() {
        return InstanceContainer.instance;
    }

    public Result<Record> getByHash(int hash) {
        return DB.getInstance().getDslContext().select().from(getTableLike()).where(Tables.FETCHED_PAGES.UNIQUE_HASH_CODE.equal(hash)).fetch();
    }
    
    public Boolean isRawDataPersisted(String cursor, String linkedPartitioning, int pageNumber, int pageSize, String content) {
        ModelFetchedPages modelFetchedPages = new ModelFetchedPages(cursor, linkedPartitioning, pageNumber, pageSize, content);

        return getByHash(modelFetchedPages.getGeneratedHashCode()) != null; 
    }
}
