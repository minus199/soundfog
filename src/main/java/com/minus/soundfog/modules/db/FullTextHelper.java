package com.minus.soundfog.modules.db;

import com.minus.soundfog.modules.db.models.ModelSound;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Session;
import org.hibernate.search.FullTextQuery;
import org.hibernate.search.FullTextSession;
import org.hibernate.search.Search;
import org.hibernate.search.query.DatabaseRetrievalMethod;
import org.hibernate.search.query.ObjectLookupMethod;

/**
 * @author minus
 */
class FullTextHelper {

    public List<ModelSound> lookUp(Session session, String searchTerm) throws SQLException {
        FullTextSession fullTextSession = Search.getFullTextSession(session);
        try {
            fullTextSession.createIndexer().startAndWait();
        } catch (InterruptedException ex) {
            Logger.getLogger(FullTextHelper.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        
        org.hibernate.search.query.dsl.QueryBuilder qb = fullTextSession.getSearchFactory().buildQueryBuilder().forEntity(ModelSound.class).get();
        org.apache.lucene.search.Query luceneQuery = qb
                .keyword()
                .onFields("title", "genere", "tags")
                .matching(searchTerm)
                .createQuery();
        
        FullTextQuery query = fullTextSession.createFullTextQuery(luceneQuery, ModelSound.class);
        query.initializeObjectsWith(ObjectLookupMethod.PERSISTENCE_CONTEXT, DatabaseRetrievalMethod.QUERY);
        
        return query.list();
    }
}
