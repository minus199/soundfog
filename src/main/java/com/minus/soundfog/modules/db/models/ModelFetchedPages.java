package com.minus.soundfog.modules.db.models;

import java.util.Date;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

/**
 * @author minus
 */
@Entity
@Table(name = "fetched_pages")
public class ModelFetchedPages extends BaseModel {

    @Id
    @GeneratedValue
    private Integer id;

    private String cursor;

    @Column(name = "linked_partitioning", nullable = false)
    private String linkedPartitioning;

    @Column(name = "page_number", nullable = false)
    private int pageNumber;

    @Column(name = "page_size", nullable = false)
    private int pageSize;

    @Column(length = 10485760, nullable = false)
    private String content;

    @Column(name = "created", nullable = false)
    private Date created;

    @Column(name = "unique_hash_code")
    private Integer generatedHashCode;

    @Version
    private Date lastModified;

    public ModelFetchedPages() {

    }

    public ModelFetchedPages(String cursor, String linkedPartitioning, int pageNumber, int pageSize, String content) {
        this.cursor = cursor;
        this.linkedPartitioning = linkedPartitioning;
        this.pageNumber = pageNumber;
        this.pageSize = pageSize;
        this.content = content;
        generatedHashCode = hashCode();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCursor() {
        return cursor;
    }

    public void setCursor(String cursor) {
        this.cursor = cursor;
    }

    public String getLinkedPartitioning() {
        return linkedPartitioning;
    }

    public void setLinkedPartitioning(String linkedPartitioning) {
        this.linkedPartitioning = linkedPartitioning;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Integer getGeneratedHashCode() {
        return generatedHashCode;
    }

    public void setGeneratedHashCode(Integer generatedHashCode) {
        this.generatedHashCode = generatedHashCode;
    }

    public Date getLastModified() {
        return lastModified;
    }

    public void setLastModified(Date lastModified) {
        this.lastModified = lastModified;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + Objects.hashCode(this.cursor);
        hash = 97 * hash + Objects.hashCode(this.content.subSequence(0, 1000));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ModelFetchedPages other = (ModelFetchedPages) obj;
        if (!Objects.equals(this.cursor, other.cursor)) {
            return false;
        }
        if (!Objects.equals(this.content, other.content)) {
            return false;
        }
        return true;
    }
}
