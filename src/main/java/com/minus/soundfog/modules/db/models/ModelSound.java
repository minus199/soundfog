/**
 * @author minus
 */
package com.minus.soundfog.modules.db.models;

import com.minus.soundfog.descriptors.SoundDescriptor;
import com.minus.soundfog.modules.playlist.LoaderType;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.search.annotations.Analyze;
import org.hibernate.search.annotations.Analyzer;
import org.hibernate.search.annotations.AnalyzerDef;
import org.hibernate.search.annotations.DocumentId;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Index;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.Parameter;
import org.hibernate.search.annotations.Store;
import org.hibernate.search.annotations.TokenFilterDef;
import org.hibernate.search.annotations.TokenizerDef;

@Entity
@Table(name = "sounds"/*,
 uniqueConstraints
 = @UniqueConstraint(columnNames = {"soundcloud_id"})*/)
@Indexed
@AnalyzerDef(name = "customanalyzer",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
            @TokenFilterDef(factory = LowerCaseFilterFactory.class),
            @TokenFilterDef(factory = SnowballPorterFilterFactory.class, params = {
                @Parameter(name = "language", value = "English")
            })
        })
public class ModelSound extends BaseModel {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @DocumentId
    @Column(name = "id", insertable = false, updatable = false)
    private int id;

    @Column(name = "soundcloud_id")
    private int soundcloudID;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "soundArtist", nullable = true, foreignKey = @ForeignKey(name = "sounds"))
    private ModelArtist artist;

    @Field(index = Index.YES, analyze = Analyze.YES, store = Store.YES)
    @Analyzer(definition = "customanalyzer")
    @Column(name = "title")
    private String title;

    @Field(index = Index.YES, analyze = Analyze.YES, store = Store.YES)
    @Analyzer(definition = "customanalyzer")
    @Column(name = "tags", length = 1500)
    private String tags;

    @Field(index = Index.YES, analyze = Analyze.YES, store = Store.YES)
    @Analyzer(definition = "customanalyzer")
    @Column(name = "genere")
    private String genere;

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "sound", cascade = CascadeType.ALL)
    private ModelSoundArtwork soundArtwork;

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "sound", cascade = CascadeType.ALL)
    private ModelSoundWaveform soundWaveform;

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "sound", cascade = CascadeType.ALL)
    private ModelSoundExtras extras;

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "sound", cascade = CascadeType.ALL)
    private ModelSoundStatistics statistics;

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "sound", cascade = CascadeType.ALL)
    private ModelSoundURLs urls;

    @Column(name = "type")
    private LoaderType type;

    @Column(name = "chronoligcall_order")
    private Integer chronoligcallOrder;

    public ModelSound() {
    }

    public ModelSound(SoundDescriptor soundDescriptor) {
        this(soundDescriptor, null, null);
    }

    public ModelSound(SoundDescriptor soundDescriptor, ModelSoundArtwork artwork, ModelSoundWaveform waveform) {
        this.title = soundDescriptor.getTitle();
        this.tags = soundDescriptor.getTagList();
        this.genere = soundDescriptor.getGenre();
        this.soundcloudID = soundDescriptor.getId();

        this.soundArtwork = artwork;
        this.soundWaveform = waveform;

        this.type = soundDescriptor.getSoundType();
        this.chronoligcallOrder = soundDescriptor.getChronologicallOrder();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ModelArtist getArtist() {
        return artist;
    }

    public void setArtist(ModelArtist artist) {
        this.artist = artist;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getGenere() {
        return genere;
    }

    public void setGenere(String genere) {
        this.genere = genere;
    }

    public ModelSoundArtwork getSoundArtwork() {
        return soundArtwork;
    }

    public void setSoundArtwork(ModelImage modelImage) {
        this.soundArtwork = new ModelSoundArtwork(this, modelImage);
    }

    public void setSoundArtwork(ModelSoundArtwork soundArtwork) {
        this.soundArtwork = soundArtwork;
    }

    public ModelSoundWaveform getSoundWaveform() {
        return soundWaveform;
    }

    public void setSoundWaveform(ModelImage modelImage) {
        this.soundWaveform = new ModelSoundWaveform(this, modelImage);
    }

    public void setSoundWaveform(ModelSoundWaveform soundWaveform) {
        this.soundWaveform = soundWaveform;
    }

    public int getSoundcloudID() {
        return soundcloudID;
    }

    public void setSoundcloudID(int soundcloudID) {
        this.soundcloudID = soundcloudID;
    }

    public ModelSoundExtras getExtras() {
        return extras;
    }

    public void setExtras(ModelSoundExtras extras) {
        this.extras = extras;
    }

    public ModelSoundStatistics getStatistics() {
        return statistics;
    }

    public void setStatistics(ModelSoundStatistics statistics) {
        this.statistics = statistics;
    }

    public ModelSoundURLs getUrls() {
        return urls;
    }

    public void setUrls(ModelSoundURLs urls) {
        this.urls = urls;
    }

    public LoaderType getType() {
        return type;
    }

    public void setType(LoaderType type) {
        this.type = type;
    }

    public Integer getChronoligcallOrder() {
        return chronoligcallOrder;
    }

    public void setChronoligcallOrder(Integer chronoligcallOrder) {
        this.chronoligcallOrder = chronoligcallOrder;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 19 * hash + this.soundcloudID;
        hash = 19 * hash + Objects.hashCode(this.title);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ModelSound other = (ModelSound) obj;
        if (this.soundcloudID != other.soundcloudID) {
            return false;
        }
        if (!Objects.equals(this.title, other.title)) {
            return false;
        }
        return true;
    }

}
