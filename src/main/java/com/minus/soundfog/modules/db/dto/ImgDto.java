package com.minus.soundfog.modules.db.dto;

import com.minus.soundfog.annotations.DtoMeta;
import com.minus.soundfog.modules.db.mappings.daos.ImageDao;
import com.minus.soundfog.modules.db.models.ModelImage;
import org.jooq.impl.DAOImpl;
import org.jooq.impl.TableImpl;

/**
 * @author minus
 */
@DtoMeta(baseModelClass = ModelImage.class, daoClass = ImageDao.class)
public class ImgDto extends BaseDto {

    @Override
    public TableImpl getTableLike() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public DAOImpl getDao() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private static class InstanceContainer {

        final private static ImgDto instance = new ImgDto();
    }

    public static ImgDto getInstance() {
        return InstanceContainer.instance;
    }
}
