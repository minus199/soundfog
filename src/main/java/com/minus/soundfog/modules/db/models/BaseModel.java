package com.minus.soundfog.modules.db.models;

import com.minus.soundfog.modules.db.DB;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.Date;

/**
 * @author minus
 */
public class BaseModel implements Serializable{
    private void stampOnPersist(){
        try {
            Field createdField = getClass().getDeclaredField("created");
            createdField.setAccessible(true);
            
            if (createdField.get(this) != null){
                /* Do not override child settings */
                return;
            }
            
            createdField.set(this, new Date());
            createdField.setAccessible(false);
        } catch (NoSuchFieldException | IllegalArgumentException | IllegalAccessException ex) {
        }
    }
    
    public void save() {
        stampOnPersist();

        DB.getInstance().save(this);
    }
}
