package com.minus.soundfog.modules.db.grouppersisters;

import com.minus.soundfog.exceptions.UnableToCommitGroupException;
import com.minus.soundfog.exceptions.UnableToPersistGroupException;
import com.minus.soundfog.modules.db.models.BaseModel;
import java.util.HashMap;
/**
 *
 * @author minus
 */
public interface IPersister {
    public IPersister persistGroup() throws UnableToPersistGroupException;
    
    public IPersister commit() throws UnableToCommitGroupException;
    
    public HashMap<Class<? extends BaseModel>, BaseModel> getModels();
}
