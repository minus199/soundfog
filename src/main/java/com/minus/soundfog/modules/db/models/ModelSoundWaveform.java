package com.minus.soundfog.modules.db.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * @author minus
 */

@Entity
@Table(name = "sound_waveform")
public class ModelSoundWaveform extends BaseModel {
     @Id
    @GeneratedValue
    @Column(name = "id", insertable = false, updatable = false)
    private int id;
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "sound", nullable = true, foreignKey = @ForeignKey(name = "soundArtwork"))
    private ModelSound sound;
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "artworkImage", nullable = true, foreignKey = @ForeignKey(name = "id"))
    private ModelImage modelImage;

    public ModelSoundWaveform() {
    }

    public ModelSoundWaveform(ModelSound sound, ModelImage modelImage) {
        this.sound = sound;
        this.modelImage = modelImage;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ModelSound getSound() {
        return sound;
    }

    public void setSound(ModelSound sound) {
        this.sound = sound;
    }

    public ModelImage getModelImage() {
        return modelImage;
    }

    public void setModelImage(ModelImage modelImage) {
        this.modelImage = modelImage;
    }
    
    
}
