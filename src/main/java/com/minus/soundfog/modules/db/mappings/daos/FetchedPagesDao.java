/**
 * This class is generated by jOOQ
 */
package com.minus.soundfog.modules.db.mappings.daos;


import com.minus.soundfog.modules.db.mappings.tables.FetchedPages;
import com.minus.soundfog.modules.db.mappings.records.FetchedPagesRecord;

import java.sql.Timestamp;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Configuration;
import org.jooq.impl.DAOImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
	value = {
		"http://www.jooq.org",
		"jOOQ version:3.7.2"
	},
	comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class FetchedPagesDao extends DAOImpl<FetchedPagesRecord, com.minus.soundfog.modules.db.mappings.pojos.FetchedPages, Integer> {

	/**
	 * Create a new FetchedPagesDao without any configuration
	 */
	public FetchedPagesDao() {
		super(FetchedPages.FETCHED_PAGES, com.minus.soundfog.modules.db.mappings.pojos.FetchedPages.class);
	}

	/**
	 * Create a new FetchedPagesDao with an attached configuration
	 */
	public FetchedPagesDao(Configuration configuration) {
		super(FetchedPages.FETCHED_PAGES, com.minus.soundfog.modules.db.mappings.pojos.FetchedPages.class, configuration);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected Integer getId(com.minus.soundfog.modules.db.mappings.pojos.FetchedPages object) {
		return object.getId();
	}

	/**
	 * Fetch records that have <code>ID IN (values)</code>
	 */
	public List<com.minus.soundfog.modules.db.mappings.pojos.FetchedPages> fetchById(Integer... values) {
		return fetch(FetchedPages.FETCHED_PAGES.ID, values);
	}

	/**
	 * Fetch a unique record that has <code>ID = value</code>
	 */
	public com.minus.soundfog.modules.db.mappings.pojos.FetchedPages fetchOneById(Integer value) {
		return fetchOne(FetchedPages.FETCHED_PAGES.ID, value);
	}

	/**
	 * Fetch records that have <code>CONTENT IN (values)</code>
	 */
	public List<com.minus.soundfog.modules.db.mappings.pojos.FetchedPages> fetchByContent(String... values) {
		return fetch(FetchedPages.FETCHED_PAGES.CONTENT, values);
	}

	/**
	 * Fetch records that have <code>CREATED IN (values)</code>
	 */
	public List<com.minus.soundfog.modules.db.mappings.pojos.FetchedPages> fetchByCreated(Timestamp... values) {
		return fetch(FetchedPages.FETCHED_PAGES.CREATED, values);
	}

	/**
	 * Fetch records that have <code>CURSOR IN (values)</code>
	 */
	public List<com.minus.soundfog.modules.db.mappings.pojos.FetchedPages> fetchByCursor(String... values) {
		return fetch(FetchedPages.FETCHED_PAGES.CURSOR, values);
	}

	/**
	 * Fetch records that have <code>UNIQUE_HASH_CODE IN (values)</code>
	 */
	public List<com.minus.soundfog.modules.db.mappings.pojos.FetchedPages> fetchByUniqueHashCode(Integer... values) {
		return fetch(FetchedPages.FETCHED_PAGES.UNIQUE_HASH_CODE, values);
	}

	/**
	 * Fetch records that have <code>LASTMODIFIED IN (values)</code>
	 */
	public List<com.minus.soundfog.modules.db.mappings.pojos.FetchedPages> fetchByLastmodified(Timestamp... values) {
		return fetch(FetchedPages.FETCHED_PAGES.LASTMODIFIED, values);
	}

	/**
	 * Fetch records that have <code>LINKED_PARTITIONING IN (values)</code>
	 */
	public List<com.minus.soundfog.modules.db.mappings.pojos.FetchedPages> fetchByLinkedPartitioning(String... values) {
		return fetch(FetchedPages.FETCHED_PAGES.LINKED_PARTITIONING, values);
	}

	/**
	 * Fetch records that have <code>PAGE_NUMBER IN (values)</code>
	 */
	public List<com.minus.soundfog.modules.db.mappings.pojos.FetchedPages> fetchByPageNumber(Integer... values) {
		return fetch(FetchedPages.FETCHED_PAGES.PAGE_NUMBER, values);
	}

	/**
	 * Fetch records that have <code>PAGE_SIZE IN (values)</code>
	 */
	public List<com.minus.soundfog.modules.db.mappings.pojos.FetchedPages> fetchByPageSize(Integer... values) {
		return fetch(FetchedPages.FETCHED_PAGES.PAGE_SIZE, values);
	}
}
