package com.minus.soundfog.modules.db.models;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.search.annotations.DocumentId;

/**
 * @author minus
 */
@Entity
@Table(name = "runtime_meta")
public class ModelRuntimeMeta extends BaseModel {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @DocumentId
    @Column(name = "id", insertable = false, updatable = false)
    private int id;

    private String key;

    private String value;

    private Date created;

    public ModelRuntimeMeta() {
    }

    public ModelRuntimeMeta(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
