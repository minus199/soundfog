package com.minus.soundfog.modules.db.dto;

import com.minus.soundfog.annotations.DtoMeta;
import com.minus.soundfog.modules.db.DB;
import com.minus.soundfog.modules.db.mappings.Tables;
import com.minus.soundfog.modules.db.mappings.daos.SoundsDao;
import com.minus.soundfog.modules.db.models.BaseModel;
import com.minus.soundfog.modules.db.models.ModelSound;
import java.util.List;
import org.jooq.impl.TableImpl;

/**
 * @author minus
 */
@DtoMeta(baseModelClass = ModelSound.class, daoClass = SoundsDao.class)
public class SoundDto extends BaseDto {

    @Override
    public TableImpl getTableLike() {
        return Tables.ARTIST;
    }

    @Override
    public SoundsDao getDao() {
        return new SoundsDao();
    }

    private static class InstanceContainer {

        final private static SoundDto instance = new SoundDto();
    }

    public static SoundDto getInstance() {
        return InstanceContainer.instance;
    }

    public ModelSound getBySoundCloudID(int soundcloudID) {
        return (ModelSound) singleParamQuery("soundcloudID", soundcloudID);
    }

    @Override
    public List<BaseModel> getAll() {
        String a = "from " + getClassOfModel().getName() + " order by " + getClassOfModel().getName() + ".chronoligcallOrder";
        return DB.getInstance().query("from " + getClassOfModel().getName() + " order by " + getClassOfModel().getName() + ".chronoligcallOrder");
    }
    
    
}
