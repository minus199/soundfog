package com.minus.soundfog.modules.db.grouppersisters;

import com.minus.soundfog.descriptors.SoundDescriptor;
import com.minus.soundfog.descriptors.User;
import com.minus.soundfog.exceptions.ExceptionUtils;
import com.minus.soundfog.exceptions.ImmutablePropertyException;
import com.minus.soundfog.exceptions.UnableToCommitGroupException;
import com.minus.soundfog.exceptions.UnableToPersistGroupException;
import com.minus.soundfog.modules.db.DB;
import com.minus.soundfog.modules.db.dto.ArtistDto;
import com.minus.soundfog.modules.db.dto.SoundDto;
import com.minus.soundfog.modules.db.models.BaseModel;
import com.minus.soundfog.modules.db.models.ModelArtist;
import com.minus.soundfog.modules.db.models.ModelSound;
import com.minus.soundfog.modules.db.models.ModelSoundExtras;
import com.minus.soundfog.modules.db.models.ModelSoundStatistics;
import com.minus.soundfog.modules.db.models.ModelSoundURLs;
import com.minus.soundfog.modules.dispatching.events.SoundCreatedEvent;
import com.minus.soundfog.modules.eventsdispatcher.Dispatcher;
import com.minus.soundfog.modules.picturegallery.ImageCreator;
import com.minus.soundfog.modules.picturegallery.Picture;
import java.util.HashMap;
import org.apache.logging.log4j.Logger;

/**
 * @author minus
 */
public class SoundModelsGroupPersister implements IPersister {
    private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger(SoundModelsGroupPersister.class.getName());
    

    private final SoundDescriptor soundDescriptor;
    private ModelSound modelSound;
    private ModelArtist modelArtist;

    public SoundModelsGroupPersister(SoundDescriptor soundDescriptor) {
        this.soundDescriptor = soundDescriptor;
    }

    @Override
    public SoundModelsGroupPersister persistGroup() throws UnableToPersistGroupException {
        persistPrimaryModels();
        return this;
    }

    @Override
    public SoundModelsGroupPersister commit() throws UnableToCommitGroupException {
        DB.getInstance().save(modelArtist);
        DB.getInstance().save(modelSound);
        Dispatcher.manage().dispatch(new SoundCreatedEvent(soundDescriptor, modelSound));

        return this;
    }

    private ModelSound persistPrimaryModels() {
        User user = soundDescriptor.getUser();
        modelArtist = ArtistDto.getInstance().getBySoundCloudID(user.getId());
        if (modelArtist == null) {
            modelArtist = new ModelArtist(user.getId(), user.getAvatarUrl(), user.getUsername(), user.getPermalink(), user.getPermalinkUrl(), user.getUri());
        }

        modelSound = SoundDto.getInstance().getBySoundCloudID(soundDescriptor.getId());
        if (modelSound == null) {
            modelSound = new ModelSound(soundDescriptor);
        }

        modelSound.setArtist(modelArtist);
        modelSound = persistAdditionalModels(modelSound);
        try {
            soundDescriptor.setModelSound(modelSound);
        } catch (ImmutablePropertyException ex) {
            logger.error("Tried to change likeDescriptor's modelSound", ex.getMessage(), ex);
        }

        return modelSound;
    }

    private ModelSound persistAdditionalModels(ModelSound modelSound) {
        ModelSoundExtras modelSoundExtras = new ModelSoundExtras(
                modelSound,
                soundDescriptor.getCommentable(),
                soundDescriptor.getDownloadable(),
                soundDescriptor.getEmbeddableBy(),
                soundDescriptor.getStreamable(),
                (Double) soundDescriptor.getLabelId(),
                soundDescriptor.getLabelName(),
                soundDescriptor.getKind(),
                soundDescriptor.getSharing(),
                soundDescriptor.getState(),
                (String) soundDescriptor.getRelease(),
                soundDescriptor.getReleaseDay(),
                soundDescriptor.getReleaseMonth(),
                soundDescriptor.getReleaseYear(),
                soundDescriptor.getLastModified()
        );
        modelSound.setExtras(modelSoundExtras);

        ModelSoundStatistics modelSoundStatistics = new ModelSoundStatistics(
                modelSound,
                (Double) soundDescriptor.getBpm(),
                soundDescriptor.getDuration(),
                soundDescriptor.getOriginalContentSize() == null ? 0L : Integer.toUnsignedLong(soundDescriptor.getOriginalContentSize()),
                soundDescriptor.getOriginalFormat(),
                soundDescriptor.getPlaybackCount(),
                soundDescriptor.getCommentCount(),
                soundDescriptor.getDownloadCount(),
                soundDescriptor.getFavoritingsCount()
        );
        modelSound.setStatistics(modelSoundStatistics);

        ModelSoundURLs modelSoundURLs = new ModelSoundURLs(
                modelSound,
                soundDescriptor.getAttachmentsUri(),
                soundDescriptor.getPermalink(),
                soundDescriptor.getPermalinkUrl(),
                soundDescriptor.getPurchaseUrl(),
                soundDescriptor.getStreamUrl(),
                soundDescriptor.getVideoUrl(),
                soundDescriptor.getWaveformUrl()
        );
        modelSound.setUrls(modelSoundURLs);

        persistImages();
        return modelSound;
    }

    private void persistImages() {
        Picture makeWaveform;
        try {
            if ((makeWaveform = ImageCreator.makeWaveform(soundDescriptor.getWaveformUrl())) != null) {
                modelSound.setSoundWaveform(makeWaveform.getModelImage());
            }
        } catch (Exception ex) {
            logger.error("Error during persisting ***waveform*** for " + soundDescriptor.getTitle() + "[" + soundDescriptor.getId() + "]", ExceptionUtils.getExceptionMessageChainAsString(ex));
        }

        Picture makeArtwork;
        try {
            if ((makeArtwork = ImageCreator.makeArtwork(soundDescriptor.getArtworkUrl())) != null) {
                modelSound.setSoundArtwork(makeArtwork.getModelImage());
            }
        } catch (Exception ex) {
            logger.error("Error during persisting ---artwork--- for " + soundDescriptor.getTitle() + "[" + soundDescriptor.getId() + "]", ExceptionUtils.getExceptionMessageChainAsString(ex));
        }
    }

    @Override
    public HashMap<Class<? extends BaseModel>, BaseModel> getModels() {
        HashMap<Class<? extends BaseModel>, BaseModel> models = new HashMap<>();
        models.put(modelSound.getClass(), modelSound);

        return models;
    }
}
