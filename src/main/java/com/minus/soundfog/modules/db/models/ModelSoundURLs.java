package com.minus.soundfog.modules.db.models;

import java.net.URL;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * @author minus
 */
@Entity
@Table(name = "sound_urls")
public class ModelSoundURLs extends BaseModel {

    @Id
    @GeneratedValue
    @Column(name = "id", insertable = false, updatable = false)
    private int id;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "soundForURLs", nullable = true, foreignKey = @ForeignKey(name = "urls"))
    private ModelSound sound;

    @Column(nullable = true)
    private URL attachmentsURI;
    @Column(nullable = true)
    private URL permalink;
    @Column(nullable = true)
    private URL permalinkURL;
    @Column(nullable = true)
    private URL purchaseURL;
    
    @Column(nullable = true, length = 1000)
    private URL streamURL;
    @Column(nullable = true)
    private URL videoURL;
    @Column(nullable = true)
    private URL waveformURL;

    public ModelSoundURLs() {
    }

    public ModelSoundURLs(ModelSound sound, URL attachmentsURI, URL permalink, URL permalinkURL, URL purchaseURL, URL streamURL, URL videoURL, URL waveformURL) {
        this.sound = sound;
        this.attachmentsURI = attachmentsURI;
        this.permalink = permalink;
        this.permalinkURL = permalinkURL;
        this.purchaseURL = purchaseURL;
        this.streamURL = streamURL;
        this.videoURL = videoURL;
        this.waveformURL = waveformURL;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ModelSound getSound() {
        return sound;
    }

    public void setSound(ModelSound sound) {
        this.sound = sound;
    }

    public URL getAttachmentsURI() {
        return attachmentsURI;
    }

    public void setAttachmentsURI(URL attachmentsURI) {
        this.attachmentsURI = attachmentsURI;
    }

    public URL getPermalink() {
        return permalink;
    }

    public void setPermalink(URL permalink) {
        this.permalink = permalink;
    }

    public URL getPermalinkURL() {
        return permalinkURL;
    }

    public void setPermalinkURL(URL permalinkURL) {
        this.permalinkURL = permalinkURL;
    }

    public URL getPurchaseURL() {
        return purchaseURL;
    }

    public void setPurchaseURL(URL purchaseURL) {
        this.purchaseURL = purchaseURL;
    }

    public URL getStreamURL() {
        return streamURL;
    }

    public void setStreamURL(URL streamURL) {
        this.streamURL = streamURL;
    }

    public URL getVideoURL() {
        return videoURL;
    }

    public void setVideoURL(URL videoURL) {
        this.videoURL = videoURL;
    }

    public URL getWaveformURL() {
        return waveformURL;
    }

    public void setWaveformURL(URL waveformURL) {
        this.waveformURL = waveformURL;
    }
}
