package com.minus.soundfog.modules.db.models;

import com.minus.soundfog.modules.picturegallery.ImgDescendantClass;
import java.sql.Blob;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

/**
 * @author minus
 */
@Entity
@Table(name = "image")
public class ModelImage extends BaseModel{
    @Id
    @GeneratedValue
    @Column(name = "id")
    private int id;

    @Column(name = "file_name")
    private String fileName;
 
    @Column(name = "img_bytes",length = 100000000)
    private byte[] imageBytes;

    @Column(name = "descendant_class")
    @Enumerated(EnumType.STRING)
    private ImgDescendantClass descendantClass;
    
    public ModelImage() {
    }

    public ModelImage(String fileName, byte[] imageBytes, ImgDescendantClass descendantClass) {
        this.fileName = fileName;
        this.imageBytes = imageBytes;
        this.descendantClass = descendantClass;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public byte[] getImageBytes() {
        return imageBytes;
    }

    public void setImageBytes(byte[] imageBytes) {
        this.imageBytes = imageBytes;
    }

    public ImgDescendantClass getDescendantClass() {
        return descendantClass;
    }

    public void setDescendantClass(ImgDescendantClass descendantClass) {
        this.descendantClass = descendantClass;
    }
}
