package com.minus.soundfog.modules.db.models;

import com.minus.soundfog.modules.db.dto.ImgDto;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

/**
 * @author minus
 */
@Entity

@Table(name = "artist"/*,
        uniqueConstraints
= @UniqueConstraint(columnNames = {"profile_url", "soundcloud_id", "nicename"})*/)
public class ModelArtist extends BaseModel {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private int id;

    @Column(name = "soundcloud_id")
    private Integer soundcloudID;

    @Column(name = "nicename")
    private String nicename;

    @Column(name = "nicename_normalized")
    private String nicenameNormalized;

    @Column(name = "profile_url")
    private String profileURL;

    @Column(name = "avatar_id", nullable = true)
    private int avatarImgId;

    @Transient
    private ModelImage avatar;

    @Column(name = "uri")
    private String uri;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "artist")
    private Set<ModelSound> sounds = new HashSet<>();

    public ModelArtist() {
    }

    public ModelArtist(Integer soundcloudID, String avatarUrl, String nicename, String nicenameNormalized, String profileURL, String uri) {
        this.soundcloudID = soundcloudID;
        this.nicename = nicename;
        this.nicenameNormalized = nicenameNormalized;
        this.profileURL = profileURL;
        this.uri = uri;
        /*
         try {
         Picture makeAvatar = ImageCreator.makeAvatar(new URL(avatarUrl));
         } catch (IOException ex) {
         Logger.getLogger(SoundCreatedListener.class.getName()).log(Level.SEVERE, null, ex);
         }*/
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getSoundcloudID() {
        return soundcloudID;
    }

    public void setSoundcloudID(Integer soundcloudID) {
        this.soundcloudID = soundcloudID;
    }

    public String getNicename() {
        return nicename;
    }

    public void setNicename(String nicename) {
        this.nicename = nicename;
    }

    public String getNicenameNormalized() {
        return nicenameNormalized;
    }

    public void setNicenameNormalized(String nicenameNormalized) {
        this.nicenameNormalized = nicenameNormalized;
    }

    public URL getProfileURL() throws MalformedURLException {
        return new URL(profileURL);
    }

    public void setProfileURL(String profileURL) {
        this.profileURL = profileURL;
    }

    public void setProfileURL(URL profileURL) {
        this.profileURL = profileURL.toString();
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public int getAvatarImgId() {
        return avatarImgId;
    }

    public void setAvatarImgId(int avatarImgId) {
        this.avatarImgId = avatarImgId;
    }

    public ModelImage getAvatar() {
        if (avatar == null) {
            avatar = (ModelImage) ImgDto.getInstance().getByID(avatarImgId);
        }

        return avatar;
    }

    public void setAvatar(ModelImage avatar) {
        this.avatar = avatar;
    }

    public Set<ModelSound> getSounds() {
        return sounds;
    }

    public void setSounds(Set<ModelSound> sounds) {
        this.sounds = sounds;
    }
}
