package com.minus.soundfog.modules.player;

/**
 * @author minus
 */
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class RedirectHandler {

    final private URL url;
    private URL redirectURL;

    private HttpURLConnection conn;

    public RedirectHandler(URL url) throws IOException {
        this.url = url;
        prepareConnection();
    }

    private RedirectHandler prepareConnection() throws IOException {
        conn = (HttpURLConnection) url.openConnection();
        conn.setInstanceFollowRedirects(false);
        conn.setReadTimeout(5000);
        conn.addRequestProperty("Accept-Language", "en-US,en;q=0.8");
        conn.addRequestProperty("User-Agent", "Mozilla");
        conn.addRequestProperty("Referer", "google.com");

        conn.connect();

        if (!isResponseCodeRedirect()) {
            redirectURL = url;
            return this;
        }

        // get redirect url from "location" header field
        redirectURL = new URL(conn.getHeaderField("Location"));

        /*// get the cookie if need, for login
         //String cookies = conn.getHeaderField("Set-Cookie");
         // open the new connnection again
         conn = (HttpURLConnection) new URL(newUrl).openConnection();
         //conn.setRequestProperty("Cookie", cookies);
         conn.addRequestProperty("Accept-Language", "en-US,en;q=0.8");
         conn.addRequestProperty("User-Agent", "Mozilla");
         conn.addRequestProperty("Referer", "google.com");

         System.out.println("Redirect to URL : " + newUrl);*/
        return this;
    }

    private Boolean isResponseCodeRedirect() throws IOException {
        int status = conn.getResponseCode();

        return status != HttpURLConnection.HTTP_OK && (status == HttpURLConnection.HTTP_MOVED_TEMP
                || status == HttpURLConnection.HTTP_MOVED_PERM
                || status == HttpURLConnection.HTTP_SEE_OTHER);
    }

    private RedirectHandler doRequest() throws IOException {
        BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String inputLine;
        StringBuffer html = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            html.append(inputLine);
        }

        in.close();

        return this;
    }

    public URL getRedirectURL() {
        return redirectURL;
    }

}
