package com.minus.soundfog.modules.player;

import com.minus.soundfog.MainApp;
import com.minus.soundfog.exceptions.UnableToPlaySoundException;
import com.minus.soundfog.modules.dispatching.events.SoundProgressEvent;
import com.minus.soundfog.modules.eventsdispatcher.Dispatcher;
import com.minus.soundfog.modules.playlist.ActivePlaylist;
import com.minus.soundfog.modules.sounds.SoundService;
import java.io.IOException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.util.Duration;

/**
 * @author minus
 */
public class Player {

    private static MediaPlayer mediaPlayer;
    private SoundService currentlyPlaying;
    private SoundService previouslyPlaying;

    private Player() {
    }

    public boolean isPlaying() {
        return mediaPlayer != null && mediaPlayer.getStatus() == MediaPlayer.Status.PLAYING;
    }

    public MediaPlayer.Status getStatus() {
        return mediaPlayer.getStatus();
    }

    public Player loadPlaylist() {
        return this;
    }

    private static final class InstanceContainer {

        private static final Player instance = new Player();
    }

    public static Player getInstance() {
        return InstanceContainer.instance;
    }

    private MediaPlayer createPlayer(SoundService soundService) throws UnableToPlaySoundException {
        if (mediaPlayer != null) {
            mediaPlayer.stop();
            mediaPlayer.dispose();
        }

        String url;
        try {
            url = new RedirectHandler(new URL(soundService.getStreamURL().toString())).getRedirectURL().toString().replace("https://", "http://");
        } catch (IOException ex) {
            throw new UnableToPlaySoundException(ex);
        }
        mediaPlayer = new MediaPlayer(new Media(url));
        
        mediaPlayer.setOnPlaying(() -> {
            bindVolumeProperty();
        });

        mediaPlayer.currentTimeProperty().addListener((ObservableValue<? extends Duration> observable, Duration oldValue, Duration newValue) -> {
            Dispatcher.manage().dispatch(new SoundProgressEvent(newValue, soundService == null ? currentlyPlaying : soundService));
        });

        mediaPlayer.setAutoPlay(false);

        return mediaPlayer;
    }

    public void play() throws UnableToPlaySoundException {
        if (currentlyPlaying == null) {
            play(ActivePlaylist.getInstance().firstKey());
        } else {
            play(null);
        }
    }

    public void play(SoundService soundService) throws UnableToPlaySoundException {
        if (soundService == null) {
            mediaPlayer.play();
        } else {
            createPlayer(soundService).play();
            this.previouslyPlaying = this.currentlyPlaying;

            this.currentlyPlaying = soundService;
            ActivePlaylist.getInstance().update(currentlyPlaying);
        }
    }

    private void monitorPlay() {
        new Thread(() -> {
            while (mediaPlayer.getStatus() == MediaPlayer.Status.UNKNOWN) {
                System.out.println("waiting for pplayback " + mediaPlayer.getBufferProgressTime());
            }
            System.out.println(mediaPlayer.getStatus());
            while (mediaPlayer.getStatus() == MediaPlayer.Status.PLAYING) {

                //System.out.println("playing" + mediaPlayer.getTotalDuration());
            }

            try {
                Thread.currentThread().join();
            } catch (InterruptedException ex) {
                Logger.getLogger(Player.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
    }

    public void pause() {
        mediaPlayer.pause();
    }

    public void pausePlay() throws UnableToPlaySoundException {
        if (isPlaying()) {
            pause();
        } else {
            play();
        }
    }

    public void stop() {
        mediaPlayer.stop();
    }

    private void bindVolumeProperty() {
        mediaPlayer.volumeProperty().bind(MainApp.getInstance().getMainViewController().getVolumeValueProperty());
    }

    public void next() throws UnableToPlaySoundException {
        play(ActivePlaylist.getInstance().getNext().getKey());
    }

    public void previous() throws UnableToPlaySoundException {
        play(ActivePlaylist.getInstance().getPrevious().getKey());
    }

    public void seekTo(double second){
        mediaPlayer.seek(mediaPlayer.getCurrentTime().add(Duration.seconds(second)));
    }
    
    public void seekForward() {
        mediaPlayer.seek(mediaPlayer.getCurrentTime().add(Duration.seconds(2)));
    }

    public void seekBackwards() {
        mediaPlayer.seek(mediaPlayer.getCurrentTime().add(Duration.seconds(-2)));
    }

    public SoundService currentlyPlaying() {
        return currentlyPlaying;
    }

    public Duration getCurrentPosition() {
        return mediaPlayer.getCurrentTime();
    }

    public Duration getDuration() {
        return mediaPlayer.getMedia().getDuration();
    }

    public double getProgressPrecentage() {
        return getCurrentPosition().toMillis() / getDuration().toMillis();
    }

    public ReadOnlyObjectProperty<Duration> getCurrentTimeProperty() {
        return mediaPlayer.currentTimeProperty();
    }

    public SoundService getPreviouslyPlaying() {
        return previouslyPlaying;
    }
}
