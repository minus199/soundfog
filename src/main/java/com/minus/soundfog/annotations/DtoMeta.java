package com.minus.soundfog.annotations;

import com.minus.soundfog.modules.db.models.BaseModel;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import org.jooq.Record;
import org.jooq.TableLike;
import org.jooq.impl.DAOImpl;
import org.jooq.impl.TableImpl;

/**
 * @author minus
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface DtoMeta {
    Class<? extends BaseModel> baseModelClass();
    Class<? extends DAOImpl> daoClass();
    
}
