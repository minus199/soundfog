package com.minus.soundfog;

import com.minus.soundfog.modules.db.DB;
import com.minus.soundfog.modules.db.dto.SoundDto;
import com.minus.soundfog.modules.db.models.BaseModel;
import com.minus.soundfog.modules.ui.MainViewController;
import com.minus.soundfog.modules.ui.eventhandlers.IUserActionEventManager;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Screen;
import javafx.stage.Stage;
import org.reflections.Reflections;

/**
 * @author minus
 */
public class MainApp extends Application {
    private static final org.apache.logging.log4j.Logger logger = org.apache.logging.log4j.LogManager.getLogger(MainApp.class.getName());
    
    private MainViewController mainViewController;
    private BorderPane mainView;

    private Stage stage;
    private Scene scene;
    final public String APP_NAME = "SoundFog";
    private static MainApp instance;

    @Override
    public void init() throws Exception {
        super.init();
        instance = this;
        DB.getInstance();
    }

    public static MainApp getInstance() {
        return instance;
    }

    public MainViewController getMainViewController() {
        return mainViewController;
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        onStart(primaryStage);
        initEventListeners();
    }

    public void initEventListeners() {
        Set<Class<? extends IUserActionEventManager>> uiEventssManagers = new Reflections(getClass()).getSubTypesOf(IUserActionEventManager.class);

        uiEventssManagers.stream().forEach((Class<? extends IUserActionEventManager> next) -> {
            try {
                IUserActionEventManager newInstance = next.getConstructor(MainApp.class).newInstance(this);
                newInstance.handle();
            } catch (InstantiationException | IllegalAccessException | NoSuchMethodException | SecurityException | IllegalArgumentException | InvocationTargetException ex) {
                logger.error(ex);
            }
        });
    }

    private void onStart(Stage primaryStage) throws Exception {
        this.stage = primaryStage;

        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getResource("/fxml/MainView.fxml"));
        mainView = fxmlLoader.load();
        mainViewController = fxmlLoader.getController();

        scene = new Scene(mainView);
        mainView.getBottom().setCursor(Cursor.NONE);
        prepareStage();
        
    }

    private void loadCSS() {
        scene.getStylesheets().clear();

        scene.getStylesheets().addAll(
                "/styles/main.css",
                "/styles/player.css",
                "/styles/playlist-item.css",
                "/styles/playlist-scrollbar.css",
                "/styles/play-progress-bar.css",
                "/styles/sound-container.css"
        );
        
        logger.info("Loaded CSS.");
    }

    private MainApp prepareStage() {
        String processName = java.lang.management.ManagementFactory.getRuntimeMXBean().getName();
        long parseLong = Long.parseLong(processName.split("@")[0]);

        stage.setTitle(APP_NAME + " -- pid: " + parseLong);
        stage.setScene(scene);
        positionAndSizeStage();

        stage.show();

        loadCSS();
        return this;
    }

    private void positionAndSizeStage() {
        Rectangle2D bounds = Screen.getScreens().get(0).getBounds();
        double width = bounds.getWidth() * 0.85;
        double height = bounds.getHeight();

        stage.setMinWidth(width * 0.6);
        stage.setMinHeight(height * 0.3);
        stage.setWidth(850);
        stage.setHeight(600);

        //stage.setX(0 + stage.getWidth());
        stage.setY(0 + stage.getHeight());
        //stage.centerOnScreen();
    }

    public Scene getScene() {
        return scene;
    }

    public Stage getStage() {
        return stage;
    }

    public BorderPane getMainView() {
        return mainView;
    }

    final public static Properties getProperties() {
        Object currentProperties = System.getProperties().get("soundfog.properties");

        if (currentProperties == null) {
            System.getProperties().put("soundfog.properties", new Properties());
        }

        return (Properties) System.getProperties().get("soundfog.properties");
    }
}
